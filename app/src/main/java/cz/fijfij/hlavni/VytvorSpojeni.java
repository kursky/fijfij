package cz.fijfij.hlavni;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;

import cz.fijfij.R;
import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.service.FijService;
import cz.fijfij.hlavni.sms.SmsItem;
import cz.fijfij.hlavni.zapouzdreno.SmsControl;

/**
 * Toto je class na straně ridiciho a ten vola ActivityParametryNoveho, zbytek by mela byt historie
 */

public class VytvorSpojeni extends BaseActivity {
    private static final int INICIALIZACE = 1;
    private static final int KOD_ZADAN = 2;
    private static final int HANDSHAKE = 4;
    private static final int SMS_CISLO = 5;
    private static final int OMEZENY_POCET = 6;

    private final int stRIDICI = 10;
    private final int stPASIVNI = 20;
    private final int stKONEC = 30;
    private final int stCEKANI_SMS = 40;
    private final int stOMEZENY_POCET = 50;

    private static final String TAG = "VYS";

    private TextView txtSpojovaciKlic;
    private EditText edNastavKod;
    private String spojovaciKlic = null;
    private TextView txtInfoRidici;
    private ServiceOp serviceOp;
    private SmsControl smsControl;
    private String mCislo;
    private PrateleOp prateleOp;
    private SmsParovani smsParovani=null;
    private IntentFilter smsFilter;
    private Activity activity;


    public VytvorSpojeni() {
        super(R.layout.activity_vytvor_spojeni);
    }

    ServiceOp.ServiceInt serviceInt = new ServiceOp.ServiceInt() {

        @Override
        public void onServiceConnected() {
            serviceOp.registerClient();
        }

        @Override
        public void onServiceDisconnected() {
            // Odtud se se volaji dalsi aktivity vcetne MainActivity
            /*
            konfigurujSluzbu = new KonfigurujSluzbu(getBaseContext(), kf);
            konfigurujSluzbu.execute();
            */
        }

        @Override
        public boolean receivedMessage(Message msg) {
            switch (msg.what) {
                case FijService.MSG_NOT_RUNNING:
                    serviceOp.startService();
                    break;
                // Sluzba je pripravena na prijeti potvrzovaci zpravy
                case FijService.MSG_POTVRZENI_SMS:
                    if (def.jeRidici()) smsControl.odesliZadostPratelstvi(mCislo);
                    break;
                case FijService.MSG_KONEC_POTVRZENI_SMS:
                    TextView txtKonec = (TextView) findViewById(R.id.txtInfoKonec);
                    txtKonec.setText(R.string.smsPotvrzeno);
                    nastavOvladani(stKONEC);
                    break;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity=this;
        BufferOp bufferOp = new BufferOp(this);
        prateleOp = new PrateleOp(bufferOp);
        prateleOp.nactiOffline();
        nastavOvladani(def.jeRidici() ? stPASIVNI : stRIDICI);
        edNastavKod = (EditText) findViewById(R.id.edNastavKod);
        txtSpojovaciKlic = (TextView) findViewById(R.id.txtSpojovaciKlic);
        txtSpojovaciKlic.setText("");
        txtInfoRidici = (TextView) findViewById(R.id.txtInfoRidici);
        txtInfoRidici.setText("");
        TextView txtInfoKonec = (TextView) findViewById(R.id.txtInfoKonec);
        txtInfoKonec.setText("");
        serviceOp = new ServiceOp(getBaseContext(), serviceInt);
        smsControl = new SmsControl(this);

        // @TODO Pripravovana funkce - zatim nefunkcni
        Button btnSmsNovy = (Button) findViewById(R.id.btnSmsNovy);
        btnSmsNovy.setVisibility(View.GONE);
        Button btnPotvrzeniKodu = (Button) findViewById(R.id.btnPotvrzeniKodu);
        btnPotvrzeniKodu.setVisibility(View.VISIBLE);
        Button btnPresSMS = (Button) findViewById(R.id.btnPresSMS);
        btnPresSMS.setVisibility(View.GONE);
        //
        if (prateleOp.getSize() >= 5) {
            nastavCekej(OMEZENY_POCET);
        } else {
            nastavCekej(INICIALIZACE);
        }

        smsFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        smsFilter.setPriority(1000);
        final Context ctx = this;
        if (!def.jeRidici()) {
            smsParovani = new SmsParovani(new SmsParovani.PritelHandshake() {
                @Override
                public void onPridejRodice(SmsItem smsItem) {
                    SmsControl smsControl = new SmsControl(ctx);
                    smsControl.odesliOdpovedClena(smsItem);
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (smsParovani!=null) registerReceiver(smsParovani, smsFilter);
        serviceOp.doBindService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (smsParovani!=null) unregisterReceiver(smsParovani);
        serviceOp.sendMessageToService(FijService.MSG_KONEC_POTVRZENI_SMS, 0, null);
        serviceOp.unRegisterClient();
        serviceOp.doUnbindService();
    }


    /**
     * Přepnutí obrazovek
     */
    private void nastavOvladani(int stav) {
        zrusCekej();
        RelativeLayout rlRidici = (RelativeLayout) findViewById(R.id.rlRidici);
        RelativeLayout rlPasivni = (RelativeLayout) findViewById(R.id.rlPasivni);
        RelativeLayout rlKonec = (RelativeLayout) findViewById(R.id.rlKonec);
        RelativeLayout rlCekaniSMS = (RelativeLayout) findViewById(R.id.rlCekaniSMS);
        RelativeLayout rlOmezenyLimit = (RelativeLayout) findViewById(R.id.rlOmezenyLimit);

        switch (stav) {
            case stRIDICI:
                rlRidici.setVisibility(View.VISIBLE);
                rlPasivni.setVisibility(View.GONE);
                rlKonec.setVisibility(View.GONE);
                rlCekaniSMS.setVisibility(View.GONE);
                rlOmezenyLimit.setVisibility(View.GONE);
                break;
            case stPASIVNI:
                rlRidici.setVisibility(View.GONE);
                rlPasivni.setVisibility(View.VISIBLE);
                rlKonec.setVisibility(View.GONE);
                rlCekaniSMS.setVisibility(View.GONE);
                rlOmezenyLimit.setVisibility(View.GONE);
                break;
            case stCEKANI_SMS:
                rlRidici.setVisibility(View.GONE);
                rlPasivni.setVisibility(View.GONE);
                rlKonec.setVisibility(View.GONE);
                rlCekaniSMS.setVisibility(View.VISIBLE);
                rlOmezenyLimit.setVisibility(View.GONE);
                break;
            case stKONEC:
                rlRidici.setVisibility(View.GONE);
                rlPasivni.setVisibility(View.GONE);
                rlCekaniSMS.setVisibility(View.GONE);
                rlKonec.setVisibility(View.VISIBLE);
                rlOmezenyLimit.setVisibility(View.GONE);
                break;
            case stOMEZENY_POCET:
                rlRidici.setVisibility(View.GONE);
                rlPasivni.setVisibility(View.GONE);
                rlCekaniSMS.setVisibility(View.GONE);
                rlKonec.setVisibility(View.GONE);
                rlOmezenyLimit.setVisibility(View.VISIBLE);
                break;
        }
    }


    private AsyncService.TaskListener odeslaniKodu = new AsyncService.TaskListener() {
        @Override
        public void onFinished(String nameAction, JSONObject result) {
            if (result == null) {
                zrusCekej();
                return;
            }
            try {
                String vysledek = result.getString("VYSLEDEK");
                if (!vysledek.equals("OK")) {
                    edNastavKod.setError(getString(R.string.error_chybny_kod));
                    nastavOvladani(stPASIVNI);
                    return;
                }
            } catch (Exception e) {
                MyLog.e(TAG, e);
                return;
            }
            nastavOvladani(stKONEC);
        }
    };


    private AsyncService.TaskListener ziskaniKodu = new AsyncService.TaskListener() {
        @Override
        public void onFinished(String nameAction, JSONObject result) {
            if (result == null) {
                zrusCekej();
                return;
            }
            try {
                spojovaciKlic = result.getString("SPOJOVACI_KLIC");
                if (spojovaciKlic != null) {
                    txtSpojovaciKlic.setText(spojovaciKlic);
                }
            } catch (Exception e) {
                MyLog.e(TAG, e);
            }
            zrusCekej();
        }
    };


    @Override
    public void cekaniNastaveno(int zdrojVolani) {
        switch (zdrojVolani) {
            case OMEZENY_POCET:
                nastavOvladani(stOMEZENY_POCET);
                break;
            case INICIALIZACE:
                if (def.jeRidici()) {
                    nastavOvladani(stRIDICI);
                } else {
                    nastavOvladani(stPASIVNI);
                }
                zrusCekej();
                break;
            case KOD_ZADAN:
                String sKodOdRidiciho=edNastavKod.getText().toString();
                SpojeniOp spojeniOp = new SpojeniOp(this);
                spojeniOp.kodRodiceZadan(sKodOdRidiciho, def.android_id, new SpojeniOp.OdeslanoServeru() {
                    @Override
                    public void nastalaChyba() {
                        zrusCekej();
                        edNastavKod.setError(getString(R.string.wrong_code));
                    }

                    @Override
                    public void vseOK(Bundle bundle) {
                        def.restartApp(activity);
                    }
                });
                break;
            case SMS_CISLO:
                String sMsg = null;
                if (TextUtils.isEmpty(mCislo)) {
                    sMsg = getString(R.string.cislo_nesmi_byt_prazdne);
                }
                mCislo = def.phoneNumberStandard(mCislo);
                Osoba osoba = prateleOp.vratPriteleDlePhoneNumber(mCislo);
                if (osoba != null) {
                    sMsg = "Dané číslo má: " + osoba.getNick();
                }
                if (sMsg != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(R.string.dialog);
                    builder.setMessage(sMsg);
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            nastavCekej(INICIALIZACE);
                        }
                    });
                    builder.show();
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putString("phoneNumber", mCislo);
                serviceOp.sendMessageToService(FijService.MSG_POTVRZENI_SMS, 0, bundle);
                nastavOvladani(stCEKANI_SMS);
                break;
        }
    }

    public void pritelKodZadal(View view) {
        String kod = edNastavKod.getText().toString().trim();
        if (TextUtils.isEmpty(kod)) {
            edNastavKod.setError(getString(R.string.error_field_required));
            return;
        }
        nastavCekej(KOD_ZADAN);
    }

    public void spojeniVytvoreno(View view) {
        def.hlavniStranka(this);
    }

    public void zrusitSMS(View view) {
        nastavOvladani(def.jeRidici() ? stRIDICI : stPASIVNI);
    }

    public void presSMS(View view) {
        Bundle bundle = new Bundle();
        bundle.putString("phoneNumber", "OK");
        serviceOp.sendMessageToService(FijService.MSG_POTVRZENI_SMS, 0, bundle);
        nastavOvladani(stCEKANI_SMS);
    }


    public void spojeniPresSMS(View view) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Telefonní číslo přítele");

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_PHONE);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mCislo = input.getText().toString();
                nastavCekej(SMS_CISLO);
            }
        });
        builder.setNegativeButton("Zpět", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
}