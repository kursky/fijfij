package cz.fijfij.hlavni.base;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;
import cz.fijfij.R;
/**
 * Created by jiri.kursky on 03.04.2017
 * Konstanty chybových hlášení
 * Pozor ERR_S_ musí musí korespondovat se serverem
 */

public class Err {
    private static final Map<Integer, Integer> ERR_MAP = new HashMap<Integer, Integer>();
    public static final int ERR_INTERNAL = 0;
    private static final int ERR_S_NENI_UZIVATEL = 10;
    private static final int ERR_S_NENI_TOKEN = 20;
    static {
        addItem(ERR_INTERNAL, R.string.err_internal);
        addItem(ERR_S_NENI_UZIVATEL, R.string.err_internal);
        addItem(ERR_S_NENI_TOKEN, R.string.err_internal);
    }

    private static void addItem(int err, int explanation) {
        ERR_MAP.put(err, explanation);
    }

    public static String getErrString(Context context, Integer number) {
        int resId=ERR_MAP.get(number);
        return context.getString(resId);
    }
}
