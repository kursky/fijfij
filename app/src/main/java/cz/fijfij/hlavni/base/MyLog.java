package cz.fijfij.hlavni.base;

import android.util.Log;

/**
 * Created by jiri.kursky on 5.8.2016.
 */
public class MyLog {
    private static Boolean doProtocol = false;

    public static void v(String tag, String msg) {
        Log.v(tag, msg);
        pridejRadek("v", tag, msg);
    }

    public static void w(String tag, String msg, Throwable t) {
        Log.w(tag, msg, t);
        pridejRadek("w", tag, msg);
    }

    public static void d(String tag, String msg) {
        Log.d(tag, msg);
        pridejRadek("d", tag, msg);
    }

    public static void e(String tag, String msg) {
        Log.e(tag, msg);
        pridejRadek("e", tag, msg);
    }

    public static void e(String tag, Exception e) {
        String s = e.toString();
        Log.e(tag, s);
        pridejRadek("e", tag, s);
    }

    public static void pridejRadek(String typ, String tag, String zprava) {
        /*
        if (!doProtocol || (typ!="v" && (typ!="e"))) return;
        if (bufferOp==null) return; // musi byt  nastartovan
        bufferOp.openDatabase(BufferOp.DB_WRITABLE);
        bufferOp.mDatabase.execSQL("insert into " + BufferOp.TBL_LOG +
                " (typ text, tag text, zprava text)+" +
                "values('" + typ + "','" + tag + "','" + zprava + "')");
                */
    }
}