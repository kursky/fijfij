/*
 * Copyright (c) 2017 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.app.Dialog;
import android.view.View;

/**
 * Created by jiri.kursky on 15.01.2017.
 */

public abstract class DialogListener implements View.OnClickListener {
    public Dialog dialog;

    public DialogListener(Dialog dialog) {
        this.dialog = dialog;
    }

    @Override
    public void onClick(View v) {
        klikNaButton();
    }

    public abstract void klikNaButton();
}
