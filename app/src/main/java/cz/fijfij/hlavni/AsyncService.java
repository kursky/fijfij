package cz.fijfij.hlavni;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import cz.fijfij.hlavni.base.Err;
import cz.fijfij.hlavni.base.MyLog;

/**
 * Created by jiri.kursky on 5.8.2016
 * Asynchronní volání serveru
 * Úprava 23.3.2017 - přidán hlídací pes kvůli pádům v hlavní smyčce
 */
public class AsyncService extends ExceptionAsyncTask<Void, Integer, JSONObject> {
    private final TaskListener taskListener;
    private WatchDogListener watchDogListener = null;
    private StandardListener standardListener = null;
    private ErrorListener errorListener = null;
    public boolean hasError = false;
    private String action;
    private String nameAction;
    static final String TAG = AsyncService.class.getSimpleName();
    private static final int TIME_OUT = 7000;
    private static final int TIME_OUT_WATCH_DOG = 10000;
    private boolean mAsyncBezi = false;
    private boolean wasWatchDogError = false;


    public interface TaskListener {
        void onFinished(String nameAction, JSONObject result);
    }

    public interface WatchDogListener {
        void onError();
    }

    public interface StandardListener {
        void onReturn(int stReturn, String msg);
    }

    public interface ErrorListener {
        void onError(int errCode);
    }

    public void setupStandardListener(StandardListener standardListener) {
        this.standardListener = standardListener;
    }

    public void setupErrorListener(ErrorListener errorListener) {
        this.errorListener = errorListener;
    }

    public void setupWatchDog(WatchDogListener watchDogListener) {
        this.watchDogListener = watchDogListener;
    }


    /**
     * Základní definice funkce - nyní se nepoužívá
     *
     * @param taskListener návratová funkce
     * @param nameAction   název procesu - slouží jen pro identifikaci a nemá jinak žádný význam
     * @param action       plný dotaz s parametry
     * @param aId          id číslo v tabulce t_loop, může být null
     */
    public AsyncService(@Nullable TaskListener taskListener, Context context, String nameAction, String action, @Nullable String aId) {
        // The taskListener reference is passed in through the constructor

        this.taskListener = taskListener;
        this.action = action;
        this.nameAction = nameAction;
        SettingOp settingOp = new SettingOp(context);

        if (this.action == null) {
            this.action = settingOp.getActionString(this.nameAction);
            if (aId != null) this.action += "&idrecord=" + aId;
        }
    }

    public AsyncService(@Nullable TaskListener taskListener, Context context, String nameAction, String action) {
        // The taskListener reference is passed in through the constructor

        this.taskListener = taskListener;
        this.action = action;
        this.nameAction = nameAction;
        SettingOp settingOp = new SettingOp(context);
        if (this.action == null) {
            this.action = settingOp.getActionString(this.nameAction);
        }
    }

    /**
     * Základní definice
     *
     * @param taskListener návratová funkce
     * @param context      context
     * @param nameAction   název akce a k tomu budou přiřazeny parametry ze SettingOp
     */
    public AsyncService(@Nullable TaskListener taskListener, Context context, String nameAction) {
        SettingOp settingOp = new SettingOp(context);
        this.taskListener = taskListener;
        this.nameAction = nameAction;
        action = settingOp.getActionString(this.nameAction);
    }


    /**
     * Hlavní procedura zasílající dotaz
     */
    private JSONObject sendRequest(String sPar) {
        JSONObject retVal = null;
        try {
            hasError = false;
            URL url = new URL(def.sUrl);
            HttpURLConnection urlConnection =
                    (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");

            OutputStream stream = urlConnection.getOutputStream();
            OutputStreamWriter writer = new OutputStreamWriter(stream);

            MyLog.d(TAG, "Zapsání dotazu: " + sPar);
            writer.write(sPar);
            writer.close();

            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader bufferedReader =
                        new BufferedReader(new InputStreamReader(
                                urlConnection.getInputStream()));
                String next;
                next = bufferedReader.readLine();
                if (next != null) {
                    try {
                        retVal = new JSONObject(next);
                    } catch (JSONException e) {
                        MyLog.e(TAG, "JSON navrat ma chybu");
                        infoWatchDogError();
                        retVal = null;
                    }
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, e);
            infoWatchDogError();
            retVal = null;
        }
        return retVal;
    }

    @Override
    protected JSONObject doInBackground() {
        MyLog.d(TAG, "InBackground: " + action);
        return sendRequest(action);
    }


    protected void stop() {
        cancel(true);
        mAsyncBezi = false;
    }

    /**
     * Návrat ze serveru
     */
    @Override
    protected void onPostExecute(Exception e, JSONObject result) {
        mAsyncBezi = false;
        if (e != null) {
            infoWatchDogError();
        }
        MyLog.d(TAG, "Dotaz skončil");
        if (result != null && standardListener != null) {
            if (result.has(def.RET_BAN)) {
                String msg;
                try {
                    msg = result.getString(def.RET_BAN);
                } catch (Exception n) {
                    msg = "";
                }
                standardListener.onReturn(def.ST_BAN, msg);
                return;
            }
            if (result.has(def.RET_ERROR) && errorListener != null) {
                hasError = true;
                try {
                    errorListener.onError(result.getInt(def.RET_ERROR));
                } catch (Exception o) {
                    errorListener.onError(Err.ERR_INTERNAL);
                }
            }
        }

        if (taskListener != null) {
            // And if it is we call the callback function on it.
            taskListener.onFinished(nameAction, result);
        } else {
            infoWatchDogError();
        }
    }


    /**
     * Procedura zajišťující spuštění dotazu
     */
    protected void execute() {
        hasError = false;
        wasWatchDogError = false;
        mAsyncBezi = true;
        if (watchDogListener != null) {
            Handler hlidaniHandler = new Handler();
            hlidaniHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!mAsyncBezi) return;
                    infoWatchDogError();
                }
            }, TIME_OUT_WATCH_DOG);
        }
        super.execute();
        try {
            get(TIME_OUT, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            onPostExecute(e, null);
        } catch (ExecutionException e) {
            onPostExecute(e, null);
            e.printStackTrace();
        } catch (TimeoutException e) {
            onPostExecute(e, null);
        }
    }

    private void infoWatchDogError() {
        if (wasWatchDogError) return;
        wasWatchDogError = true;
        try {
            stop();
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        if (watchDogListener != null) watchDogListener.onError();
    }
}