package cz.fijfij.hlavni;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import cz.fijfij.R;

public class MainActivityLarge extends FragmentActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_large);
    }

    @Override
    public void onBackPressed() {
        def.otazkaUkoncitProgram(this);
    }
}
