/*
 * Copyright (c) 2017 Jiri Kursky
 */

package cz.fijfij.hlavni.lokace;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import cz.fijfij.R;
import cz.fijfij.hlavni.def;
import cz.fijfij.hlavni.nastaveni.ParametrItem;

/**
 * Created by jiri.kursky on 16.01.2017.
 * Aktivity spojené s polohou
 */

public class PolohaOp {
    private ArrayList<String> allKeys;
    private ArrayList<ParametrItem> parametrItems;
    private LatLngItem latLngItem;

    PolohaOp(LatLngItem latLngItem) {
        allKeys = new ArrayList<>();
        parametrItems = new ArrayList<>();
        this.latLngItem=latLngItem;
        setParamPoloha(def.PREF_POLOHA_NAZEV, R.string.poloha_nazev, ParametrItem.PAR_STRING, "", 20)
                .setValueString(latLngItem.vratNazevBodu());
        setParamPoloha(def.PREF_LAT_LNG, R.string.souradnice, ParametrItem.PAR_STRING, "", 20)
                .setZakazEditace()
                .setValueString(latLngItem.getLatLng().toString());
        setParamPoloha(def.PREF_POLOHA_IN, R.string.poloha_vstup, ParametrItem.PAR_BOOLEAN, false);
        setParamPoloha(def.PREF_POLOHA_OUT, R.string.poloha_vystup, ParametrItem.PAR_BOOLEAN, false);
    }

    private ParametrItem setParamPoloha(String key, int nazev, int typ, boolean pocatecniHodnota) {
        ParametrItem pi;
        parametrItems.add(pi = new ParametrItem(key, null, nazev, typ, pocatecniHodnota));
        allKeys.add(pi.getKlic());
        return pi;
    }

    private ParametrItem setParamPoloha(String key, int nazev, int typ, String pocatecniHodnota, int maxDelka) {
        ParametrItem pi;
        parametrItems.add(pi = new ParametrItem(key, null, nazev, typ, pocatecniHodnota, maxDelka));
        allKeys.add(pi.getKlic());
        return pi;
    }

    ArrayList<String> getAllKeys() {
        return allKeys;
    }

    public ParametrItem getParametrItem(int position) {
        return parametrItems.get(position);
    }

    public LatLng getLatLng() {
        return null;
    }

    public LatLngItem getLatLngItem() {
        return latLngItem;
    }
}
