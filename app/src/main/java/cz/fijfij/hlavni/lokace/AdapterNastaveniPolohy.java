/*
 * Copyright (c) 2017 Jiri Kursky
 */

package cz.fijfij.hlavni.lokace;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import cz.fijfij.R;
import cz.fijfij.hlavni.BufferOp;
import cz.fijfij.hlavni.Osoba;
import cz.fijfij.hlavni.PrateleOp;
import cz.fijfij.hlavni.def;
import cz.fijfij.hlavni.nastaveni.ParametrItem;

/**
 * Created by jiri.kursky on 03.12.2016
 * Viditelne parametry pro editaci pritele
 */

class AdapterNastaveniPolohy extends ArrayAdapter<String> {
    private Activity activity;
    private final ListView listView;
    private BufferOp bufferOp;
    private Osoba osoba;
    private String nazevBodu;
    private LatLngItem latLngItem;
    private PolohaOp polohaOp;


    AdapterNastaveniPolohy(Activity activity, ListView listView, String userId, PolohaOp polohaOp) {
        super(activity, R.layout.list_parametry, polohaOp.getAllKeys());
        this.activity = activity;
        this.listView = listView;
        this.polohaOp = polohaOp;
        bufferOp = new BufferOp(activity);
        PrateleOp prateleOp = new PrateleOp(bufferOp);
        prateleOp.nactiOffline();
        osoba = prateleOp.vratDleUserId(userId);
        latLngItem = polohaOp.getLatLngItem();
        nazevBodu = latLngItem.vratNazevBodu();
        if (TextUtils.isEmpty(nazevBodu)) nazevBodu = osoba.getNick();
    }

    String getNazevBodu() {
        return nazevBodu;
    }

    void setNazevBodu(String nazevBodu) {
        ParametrItem parametrItem = polohaOp.getParametrItem(0);
        parametrItem.setValueString(nazevBodu);
        this.nazevBodu = nazevBodu;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_parametry, null, true);

        TextView txtNazevParametru = (TextView) rowView.findViewById(R.id.txtNazevParametru);
        TextView txtSubParametr = (TextView) rowView.findViewById(R.id.txtSubNazevParametru);
        Switch prepinac = (Switch) rowView.findViewById(R.id.prepinac);
        ParametrItem parametrItem = polohaOp.getParametrItem(position);
        txtNazevParametru.setText(parametrItem.getNazev());

        switch (parametrItem.getTyp()) {
            case ParametrItem.PAR_STRING:
                txtSubParametr.setText(parametrItem.getValueString());
                prepinac.setVisibility(View.INVISIBLE);
                break;
            case ParametrItem.PAR_BOOLEAN:
                txtSubParametr.setVisibility(View.GONE); // neni potrebovat text u přepínace, ten se definuje vlevo, vypadá to blbě
                switch (parametrItem.getKlic()) {
                    case def.PREF_POLOHA_IN:
                        prepinac.setChecked(latLngItem.vratNaBoduSMS(osoba.getUserId()));
                        break;
                    default:
                        prepinac.setChecked(parametrItem.getValueBoolean());
                }
                prepinac.setVisibility(View.VISIBLE);
                break;
        }

        prepinac.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int position = listView.getPositionForView(buttonView);
                ParametrItem parametrItem = polohaOp.getParametrItem(position);
                String klic = parametrItem.getKlic();
                switch (klic) {
                    case def.PREF_POLOHA_IN:
                        latLngItem.nastavBodSMS(osoba.getUserId(), isChecked);
                        break;
                }
            }
        });
        return rowView;
    }
}
