package cz.fijfij.hlavni.lokace;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;

import cz.fijfij.hlavni.service.FijService;
import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.SettingOp;
import cz.fijfij.hlavni.def;

/**
 * Created by jiri.kursky on 5.8.2016.
 * Operace s lokací
 * Používat LatLngItem
 */
public class LocationService {
    //The minimum distance to change updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 20; //200

    //The minimum time between updates in milliseconds
    //private static final long minTimeUpdate = 5*60*1000;
    private static final long minTimeUpdate = 60*1000;
    static final String TAG = FijService.class.getSimpleName();


    private Location storedLocation = null;

    public boolean isGPSEnabled;
    private boolean isNetworkEnabled;
    private LocationManager locationManager;
    private Context context;
    private SettingOp settingOp;
    private String currentProvider = "";
    private ZmenaNastaveni zmenaNastaveni = null;
    public LocationListener locationListener =null;

    public interface LocationListener {
        public void onChangeLocation(Location location, boolean pretecenParametr);
    }

    private void locationChanged(Location location) {
        def.actualLocation = new LatLngItem(storedLocation, location);
        if (locationListener !=null) locationListener.onChangeLocation(def.actualLocation, false);

        Long zmena = Math.round(def.vzdalenostSouradnic(location, storedLocation));
        int maxRychlost = settingOp.getIntFromString(def.PREF_RYCHLOSTNI_LIMIT, 0);
        boolean prekrocenaRychlost=maxRychlost!=0 &&  def.actualLocation.getSpeed()>maxRychlost;

        if ((zmena >= MIN_DISTANCE_CHANGE_FOR_UPDATES) || (prekrocenaRychlost)) {
            settingOp.setLatLng(def.actualLocation);
            storedLocation=new LatLngItem(location);
            if (locationListener !=null) locationListener.onChangeLocation(def.actualLocation, true);
        }
    }

    /**
     * Zatím rozděleno na listener od sítě a GPS
     */
    private android.location.LocationListener listenerNetwork = new android.location.LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if ((location == null) || (LatLngItem.tabulkaTypZjisteniLokace(currentProvider)==def.LOKACE_PROVIDER_GPS)) return;
            locationChanged(location);
        }

        @Override
        public void onProviderEnabled(String provider) {
            if (zmenaNastaveni != null)
                zmenaNastaveni.onPolohaPovolena(provider.equals(currentProvider));
            if (provider.equals(currentProvider)) return;
            noveNastaveni();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (provider.equals(currentProvider)) return;
            noveNastaveni();
        }

        @Override
        public void onProviderDisabled(String provider) {
            if (zmenaNastaveni != null) zmenaNastaveni.onPolohaPovolena(false);
            if (currentProvider.equals("")) return; // Není nastavený žádný
            noveNastaveni();
        }
    };

    /**
     * Zatím rozděleno na listener od sítě a GPS
     */
    private android.location.LocationListener listenerGPS = new android.location.LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            locationChanged(location);
        }

        @Override
        public void onProviderEnabled(String provider) {
            if (zmenaNastaveni != null)
                zmenaNastaveni.onPolohaPovolena(provider.equals(currentProvider));
            noveNastaveni();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (provider.equals(currentProvider)) return;
            noveNastaveni();
        }

        @Override
        public void onProviderDisabled(String provider) {
            if (zmenaNastaveni != null) zmenaNastaveni.onPolohaPovolena(false);
            noveNastaveni();
        }
    };

    /**
     * Local constructor
     */
    public LocationService(Context context, @Nullable ZmenaNastaveni zmenaNastaveni) {
        settingOp = new SettingOp(context);
        this.zmenaNastaveni = zmenaNastaveni;
        initLocationService(context);
    }

    public LocationService(SettingOp settingOp) {
        this.settingOp = settingOp;
        this.zmenaNastaveni = null;
        initLocationService(settingOp.getContext());
    }

    /**
     * Sets up location service after permissions is granted
     */
    private void initLocationService(Context aContext) {
        context = aContext;
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        this.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    minTimeUpdate,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, listenerGPS);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    minTimeUpdate,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, listenerNetwork);
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        noveNastaveni();
    }

    /**
     * Nastavuje aktuálního providera
     */
    private void noveNastaveni() {
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        try {
            if (locationManager == null) return;
            if (storedLocation == null) {
                storedLocation=settingOp.getLangLngItem();
            }

            // Get LOKACE_PROVIDER_GPS and network status
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            // Pro účely ladění
            // isNetworkEnabled=false;

            currentProvider = "";

            if (isNetworkEnabled || isGPSEnabled) {
                if (isNetworkEnabled && !isGPSEnabled) {
                    currentProvider = LocationManager.NETWORK_PROVIDER;
                }

                if (isGPSEnabled) {
                    currentProvider = LocationManager.GPS_PROVIDER;
                }
            }
            settingOp.putInt(def.PREF_TYP_ZJISTENI_LOKACE, LatLngItem.tabulkaTypZjisteniLokace(currentProvider));
        } catch (Exception ex) {
            MyLog.e(TAG, "Error creating location service: " + ex.getMessage());
        }
    }

    public interface ZmenaNastaveni {
        void onPolohaPovolena(boolean ano);
    }

    public void close() {
        if (locationManager != null) {
            if (Build.VERSION.SDK_INT >= 23 &&
                    ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.removeUpdates(listenerGPS);
            locationManager.removeUpdates(listenerNetwork);
            currentProvider = "";
        }
    }

}