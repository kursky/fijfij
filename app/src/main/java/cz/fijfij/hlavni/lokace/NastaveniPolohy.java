/*
 * Copyright (c) 2017 Jiri Kursky
 */

package cz.fijfij.hlavni.lokace;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import cz.fijfij.R;
import cz.fijfij.hlavni.BufferOp;
import cz.fijfij.hlavni.DialogListener;
import cz.fijfij.hlavni.ParametryNastaveni;
import cz.fijfij.hlavni.ServiceOp;
import cz.fijfij.hlavni.def;
import cz.fijfij.hlavni.nastaveni.ParametrItem;
import cz.fijfij.hlavni.service.FijService;

/**
 * Created by jiri.kursky on 14.01.2017
 * pro nastavení názvu polohy uživatelem a definice akce SMS
 */

public class NastaveniPolohy extends ParametryNastaveni {
    private AdapterNastaveniPolohy adapterNastaveniPolohy;
    private LatLngItem latLngItem;
    private String novyNazevBodu;
    private String userId;
    private PolohaOp polohaOp;
    private ServiceOp serviceOp;

    public NastaveniPolohy() {
        super(R.layout.nastaveni_polohy, 0);
    }

    ServiceOp.ServiceInt serviceInt = new ServiceOp.ServiceInt() {

        @Override
        public void onServiceConnected() {
            serviceOp.registerClient();
        }

        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public boolean receivedMessage(Message msg) {
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        Double latitude = intent.getDoubleExtra(def.PREF_LATITUDE, 0.0);
        Double longitude = intent.getDoubleExtra(def.PREF_LONGITUDE, 0.0);
        userId = intent.getStringExtra(def.PREF_USER_ID);
        latLngItem = new LatLngItem(latitude, longitude, new BufferOp(this));
        super.onCreate(savedInstanceState);
        polohaOp = new PolohaOp(latLngItem);
    }

    @Override
    protected String vratHodnotuString(String klic) {
        if (klic.equals(def.PREF_POLOHA_NAZEV)) {
            return adapterNastaveniPolohy.getNazevBodu();
        }
        return "";
    }

    @Override
    protected void onPause() {
        super.onPause();
        serviceOp.sendMessageToService(FijService.MSG_NASTAV_BOD, userId);
        serviceOp.unRegisterClient();
        serviceOp.doUnbindService();
    }

    @Override
    protected void defineNastaveni(String klic, Button theButton) {
        switch (klic) {
            case def.PREF_POLOHA_NAZEV:
                theButton.setOnClickListener(new PolohaListener(dialog));
                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        serviceOp = new ServiceOp(this, serviceInt);
        ListView listView = (ListView) findViewById(R.id.listNastaveniPolohy);
        adapterNastaveniPolohy = new AdapterNastaveniPolohy(this, listView, userId, polohaOp);
        adapterNastaveniPolohy.notifyDataSetChanged();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final ParametrItem parametrItem = polohaOp.getParametrItem(position);
                int typ = parametrItem.getTyp();
                if (typ != ParametrItem.PAR_STRING || parametrItem.getZakazEditace()) return;
                nastavHodnotuDialogem(parametrItem, vratHodnotuString(parametrItem.getKlic()));

            }
        });
        listView.setAdapter(adapterNastaveniPolohy);
        serviceOp.doBindService();
        zrusCekej();
    }


    class PolohaListener extends DialogListener {
        PolohaListener(Dialog dialog) {
            super(dialog);
        }

        @Override
        public void klikNaButton() {
            String mValue = dlgEditText.getText().toString();
            novyNazevBodu = mValue;
            adapterNastaveniPolohy.setNazevBodu(mValue);
            latLngItem.nastavNazevBodu(novyNazevBodu);
            adapterNastaveniPolohy.notifyDataSetChanged();
            dialog.dismiss();
        }
    }
}
