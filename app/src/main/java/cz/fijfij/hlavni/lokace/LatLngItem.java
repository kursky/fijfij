/*
 * Copyright (c) 2017 Jiri Kursky
 */

package cz.fijfij.hlavni.lokace;

import android.content.ContentValues;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import cz.fijfij.hlavni.BufferOp;
import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.SettingOp;
import cz.fijfij.hlavni.def;


/**
 * Created by jiri.kursky on 17.01.2017
 * <p>
 * Počet desetinných míst na přesnost u lokace
 * decimal  degrees    distance
 * places
 * -------------------------------
 * 0        1.0        111 km
 * 1        0.1        11.1 km
 * 2        0.01       1.11 km
 * 3        0.001      111 m
 * 4        0.0001     11.1 m
 * 5        0.00001    1.11 m  -> default
 * 6        0.000001   0.111 m
 * 7        0.0000001  1.11 cm
 * 8        0.00000001 1.11 mm
 * </p>
 */

public class LatLngItem extends Location {
    private LatLng ulozenLatLng = null;
    public float zoom = 12;
    private BufferOp bufferOp;
    private static final int POCET_DESETINNYCH_MIST = 5;
    private static final String TAG = "LL";

    public LatLngItem(double latitude, double longitude, BufferOp bufferOp) {
        super("");
        reset();
        initLatLng(latitude, longitude);
        this.bufferOp = bufferOp;
    }

    public LatLngItem(double latitude, double longitude, int typZjisteniLokace) {
        this(latitude, longitude, null);
        setProvider(typZjisteniLokace);
    }

    public LatLngItem(String latitude, String longitude, int typZjisteniLokace) {
        this(def.parseDouble(latitude), def.parseDouble(longitude), null);
        setProvider(typZjisteniLokace);
    }

    public LatLngItem(String latitude, String longitude, BufferOp bufferOp) {
        super("");
        reset();
        initLatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        this.bufferOp = bufferOp;
    }

    public LatLngItem(LatLng latLng, BufferOp bufferOp, SettingOp settingOp) {
        super("");
        reset();
        initLatLng(latLng.latitude, latLng.longitude);
        this.bufferOp = bufferOp;
        setZoom(settingOp);
    }

    public LatLngItem(@Nullable Location storedLocation, Location location) {
        super(location);
        this.bufferOp = null;
        setSpeed(storedLocation);
    }

    public LatLngItem(@NonNull Location location) {
        super(location);
        this.bufferOp = null;
    }

    void setZoom(SettingOp settingOp) {
        this.zoom = (float) settingOp.getDouble(def.PREF_MAPA_ZOOM, 12.0);
    }

    private double zaokrouhli(double souradnice) {
        double nasobek = Math.pow(10, POCET_DESETINNYCH_MIST);
        return Math.round(souradnice * nasobek) / nasobek;
    }

    private void initLatLng(double latitude, double longitude) {
        setLatitude(zaokrouhli(latitude));
        setLongitude(zaokrouhli(longitude));
    }

    @Nullable
    public String vratNazevBodu() {
        if (bufferOp == null) return null;
        bufferOp.openDatabase(BufferOp.DB_READONLY);
        String retVal;
        Cursor res = null;
        try {
            res = bufferOp.mDatabase.rawQuery("select * from " + BufferOp.TBL_NAZVY_BODU, null);
            if (res.moveToFirst()) {
                while (!res.isAfterLast()) {
                    double dbLatitude = Double.parseDouble(res.getString(res.getColumnIndex(def.PREF_LATITUDE)));
                    double dbLongitude = Double.parseDouble(res.getString(res.getColumnIndex(def.PREF_LONGITUDE)));
                    ulozenLatLng = new LatLng(dbLatitude, dbLongitude);
                    Double vzdalenost = def.vzdalenostSouradnic(this, dbLatitude, dbLongitude);
                    if (vzdalenost <= def.LOKACE_TOLEROVANA_VZDALENOST) {
                        retVal = res.getString(res.getColumnIndex("nazev"));
                        return retVal;
                    } else {
                        ulozenLatLng = null;
                    }
                    res.moveToNext();
                }
            }

        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        bufferOp.closeDatabase(res);
        return null;
    }

    private boolean nazevNeexistuje() {
        if (ulozenLatLng != null) return false;
        vratNazevBodu();
        return ulozenLatLng == null;
    }

    private void vymazNazev() {
        if (nazevNeexistuje() || bufferOp == null) return;
        bufferOp.openDatabase(BufferOp.DB_WRITABLE);
        bufferOp.mDatabase.execSQL("delete from BufferOp.TBL_NAZVY_BODU where " + vratPodminkuBodu());
        Cursor res;
        ArrayList<Integer> ids = new ArrayList<>();
        try {
            res = bufferOp.mDatabase.rawQuery("select * from " + BufferOp.TBL_NAZVY_BODU, null);
            if (res.moveToFirst()) {
                while (!res.isAfterLast()) {
                    double dbLatitude = Double.parseDouble(res.getString(res.getColumnIndex(def.PREF_LATITUDE)));
                    double dbLongitude = Double.parseDouble(res.getString(res.getColumnIndex(def.PREF_LONGITUDE)));
                    Double vzdalenost = def.vzdalenostSouradnic(this, dbLatitude, dbLongitude);
                    if (vzdalenost <= def.LOKACE_TOLEROVANA_VZDALENOST) {
                        ids.add(res.getInt(res.getColumnIndex("id")));
                    }
                    res.moveToNext();
                }
            }
            res.close();
            for (int id : ids) {
                bufferOp.mDatabase.execSQL("delete from " + BufferOp.TBL_NAZVY_BODU + " where id=" + id);
            }

        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        bufferOp.closeDatabase();
    }


    public void nastavNazevBodu(String nazev) {
        if (bufferOp == null) return;
        vymazNazev();
        if (TextUtils.isEmpty(nazev)) return;
        bufferOp.openDatabase(BufferOp.DB_WRITABLE);
        ContentValues values = new ContentValues();
        values.put(def.PREF_LATITUDE, getLatitude());
        values.put(def.PREF_LONGITUDE, getLongitude());
        values.put("nazev", nazev);
        bufferOp.mDatabase.insert(BufferOp.TBL_NAZVY_BODU, null, values);
        bufferOp.closeDatabase();
    }


    public void nastavBodSMS(String userId, Boolean nastav) {
        if (bufferOp == null) return;
        vymazBodSMS(userId);
        if (!nastav) return;
        bufferOp.openDatabase(BufferOp.DB_WRITABLE);
        ContentValues values = new ContentValues();
        values.put(def.PREF_LATITUDE, ulozenLatLng.latitude);
        values.put(def.PREF_LONGITUDE, ulozenLatLng.longitude);
        values.put(def.PREF_SMS_POVOLENO, 1);
        values.put(def.PREF_USER_ID, userId);
        values.put(def.PREF_ZMENA, 1);
        try {
            bufferOp.mDatabase.insert(BufferOp.TBL_AKTIVITY_BODU, null, values);
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        bufferOp.closeDatabase();
    }

    private void vymazBodSMS(String userId) {
        if (bufferOp == null) return;
        bufferOp.openDatabase(BufferOp.DB_WRITABLE);
        try {
            bufferOp.mDatabase.execSQL("delete from " + BufferOp.TBL_AKTIVITY_BODU + " where " + def.PREF_USER_ID + "=" + userId
                    + " and " + vratPodminkuBodu());
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        bufferOp.closeDatabase();
    }

    /**
     * Vrátí where pro vyhledavani souřadnic
     */
    private String vratPodminkuBodu() {
        if (nazevNeexistuje()) {
            return "(" + def.PREF_LATITUDE + "='" + String.valueOf(getLatitude()) + "' and " +
                    def.PREF_LONGITUDE + "='" + String.valueOf(getLongitude()) + "')";
        } else {
            return "(" + def.PREF_LATITUDE + "='" + String.valueOf(ulozenLatLng.latitude) + "' and " +
                    def.PREF_LONGITUDE + "='" + String.valueOf(ulozenLatLng.longitude) + "')";
        }
    }

    public boolean vratNaBoduSMS(String userId) {
        boolean retVal = false;
        if (bufferOp == null) return retVal;
        bufferOp.openDatabase(BufferOp.DB_READONLY);
        Cursor res = null;
        try {
            res = bufferOp.mDatabase.rawQuery("select * from " + BufferOp.TBL_AKTIVITY_BODU + " where " + def.PREF_USER_ID +
                    "=" + userId + " and " + vratPodminkuBodu(), null);
            if (res.moveToFirst()) {
                retVal = res.getInt(res.getColumnIndex(def.PREF_SMS_POVOLENO)) != 0;
            }
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        bufferOp.closeDatabase(res);
        return retVal;
    }


    public String sGetLongitude() {
        return String.valueOf(getLongitude());
    }

    public String sGetLatitude() {
        return String.valueOf(getLatitude());
    }

    public void setZoom(float zoom, SettingOp settingOp) {
        if (this.zoom != zoom) {
            this.zoom = zoom;
            settingOp.setDouble(def.PREF_MAPA_ZOOM, (double) zoom);
        }
    }

    float setSpeed(@Nullable Location newLocation) {
        if (newLocation == null) return 0;
        float speed = 0;
        if (newLocation.hasSpeed()) {
            speed = Math.round(newLocation.getSpeed() * 3.6f); // převod na km/h
            setSpeed(speed);
        } else {
            float vzdalenost = (float) def.vzdalenostSouradnic(this, newLocation);
            float cas = (newLocation.getTime() - getTime()) / 1000;
            speed = Math.round(Math.abs(vzdalenost / cas) * 3.6f);
            if (speed > 900f) speed = 0;
            setSpeed(speed);
        }
        return speed;
    }

    public int mGetSpeed() {
        return 0;
    }


    public int iGetProvider() {
        return tabulkaTypZjisteniLokace(getProvider());
    }

    public void setProvider(int typZjisteniLokace) {
        switch (typZjisteniLokace) {
            case def.LOKACE_PROVIDER_ZADNY:
                setProvider("");
                return;
            case def.LOKACE_PROVIDER_GPS:
                setProvider(LocationManager.GPS_PROVIDER);
                return;
            case def.LOKACE_PROVIDER_NETWORK:
                setProvider(LocationManager.NETWORK_PROVIDER);
                return;
        }
    }

    public static int tabulkaTypZjisteniLokace(@Nullable String provider) {
        if (TextUtils.isEmpty(provider)) return def.LOKACE_PROVIDER_ZADNY;
        if (provider.equals(LocationManager.GPS_PROVIDER)) return def.LOKACE_PROVIDER_GPS;
        if (provider.equals(LocationManager.NETWORK_PROVIDER)) return def.LOKACE_PROVIDER_NETWORK;
        return def.LOKACE_PROVIDER_ZADNY;
    }

    public LatLng getLatLng() {
        return new LatLng(getLatitude(), getLongitude());
    }

    public void setLatitude(String latitude) {
        super.setLatitude(def.parseDouble(latitude));
    }

    public void setLongitude(String longitude) {
        super.setLongitude(def.parseDouble(longitude));
    }

    public int getTimeSec() {
        return Math.round(getTime()/1000);
    }
}
