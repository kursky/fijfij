package cz.fijfij.hlavni;

import android.app.Activity;
import android.support.annotation.StringRes;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import cz.fijfij.R;

import java.util.ArrayList;

/**
 * Created by jiri.kursky on 5.8.2016.
 */
public class OvladaciPrvky {
    private ArrayList<Button> btnOvladani;
    private TextView txtInfo;

    public OvladaciPrvky(Activity activity, int infoText) {
        btnOvladani = new ArrayList<>();
        txtInfo = (TextView) activity.findViewById(infoText);
    }

    public void aktivuj() {
        for (Button btn : btnOvladani) {
            btn.setVisibility(View.VISIBLE);
        }
    }

    public void skryj() {
        setText(R.string.cekejte);
        for (Button btn : btnOvladani) {
            btn.setVisibility(View.GONE);
        }
    }

    public void setText(@StringRes int resid) {
        if (txtInfo==null) return;
        txtInfo.setText(resid);
    }

    public void setText(CharSequence txt) {
        txtInfo.setText(txt);
    }

    public void setTextAktivujOvladani(@StringRes int resid) {
        setText(resid);
        aktivuj();
    }
}
