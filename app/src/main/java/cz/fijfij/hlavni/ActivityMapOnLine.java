package cz.fijfij.hlavni;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import org.json.JSONObject;
import cz.fijfij.R;
import cz.fijfij.hlavni.lokace.LatLngItem;
import cz.fijfij.hlavni.service.FijService;

/**
 * Created by jiri.kursky on 07.03.2017
 */

public class ActivityMapOnLine extends BaseActivity implements OnMapReadyCallback {
    private static Osoba osoba;
    private GoogleMap mMap = null;
    private boolean mapIsVisible = false;
    private View llAktivni;
    private View llCekamSpojeni;
    private static LatLngItem latLngItem = null;
    private Marker marker = null;
    private SettingOp settingOp;
    private static final long ONLINE_SMYCKA = 3000;
    private SupportMapFragment mapFragment;
    private ServiceOp serviceOp;
    private boolean bylStart = false;
    private boolean smyckaZastavena = false;

    public ActivityMapOnLine() {
        super(R.layout.map_online);
    }

    private void registerClient() {
        if (serviceOp != null) serviceOp.registerClient();
    }

    // Používá se pri doručení sms
    private ServiceOp.ServiceInt serviceIntStart = new ServiceOp.ServiceInt() {
        @Override
        public void onServiceConnected() {
            registerClient();
            smyckaZastavena = false;
            spustSmyckuSeZpozdenim();
        }

        @Override
        public void onServiceDisconnected() {
            serviceOp.unRegisterClient();
        }

        @Override
        public boolean receivedMessage(Message msg) {
            switch (msg.what) {
                case FijService.MSG_ONLINE_START:
                    int verze = msg.arg1;
                    if (verze < def.C_MIN_VERZE_ONLINE) {
                        zastavZasilani();
                        infoNeaktualniVerze();
                    }
                    break;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        llAktivni = findViewById(R.id.ll_aktivni);
        llCekamSpojeni = findViewById(R.id.ll_cekam_spojeni);
        View txt = findViewById(R.id.txtChybnaVerze);
        txt.setVisibility(View.GONE);

        llAktivni.setVisibility(View.GONE);
        llCekamSpojeni.setVisibility(View.VISIBLE);
        mapIsVisible = false;
        settingOp = new SettingOp(this);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa_online);
        mapFragment.getMapAsync(this);
        bylStart = false;
    }

    public static void factory(Osoba osoba) {
        ActivityMapOnLine.osoba = osoba;
        latLngItem = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (serviceOp == null) {
            serviceOp = new ServiceOp(this, serviceIntStart);
            serviceOp.doBindService();
        }
        mMap = googleMap;
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                //if (mapIsVisible) latLngItem.setZoom(mMap.getCameraPosition().zoom, settingOp);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (serviceOp != null) {
            zastavZasilani();
            serviceOp.unRegisterClient();
            serviceOp.doUnbindService();
            serviceOp = null;
        }
        latLngItem = null;
        mapIsVisible = false;
    }

    private void zastavZasilani() {
        if (serviceOp != null && osoba != null) serviceOp.sendMessageToService(FijService.MSG_ONLINE_STOP, osoba.getUserId());
        smyckaZastavena = true;
    }

    private boolean ukazMapu(boolean ano) {
        if (ano) {
            LatLng latLng = latLngItem.getLatLng();
            if (mapIsVisible) {
                if (marker == null) {
                    MarkerOptions markerOptions = new MarkerOptions().position(latLng);
                    marker = mMap.addMarker(markerOptions);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, latLngItem.zoom));
                } else {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, latLngItem.zoom));
                    marker.setPosition(latLng);
                }
            } else {
                if (latLngItem == null) return false;
                if (Math.round(latLng.latitude) == 0) return false;
                llAktivni.setVisibility(View.VISIBLE);
                llCekamSpojeni.setVisibility(View.GONE);
                if (mMap == null) return false;
                getSupportFragmentManager().beginTransaction().show(mapFragment).commit();
                llAktivni.setVisibility(View.VISIBLE);
                llCekamSpojeni.setVisibility(View.GONE);
                if (marker == null) {
                    MarkerOptions markerOptions = new MarkerOptions().position(latLng);
                    marker = mMap.addMarker(markerOptions);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, latLngItem.zoom));
                } else
                    marker.setPosition(latLng);
                mapIsVisible = true;
            }
        } else {
            if (mapIsVisible) {
                getSupportFragmentManager().beginTransaction().hide(mapFragment).commit();
                mapIsVisible = false;
            }
        }
        return mapIsVisible;
    }


    private void onlineSmycka() {
        if (smyckaZastavena) return;
        if (serviceOp == null || !serviceOp.isBounded()) {
            Handler onlineSmyckaHandler = new Handler();
            onlineSmyckaHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    onlineSmycka();
                }
            }, ONLINE_SMYCKA);
            return;
        }
        if (!bylStart) {
            serviceOp.sendMessageToService(FijService.MSG_ONLINE_START, osoba.getUserId());
            bylStart = true;
            Handler onlineSmyckaHandler = new Handler();
            onlineSmyckaHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    onlineSmycka();
                }
            }, ONLINE_SMYCKA);
            return;
        }
        String action = settingOp.getActionString(def.CMD_VRAT_ONLINE);
        action += def.EX_PRITELID + osoba.getUserId();
        long sec=def.getCurrentTimeSec();
        action += def.EX_CAS_PRECTENO + sec;
        AsyncService as = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
                Bundle bundle = def.transferJSON(result);
                if (bundle != null) {
                    String error = bundle.getString(def.RET_ERROR, "");
                    if (TextUtils.isEmpty(error)) {
                        if (bundle.getInt(def.RET_KONEC, 0) != 0) {
                            infoZastaveno();
                            return;
                        }
                        if (bundle.getInt(def.RET_AKTUALNI_STAV, 0) != 0) {
                            if (latLngItem == null) {
                                latLngItem = new LatLngItem(bundle.getString(def.RET_LATITUDE), bundle.getString(def.RET_LONGITUDE),
                                        bundle.getInt(def.RET_TYP_ZJISTENI_LOKACE, 0));
                            } else {
                                latLngItem.setLatitude(bundle.getString(def.RET_LATITUDE));
                                latLngItem.setLongitude(bundle.getString(def.RET_LONGITUDE));
                                latLngItem.setProvider(bundle.getInt(def.RET_TYP_ZJISTENI_LOKACE, 0));
                            }
                            latLngItem.setTime(bundle.getLong(def.RET_TIME));
                            ukazMapu(true);
                        }
                    }
                }
                spustSmyckuSeZpozdenim();
            }
        }, this, def.CMD_VRAT_ONLINE, action);
        as.execute();
    }

    private void spustSmyckuSeZpozdenim() {
        Handler onlineSmyckaHandler = new Handler();
        onlineSmyckaHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                onlineSmycka();
            }
        }, ONLINE_SMYCKA);
    }

    private void infoNeaktualniVerze() {
        llAktivni.setVisibility(View.GONE);
        llCekamSpojeni.setVisibility(View.VISIBLE);
        View txt = findViewById(R.id.txtCekamNaSpojeni);
        txt.setVisibility(View.GONE);
        txt = findViewById(R.id.txtChybnaVerze);
        txt.setVisibility(View.VISIBLE);
        def.nastavText(this, R.id.txtChybnaVerze, R.string.pritel_nema_aktualni_verzi, osoba.getNick());
    }

    private void infoZastaveno() {
        llAktivni.setVisibility(View.GONE);
        llCekamSpojeni.setVisibility(View.VISIBLE);
        View txt = findViewById(R.id.txtCekamNaSpojeni);
        txt.setVisibility(View.GONE);
        TextView txtChybnaVerze = (TextView) findViewById(R.id.txtChybnaVerze);
        txtChybnaVerze.setVisibility(View.VISIBLE);
        txtChybnaVerze.setText(R.string.preruseno);
    }

    @Override
    public void nastavActionBar() {
        if (actionBar == null) return;
        actionBar.setSubtitle(osoba.getNick());
    }
}
