package cz.fijfij.hlavni.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import cz.fijfij.hlavni.SettingOp;
import cz.fijfij.hlavni.def;


/**
 * Created by jiri.kursky on 24.03.2017
 */

/**
 * This BroadcastReceiver automatically (re)starts the alarm when the device is
 * rebooted. This receiver is set to be disabled (android:enabled="false") in the
 * application's manifest file. When the user sets the alarm, the receiver is enabled.
 * When the user cancels the alarm, the receiver is disabled, so that rebooting the
 * device will not trigger this receiver.
 */
// BEGIN_INCLUDE(autostart)
public class BootReceiver extends BroadcastReceiver {
    AlarmReceiver alarm = new AlarmReceiver();

    @Override
    public void onReceive(Context context, Intent intent) {
        SettingOp settingOp=new SettingOp(context);
        if (settingOp.getBoolean(def.PREF_PRVNI_SPUSTENI, true) || settingOp.getBoolean(def.PREF_REGISTRACE_ZRUSENA, true)) return;
        boolean jeRidici = settingOp.getBoolean(def.PREF_JE_RIDICI, false);
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED") && !jeRidici) {
            alarm.setAlarm(context);
            alarm.setEnsurance(context);
        }
    }
}
//END_INCLUDE(autostart)
