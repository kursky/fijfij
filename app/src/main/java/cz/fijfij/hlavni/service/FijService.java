/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.location.Location;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.widget.Toast;


import cz.fijfij.R;
import cz.fijfij.hlavni.AsyncService;
import cz.fijfij.hlavni.BufferOp;
import cz.fijfij.hlavni.Identifikace;
import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.Osoba;
import cz.fijfij.hlavni.ParametrOp;
import cz.fijfij.hlavni.PrateleOp;
import cz.fijfij.hlavni.SettingOp;
import cz.fijfij.hlavni.SpojeniOp;
import cz.fijfij.hlavni.Vystraha;
import cz.fijfij.hlavni.def;
import cz.fijfij.hlavni.lokace.LatLngItem;
import cz.fijfij.hlavni.lokace.LocationService;
import cz.fijfij.hlavni.sms.SmsItem;
import cz.fijfij.hlavni.zapouzdreno.SmsControl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by jiri.kursky on 5.8.2016
 * Hlavni sluzba
 */
public class FijService extends Service {
    public static final int MSG_OK = 20;
    public static final int MSG_NOT_RUNNING = 30;
    public static final int MSG_INTERVAL_HLAVNI_SMYCKA = 40;
    public static final int MSG_TEST_SERVICE = 50; // Test na beh byl proveden ze splashe, tzn, ze sluzba nebyla spuštěna po restartu
    public static final int MSG_REGISTER_CLIENT = 60;
    public static final int MSG_UNREGISTER_CLIENT = 70;
    public static final int MSG_NOVA_SMS = 80;
    public static final int MSG_PRATELE_NACTENI = 100;
    public static final int MSG_ZMENA_PARAMETRU = 120;
    public static final int MSG_NEAUTORIZOVAN = 130;
    public static final int MSG_CHYBA_KOMUNIKACE = 140;
    public static final int MSG_NENI_SIT = 150;
    public static final int MSG_POTVRZENI_SMS = 160;
    public static final int MSG_KONEC_POTVRZENI_SMS = 170;
    private static final int MSG_POTVRZENI_SMS_SELHALO = 180;
    public static final int MSG_START_MAIN = 190;
    public static final int MSG_AKTUALIZACE_PRATEL = 200;
    public static final int MSG_ZMENA_PARAMETRU_PRITEL = 210;
    public static final int MSG_SLUZBA_PRIPRAVENA = 220;
    public static final int MSG_STOP_SERVICE = 230;
    public static final int MSG_NASTAV_BOD = 250;
    public static final int MSG_PRITEL_PRIDAN = 260;
    public static final int MSG_RYCHLOST = 270;
    public static final int MSG_ONLINE_STOP = 280;
    public static final int MSG_ONLINE_START = 290;
    public static final int MSG_DEBUG = 300;
    public static final int MSG_MAPA_PRIPRAVENA = 310;
    public static final int MSG_UDAJE_MAPY = 320;
    public static final int MSG_CHECK_SERVER = 330; // Zavolání hlavní procedury

    private static final long ONLINE_SMYCKA = 1000;

    public static final int LOGIN_NOTIFY_ID = 110;
    public static final int NOTIFY_WARNING = 120;


    private LocationService locationService = null;
    private BufferOp bufferOp;
    private SettingOp settingOp;
    public static boolean isRunning = false;
    private boolean zmenaParametru = false;
    private AsyncService as;
    public PrateleOp prateleOp;
    private SmsControl smsControl;
    private Identifikace identifikace;
    private String potvrzeniSMS = null;
    private SmsItem globalSmsItem;
    private boolean prateleNacteni = false;
    private static boolean notificationVisible = false;
    private boolean baterieNeregistrovana = true;
    private boolean zpracovavamDotaz = false;
    private Vystraha vystraha;
    private Context context;

    private ArrayList<Messenger> mClients = new ArrayList<>();

    public static final String TAG = FijService.class.getSimpleName();

    private IncomingHandler incomingHandler = new IncomingHandler(new IncomingHandler.HandleMessage() {
        @Override
        public void onHandleMessage(Message msg) {
            Message answer;
            int myAnswer;
            Bundle bundle;
            switch (msg.what) {
                case MSG_TEST_SERVICE:
                    myAnswer = MSG_NOT_RUNNING;
                    if (isRunning) myAnswer = MSG_OK;
                    answer = Message.obtain(null, myAnswer);
                    try {
                        if (msg.replyTo != null) msg.replyTo.send(answer);
                    } catch (RemoteException e) {
                        MyLog.e(FijService.TAG, e);
                    }
                    break;

                case MSG_START_MAIN:
                    startLocationService();
                    break;

                case MSG_ONLINE_STOP:
                    zastavOdesilani(msg.arg1);
                    break;

                case MSG_ONLINE_START:
                    startOdesilani(msg.arg1);
                    break;
                case MSG_REGISTER_CLIENT:
                    if (!mClients.contains(msg.replyTo)) {
                        mClients.add(msg.replyTo);
                    }
                    sendMessageToUI(MSG_REGISTER_CLIENT);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_NASTAV_BOD:
                    nastavBod(msg.arg1);
                    break;
                case MSG_STOP_SERVICE:
                    isRunning = false;
                    myAnswer = MSG_STOP_SERVICE;
                    answer = Message.obtain(null, myAnswer);
                    try {
                        if (msg.replyTo != null) msg.replyTo.send(answer);
                    } catch (RemoteException e) {
                        MyLog.e(TAG, e);
                    }
                    break;
                case MSG_INTERVAL_HLAVNI_SMYCKA:
                    AlarmReceiver alarm = new AlarmReceiver();
                    alarm.setAlarm(context);
                    break;
                case MSG_CHECK_SERVER:
                    checkServer();
                    break;
                case MSG_ZMENA_PARAMETRU:
                    bundle = msg.getData();
                    String klicZmeny = bundle.getString(def.PREF_KLIC_ZMENY);
                    if (klicZmeny == null) klicZmeny = "";
                    if (klicZmeny.equals(def.PREF_NEZOBRAZOVAT_BEH)) {
                        boolean nezobrazovatBeh = bundle.getBoolean(def.PREF_NEZOBRAZOVAT_BEH, false);
                        settingOp.putBoolean(def.PREF_NEZOBRAZOVAT_BEH, nezobrazovatBeh);
                        if (!nezobrazovatBeh && !notificationVisible) showNotification();
                    } else {
                        settingOp.uzivatelZNastaveni(def.osoba, ParametrOp.PARS_ADMIN);
                        settingOp.nastavRazitko();
                        def.osoba.putBundle(msg.getData());
                        settingOp.sendUserParametres(def.osoba, true, null);
                        zmenaParametru = true;
                    }
                    break;
                case MSG_ZMENA_PARAMETRU_PRITEL:
                    bundle = msg.getData();

                    Osoba pritel = prateleOp.upravPritele(bundle);
                    if (pritel != null) settingOp.sendUserParametres(pritel, true, null);
                    prateleOp.nactiOffline();
                    break;
                case MSG_AKTUALIZACE_PRATEL:
                    prateleOp.execute();
                    sendMessageToUI(MSG_AKTUALIZACE_PRATEL);
                    break;
                case MSG_POTVRZENI_SMS:
                    Bundle b = msg.getData();
                    potvrzeniSMS = b.getString("phoneNumber");
                    sendMessageToUI(MSG_POTVRZENI_SMS);
                    break;
                case MSG_KONEC_POTVRZENI_SMS:
                    potvrzeniSMS = null;
                    break;
                case MSG_UDAJE_MAPY:
                    sendMessageToUI(MSG_UDAJE_MAPY, msg.arg1, msg.getData());
                    break;
                case MSG_MAPA_PRIPRAVENA:
                    sendMessageToUI(MSG_MAPA_PRIPRAVENA);
                    break;
            }
        }
    });


    private Messenger mMessenger;

    /**
     * Konečné volání identifikace původně voláno ze Splash
     */
    private Identifikace.CallBackListener identBackListener = new Identifikace.CallBackListener() {
        @Override
        public void onFinished(Osoba osoba, int status) {
            switch (status) {
                case Identifikace.ST_OK: // nejlepší stav
                    prateleOp.execute();
                    if (!def.jeRidici() && baterieNeregistrovana) {
                        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
                        BatteryReceiver batteryReceiver = new BatteryReceiver(settingOp);
                        registerReceiver(batteryReceiver, filter);
                        baterieNeregistrovana = false;
                    }
                    break;
                case Identifikace.ST_NEAUTORIZOVAN: //
                    sendMessageToUI(MSG_NEAUTORIZOVAN);
                    break;
                case Identifikace.ST_CHYBA_BEHEM_KOMUNIKACE:
                    prateleOp.execute();
                    sendMessageToUI(MSG_CHYBA_KOMUNIKACE);
                    break;
                case Identifikace.ST_NENI_SIT:
                    sendMessageToUI(MSG_NENI_SIT);
                    break;
            }
        }
    };

    private PrateleOp.CallBackListener prateleCallBack = new PrateleOp.CallBackListener() {
        @Override
        public void onFinished() {
            smsControl = new SmsControl(prateleOp);
            smsControl.ignorujVsechnySMS();
            sendMessageToUI(MSG_PRATELE_NACTENI);
            sendMessageToUI(MSG_SLUZBA_PRIPRAVENA);
            prateleNacteni = true;
            pokracovaniPoIdentifikaci();
        }
    };


    /**
     * Toto se spouští při bindu
     */
    @Override
    public void onCreate() {
        mMessenger = new Messenger(incomingHandler);
        super.onCreate();
        context = this;
        def.factory(this);
        bufferOp = new BufferOp(this);
        settingOp = new SettingOp(this);
        prateleOp = new PrateleOp(bufferOp, prateleCallBack);
        identifikace = new Identifikace(settingOp, true, identBackListener);
        prateleNacteni = false;
        vystraha = new Vystraha(settingOp);
        if (!settingOp.getBoolean(def.PREF_NEZOBRAZOVAT_BEH, false)) showNotification();
    }

    /***************************************************************/
    /**
     * Odesílání zpráv zaháčkovanému procesu
     */
    protected void sendMessageToUI(int sMessage) {
        for (int i = mClients.size() - 1; i >= 0; i--) {
            try {
                // Send data as an Integer
                mClients.get(i).send(Message.obtain(null, sMessage, 0, 0));

            } catch (RemoteException e) {
                // The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
                mClients.remove(i);
            }
        }
    }

    protected void sendMessageToUI(int sMessage, int arg1, @Nullable Bundle bundle) {
        for (int i = mClients.size() - 1; i >= 0; i--) {
            try {
                // Send data as an Integer
                Message msg = Message.obtain(null, sMessage, arg1, 0);
                if (bundle != null) msg.setData(bundle);
                mClients.get(i).send(msg);

            } catch (RemoteException e) {
                // The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
                mClients.remove(i);
            }
        }
    }

    protected void sendMessageToUI(int sMessage, Bundle bundle) {
        sendMessageToUI(sMessage, 0, bundle);
    }

    protected void sendMessageToUI(int sMessage, int arg1) {
        sendMessageToUI(sMessage, arg1, null);
    }

    protected void sendDebugMessageToUI(String s) {
        Bundle bundle = new Bundle();
        bundle.putString(def.PREF_ZPRAVA, s);
        if (def.debug) sendMessageToUI(MSG_DEBUG, 0, bundle);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    /**
     * Spouští se pri startService
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // Toto nastane jen v případě, že je telefon poprvé zapnut na sms
        // řízeno def.PREF_PRVNI_NAHRANI_SMS

        IntentFilter smsFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        smsFilter.setPriority(1000);
        SMSReceive smsReceive = new SMSReceive(this);
        registerReceiver(smsReceive, smsFilter);
        identifikace.execute();
        settingOp.putBoolean(def.PREF_SLUZBA_NASTAVENA, true);
        return Service.START_STICKY;
    }


    /**
     * Voláno až vše skončí
     */
    private void pokracovaniPoIdentifikaci() {
        isRunning = true;
        if (!def.jeRidici()) {
            AlarmReceiver alarm = new AlarmReceiver();
            alarm.setAlarm(context);
        }
    }

    /**
     * Přečte všechny SMS a zařadí je
     */
    private void vyridZadostiSMS() {
        if (settingOp.getBoolean(def.PREF_SMS_POVOLENO, false) || def.jeRidici()) {
            if (smsControl.vyridNevyrizeneSMS() && def.jeRidici()) {

                // Odesílá informaci, pokud je otevřená ActivityOsoba
                sendMessageToUI(MSG_NOVA_SMS);
            }

        }
    }


    /******************************************************************************
     * Část zabývající se online přenosem - platí jen pro přítele
     */
    private boolean onLineOdesilani = false;
    private AsyncService.TaskListener checkBack = new AsyncService.TaskListener() {
        @Override
        public void onFinished(String nameAction, JSONObject result) {
            Bundle b = def.transferJSON(result);
            if (!def.jeRidici()) {
                if (b != null) {
                    int onlineControl = b.getInt(def.RET_ONLINE_CONTROL, 0);
                    boolean odesilat = onlineControl != 0;
                    if (!odesilat) onLineOdesilani = false;
                    if (odesilat && !onLineOdesilani) {
                        sendDebugMessageToUI("Zahájeno on line odesílání");
                        Handler onlineSmyckaHandler = new Handler();
                        onlineSmyckaHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startOdesilejOnline();
                            }
                        }, ONLINE_SMYCKA);
                        return;
                    }
                }
            }
        }
    };

    /**
     * Platí pouze pro řídícího
     */
    private AsyncService.TaskListener checkRidiciBack = new AsyncService.TaskListener() {
        @Override
        public void onFinished(String nameAction, JSONObject result) {
            String pritelId;
            String id;
            Osoba pritel;
            String msgText;
            int msgId;
            if (result != null) {
                try {
                    JSONArray warnings = result.getJSONArray(def.RET_WARNINGS);
                    for (int i = 0; i < warnings.length(); ++i) {
                        // Návrat prvku pole
                        JSONObject obj = warnings.getJSONObject(i);
                        id = obj.getString(def.RET_ID);
                        zapisPrectenehoAlertu(id);
                        msgId = def.parseInt(obj.getString(def.RET_MSG_ID));
                        switch (msgId) {
                            case def.W_PREKROCENA_RYCHLOST:
                                pritelId = obj.getString(def.RET_PRITELID);
                                pritel = prateleOp.vratPriteleId(pritelId);
                                if (pritel == null) break;
                                notificationAlertRychlost(NOTIFY_WARNING, "Překročená rychlost:" + pritel.getNick());
                                break;
                            case def.W_OBECNA:
                                msgText = obj.getString(def.RET_MSG_TEXT);
                                notificationAlertObecna(NOTIFY_WARNING, msgText);
                                break;
                        }
                    }
                } catch (Exception e) {
                    return;
                }
            }
        }
    };

    /**
     * Nastaví na serveru, že alert byl zapsán
     */
    private void zapisPrectenehoAlertu(String sid) {
        String action = settingOp.getActionString(def.CMD_PRECTENY_ALERT);
        action += def.EX_ALERT_ID + sid;
        AsyncService as = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {

            }
        }, this, def.CMD_PRECTENY_ALERT, action);
        as.execute();
    }

    /**
     * Odesílání on-line
     * Je potřeba zahájit, jinak se čekalo na změnu lokace a to je nesmysl - dělalo to dlouhé prodlevy
     */
    private void startOdesilejOnline() {
        String action = settingOp.getActionString(def.CMD_ONLINE_START_PRITEL);
        AsyncService as = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
                onLineOdesilani = true;
                smyckaOdesilejOnline();
            }
        }, this, def.CMD_ONLINE_START_PRITEL, action);
        as.execute();
    }


    /**
     * Platí pouze pro přítele, vysoká frekvence odesílání, ale jen v případě, že se data v lokaci změnily
     */
    private void odesilejOnline() {
        String action;
        if (!onLineOdesilani || zpracovavamDotaz) return;
        zpracovavamDotaz = true;
        action = settingOp.getActionString(def.CMD_DATA_ONLINE);
        AsyncService as = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
                Bundle bundle = def.transferJSON(result);
                if (bundle != null) {
                    onLineOdesilani = bundle.getInt(def.RET_KONEC, 0) == 0;
                } else {
                    zpracovavamDotaz = false;
                    smyckaOdesilejOnline();
                    return;
                }


                // Je potřeba ověřit, jestli je uživatel čte
                int casZalozeno = bundle.getInt(def.RET_CAS_ZADANI, 0);
                int casSouradnic = bundle.getInt(def.RET_CAS_SOURADNIC, 0);
                int casPrecteno = bundle.getInt(def.RET_CAS_PRECTENO, 0);
                int prodlevaPrecteni = 0;
                int prodlevaZalozeni = 0;
                if (casPrecteno == 0) {
                    prodlevaZalozeni = casSouradnic - casZalozeno;
                } else if (casSouradnic != 0) {
                    prodlevaPrecteni = casSouradnic - casPrecteno;
                }
                zpracovavamDotaz = false;
                if (prodlevaPrecteni > 300) {
                    sendDebugMessageToUI("Překročena prodleva přečtení - zastavuji");
                    Handler onlineSmyckaHandler = new Handler();
                    onlineSmyckaHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            zastavOdesilani(def.parseInt(settingOp.getString(def.PREF_USER_ID)));
                        }
                    }, ONLINE_SMYCKA);
                    return;
                }

                smyckaOdesilejOnline();
            }
        }, this, def.CMD_VRAT_ONLINE, action);
        as.execute();

    }

    private void smyckaOdesilejOnline() {
        Handler onlineSmyckaHandler = new Handler();
        onlineSmyckaHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                odesilejOnline();
            }
        }, ONLINE_SMYCKA);
    }

    /**
     * Platí pouze pro řídícího a výjimečně pro člena, pokud selže čas - a je ve službě, protože aktivita může mezitím zmizet
     *
     * @param userId - přítele
     */
    void zastavOdesilani(int userId) {
        String action = settingOp.getActionString(def.CMD_ONLINE_STOP);
        action += def.EX_PRITELID + userId;
        AsyncService as = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
            }
        }, this, def.CMD_ONLINE_STOP, action);
        as.execute();
    }

    /**
     * Platí pouze pro řídícího a upozorňuje přítele, že má začít odesílat ve zvýšené frekvenci
     * Vrací současnou verzi přítele
     *
     * @param userId - přítele
     */
    void startOdesilani(int userId) {
        String action = settingOp.getActionString(def.CMD_ONLINE_START);
        action += def.EX_PRITELID + userId;
        AsyncService as = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
                Bundle bundle = def.transferJSON(result);
                int verze = 0;
                if (bundle != null) verze = bundle.getInt(def.RET_VERSION_CODE, 0);
                sendMessageToUI(MSG_ONLINE_START, verze, null);
            }
        }, this, def.CMD_ONLINE_START, action);
        as.execute();
    }
    /*
     * Konec online části
     */

    /********************************************************************
     * Pravidelně se opakující procedura - nadřazená je runCheckServer
     */


    private void checkServer() {
        sendDebugMessageToUI("Check server");
        try {
            startLocationService();
            smsControl.registrujVsechnySMS();
            vyridZadostiSMS(); // i když je volaný událostí, někdy to prostě nezabere nebo služba nemusí běžet
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }

        // Odesílání zásadní informace o lokaci, pokud není řídící a je povolená síť
        if (!def.jeRidici() && def.isNetworkAllowed(settingOp) && !settingOp.getBoolean(def.PREF_POUZE_SMS, false)) {
            as = new AsyncService(checkBack, this, def.CMD_CHECK);
            as.execute();
            return;
        }

        if (def.jeRidici()) {
            as = new AsyncService(checkRidiciBack, this, def.CMD_CHECK);
            as.execute();
            return;
        }

        // Odešle parametry na server
        if (zmenaParametru && def.isNetworkAllowed(settingOp)) {
            zmenaParametru = false;
            settingOp.sendUserParametres(def.osoba, false, null);
        }
    }

    @Override
    public void onDestroy() {
        isRunning = false;
        if (locationService != null) locationService.close();
        locationService = null;
        Toast.makeText(this, "service onDestroy", Toast.LENGTH_LONG).show();
        sendBroadcast(new Intent("YouWillNeverKillMe"));
        super.onDestroy();
    }

    private AsyncService.TaskListener userUdaje = new AsyncService.TaskListener() {
        @Override
        public void onFinished(String nameAction, JSONObject result) {
            if (result == null) {
                sendMessageToUI(MSG_POTVRZENI_SMS_SELHALO);
                return;
            }
            try {
                String pritelID = result.getString("PRITELID");
                if (TextUtils.isEmpty(pritelID)) {
                    sendMessageToUI(MSG_POTVRZENI_SMS_SELHALO);
                } else {
                    globalSmsItem.setOsobaUserId(pritelID);
                    smsControl.odesliZadostPratelstvi(globalSmsItem.getCislo());
                    sendMessageToUI(MSG_KONEC_POTVRZENI_SMS);
                    sendMessageToUI(MSG_AKTUALIZACE_PRATEL);
                }
            } catch (Exception e) {
                MyLog.e(TAG, e);
                sendMessageToUI(MSG_POTVRZENI_SMS_SELHALO);
            }
        }
    };

    /**
     * Ovladač na příchozí SMS
     */
    class SMSReceive extends BroadcastReceiver {
        SmsControl smsControl;

        public SMSReceive(Context context) {
            super();
            smsControl = new SmsControl(context);
        }

        /**
         * Vlastní přijetí SMS
         */
        @Override
        public void onReceive(Context context, Intent intent) {
            // Kontrola jestli nepřichází SMS týkající se párování
            Bundle bundle = intent.getExtras();
            if (def.jeRidici()) {
                if (smsControl.pridaniPritele(bundle, new SpojeniOp.OdeslanoServeru() {
                    @Override
                    public void nastalaChyba() {

                    }

                    @Override
                    public void vseOK(Bundle bundle) {
                        sendMessageToUI(MSG_PRITEL_PRIDAN);
                    }
                })) return;
            }

            if ((!settingOp.getBoolean(def.PREF_SMS_POVOLENO, false) && !def.jeRidici()) || !prateleNacteni) {
                return;
            }
            if (def.osoba == null) { // špatně nastavený začátek
                def.osoba = new Osoba();
                settingOp.uzivatelZNastaveni(def.osoba, def.jeRidici() ? ParametrOp.PARS_RIDICI : ParametrOp.PARS_CLEN);
            }
            SmsMessage currentMessage;
            Osoba osoba;
            ArrayList<SmsItem> arPrichozi = new ArrayList<>();
            SmsItem smsItem = null;
            int typSMS;
            try {
                if (bundle != null) {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    final Object[] pdusObj = (Object[]) bundle.get("pdus");
                    if (pdusObj == null) return;
                    for (int i = 0; i < pdusObj.length; i++) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            String format = bundle.getString("format");
                            currentMessage = SmsMessage.createFromPdu((byte[]) pdus[i], format);
                        } else {
                            currentMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        }
                        String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                        String message = currentMessage.getDisplayMessageBody();
                        osoba = prateleOp.vratDleTelefonu(phoneNumber);

                        if (osoba != null) {
                            if (smsItem == null) smsItem = new SmsItem(context);
                            typSMS = SmsItem.parseMessage(message, smsItem);
                            if (def.jeRidici()) {
                                if (typSMS == SmsItem.SMS_ODPOVED_LOKACE) {
                                    smsItem.setParams(osoba, currentMessage.getTimestampMillis());
                                    arPrichozi.add(smsItem);
                                    smsItem = null;
                                }
                            } else {
                                if (typSMS == SmsItem.SMS_VRAT_LOKACI) {
                                    smsItem.setParams(osoba, currentMessage.getTimestampMillis());
                                    arPrichozi.add(smsItem);
                                    smsItem = null;
                                }
                            }
                        } else if (!TextUtils.isEmpty(potvrzeniSMS)) {
                            globalSmsItem = new SmsItem(context);
                            typSMS = SmsItem.parseMessage(message, globalSmsItem);
                            if (typSMS == SmsItem.SMS_POTVRZENI_SMS) {
                                if (def.jeRidici()) {
                                    sendMessageToUI(MSG_KONEC_POTVRZENI_SMS);
                                } else {
                                    String action = settingOp.getActionString(def.CMD_SPOJ_UZIVATELE);
                                    action += "&phoneNumber=" + def.phoneNumberStandard(phoneNumber);
                                    globalSmsItem.setCislo(phoneNumber);
                                    AsyncService asService = new AsyncService(userUdaje, context, def.CMD_SPOJ_UZIVATELE, action);
                                    asService.execute();
                                }
                            }
                        }
                    }
                }

            } catch (Exception e) {
                MyLog.e("SmsReceiver", "Exception smsReceiver" + e);
            }
            for (SmsItem prichoziItem : arPrichozi) {
                if ((def.jeRidici())) {
                    smsControl.zapisOdpovedSMS(prichoziItem);
                } else {
                    if (smsControl.addZadostSMS(prichoziItem)) {
                        smsControl.odesliLokaci(prichoziItem);
                        smsControl.zapisOdpovedSMS(prichoziItem);
                    }
                }
            }
            if (arPrichozi.size() > 0) sendMessageToUI(MSG_NOVA_SMS);
        }
    }

    /**
     * Startuje lokaci pouze pro podřízené telefony, řídící polohu nehlásí
     */
    private boolean alertWindowActive = false;

    /**
     * Zde se sleduje překročení rychlosti
     */
    protected void startLocationService() {
        if (locationService == null && def.osoba != null) {
            if (!def.jeRidici()) {
                locationService = new LocationService(getBaseContext(), null);
                locationService.locationListener = new LocationService.LocationListener() {
                    @Override
                    public void onChangeLocation(Location location, boolean pretecenParametr) {
                        int i = Math.round(location.getSpeed());
                        int maxRychlost = settingOp.getIntFromString(def.PREF_RYCHLOSTNI_LIMIT, 0);
                        if (maxRychlost > 0) {
                            if (i > maxRychlost && !alertWindowActive) {
                                String rychlost = i + " km/h";
                                vystraha.zapisVystrahu(def.W_PREKROCENA_RYCHLOST, def.userId(), rychlost);
                            }
                        }
                        sendMessageToUI(MSG_RYCHLOST, i);
                        if (pretecenParametr)
                            sendDebugMessageToUI("Změna lokace s přetečením");
                        else
                            sendDebugMessageToUI("Změna lokace neprošlo filtrem");
                    }
                };
            }
        }
    }

    /**
     * Bod na mapě dostává název a další z tabulky TBL_AKTIVITY_BODU
     */
    private void nastavBod(int pritelId) {
        bufferOp.openDatabase(BufferOp.DB_WRITABLE);
        Cursor res = null;
        try {
            // zjistíme je-li změna
            res = bufferOp.mDatabase.rawQuery("select * from " + BufferOp.TBL_AKTIVITY_BODU + " where " +
                    def.PREF_USER_ID + "='" + pritelId + "' and " + def.PREF_ZMENA + "=1", null);
            if (res.moveToFirst()) {
                res.close();
                // pokud ano, vytáhni vše
                res = bufferOp.mDatabase.rawQuery("select * from " + BufferOp.TBL_AKTIVITY_BODU + " where " +
                        def.PREF_USER_ID + "='" + pritelId + "'", null);
                res.moveToFirst();
                String latitude = "", longitude = "", nazev = "";
                while (!res.isAfterLast()) {
                    if (!TextUtils.isEmpty(latitude)) {
                        latitude += def.AR_GLUE;
                        longitude += def.AR_GLUE;
                        nazev += def.AR_GLUE;
                    }
                    LatLngItem latLngItem = new LatLngItem(res.getString(res.getColumnIndex(def.PREF_LATITUDE)),
                            res.getString(res.getColumnIndex(def.PREF_LONGITUDE)), bufferOp);
                    latitude += latLngItem.getLatitude();
                    longitude += latLngItem.getLongitude();
                    nazev += latLngItem.vratNazevBodu();
                    res.moveToNext();
                }
                if (!TextUtils.isEmpty(latitude)) {
                    ContentValues values = new ContentValues();
                    values.put(def.PREF_ZMENA, 0);
                    bufferOp.mDatabase.update(BufferOp.TBL_AKTIVITY_BODU, values,
                            def.PREF_USER_ID + "='" + pritelId + "'", null);

                    String action = def.EX_ACTION + def.CMD_NASTAV_BOD;
                    action += def.EX_PRITELID + pritelId;
                    action += def.EX_AR_LAT + latitude;
                    action += def.EX_AR_LNG + longitude;
                    action += def.EX_AR_NAZEV_BODU + nazev;

                    AsyncService asService = new AsyncService(new AsyncService.TaskListener() {
                        @Override
                        public void onFinished(String nameAction, JSONObject result) {
                        }
                    }, this, "nastav_bod", action);
                    asService.execute();
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        bufferOp.closeDatabase(res);
    }

    /*******************************************************/
    /**
     * Ukáže notifikaci služby a zajišťuje aktivaci stranky
     */
    private void showNotification() {
        Context context = this.getApplicationContext();
        Intent notificationIntent = new Intent(context, def.getMainActivity(context));
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.setAction("android.intent.action.MAIN");
        notificationIntent.addCategory(TAG);

        final PendingIntent pi = PendingIntent
                .getActivity(this, def.SERVICE_NOTIFY_ID, notificationIntent, 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentTitle(getString(R.string.app_name)).
                setContentText(getString(R.string.sluzba_podnazev)).
                setSmallIcon(R.mipmap.ic_launcher_service).
                setOngoing(true).
                setContentIntent(pi).
                setAutoCancel(false);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(def.SERVICE_NOTIFY_ID, builder.build());
        notificationVisible = true;
    }

    /**
     * Schová viditelnost služby
     */
    public static void cancelNotification(Context ctx) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(def.SERVICE_NOTIFY_ID);
        notificationVisible = false;
    }

    /**
     * Výstražná notifikace
     */
    private void notificationAlertRychlost(int notifyId, String msg) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        final Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_warning_white_24dp)
                .setContentTitle(def.getText(this, R.string.upozorneni, getString(R.string.app_name)))
                .setContentText(msg)
                .build();
        notification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL | Notification.DEFAULT_SOUND;
        notificationManager.notify(notifyId, notification);
    }

    /**
     * Výstražná notifikace
     */
    private void notificationAlertObecna(int notifyId, String msg) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        final Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_warning_white_24dp)
                .setContentTitle(def.getText(this, R.string.upozorneni, getString(R.string.app_name)))
                .setContentText(msg)
                .build();
        notification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL | Notification.DEFAULT_SOUND;
        notificationManager.notify(notifyId, notification);
    }

    class BatteryReceiver extends BroadcastReceiver {
        private SettingOp settingOp;

        public BatteryReceiver(SettingOp settingOp) {
            super();
            this.settingOp = settingOp;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            // Error checking that probably isn't needed but I added just in case.
            Integer procento;
            if (level == -1 || scale == -1) {
                procento = 50;
            } else {
                procento = Math.round(((float) level / (float) scale) * 100.0f);
            }
            settingOp.putString(def.PREF_BATTERY_PERCENT, procento.toString());
        }
    }
}

/**
 * Toto je součástí služby a je mimo smyčku
 */
class IncomingHandler extends Handler { // Handler of incoming messages from clients
    private HandleMessage handleMessage;

    interface HandleMessage {
        void onHandleMessage(Message msg);
    }

    IncomingHandler(HandleMessage handleMessage) {
        super();
        this.handleMessage = handleMessage;
    }

    @Override
    public void handleMessage(Message msg) {
        handleMessage.onHandleMessage(msg);
    }
}
