package cz.fijfij.hlavni.service;

/**
 * Created by jiri.kursky on 25.03.2017
 * Vytváří token na novém zařízení nebo nová instalace aplikace
 */

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONObject;
import cz.fijfij.hlavni.AsyncService;
import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.def;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        MyLog.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        String action = def.startCmd(def.CMD_ZADEFINOVANY_TOKEN);
        action += def.EX_ANDROID_ID + def.android_id;
        action += def.EX_TOKEN + token;
        AsyncService as = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {

            }
        }, this, def.CMD_ZADEFINOVANY_TOKEN, action);
        as.execute();
    }
}