package cz.fijfij.hlavni.service;

/**
 * Created by jiri.kursky on 26.03.2017.
 */


import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import cz.fijfij.hlavni.base.MyLog;

public class MyJobService extends JobService {

    private static final String TAG = "MyJobService";

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        MyLog.d(TAG, "Performing long running task in scheduled job");
        // TODO(developer): add long running task here.
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }

}

