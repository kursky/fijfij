package cz.fijfij.hlavni.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Message;
import android.os.PowerManager;
import android.os.SystemClock;

import java.util.Calendar;

import cz.fijfij.hlavni.ServiceOp;
import cz.fijfij.hlavni.SettingOp;
import cz.fijfij.hlavni.def;

/**
 * Created by jiri.kursky on 24.03.2017
 */

public class AlarmReceiver extends BroadcastReceiver {
    // The app's AlarmManager, which provides access to the system alarm services.
    private AlarmManager alarmMgr;
    // The pending intent that is triggered when the alarm fires.
    private PendingIntent alarmIntent;
    private static ServiceOp serviceOp = null;
    private Intent intent;
    static final String TAG = AlarmReceiver.class.getSimpleName();
    private PowerManager.WakeLock wakeLock;
    private static long delayTime;
    private static boolean serviceWasOK = false;

    ///////////////////////////////////////////////////////////
    // Komunikace se službou
    ServiceOp.ServiceInt serviceInt = new ServiceOp.ServiceInt() {

        @Override
        public void onServiceConnected() {
            serviceOp.registerClient();
            serviceOp.sendMessageToService(FijService.MSG_TEST_SERVICE);
        }

        @Override
        public void onServiceDisconnected() {
            serviceOp.unRegisterClient();
        }

        /**
         * Návrat ze služby
         */
        @Override
        public boolean receivedMessage(Message msg) {
            switch (msg.what) {
                case FijService.MSG_NOT_RUNNING:
                    // Služba dosud není zaháčkovaná
                    // tzn. je zadefinovaná, ale neproběhl startService
                    serviceOp.startService();
                    break;
                case FijService.MSG_OK:
                    serviceWasOK = true;
                    serviceOp.sendMessageToService(FijService.MSG_CHECK_SERVER);
                    serviceOp.doUnbindService();
                    if (wakeLock != null) wakeLock.release();
                    break;
            }
            return false;
        }
    };


    @Override
    public void onReceive(Context context, Intent intent) {
        // Pro řídícího vyřazuji jakékoli časovače
        if (def.jeRidici()) return;

        // Zbytek pro hlášení podřízeného
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        wakeLock.setReferenceCounted(false);
        long watchDog = 60 * 1000;
        if (watchDog > delayTime) watchDog = delayTime;
        wakeLock.acquire(watchDog);
        serviceOp.doBindService();
    }

    public void setAlarm(Context context) {
        // Pro řídícího vyřazuji jakékoli časovače
        // Pozor na tento krok v budoucnosti
        SettingOp settingOp = new SettingOp(context);
        if (def.jeRidici() || settingOp.getBoolean(def.PREF_REGISTRACE_ZRUSENA, true)) return;
        
        if (serviceOp == null) serviceOp = new ServiceOp(context, serviceInt);
        alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        intent = new Intent(context, AlarmReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        delayTime = settingOp.getIntFromString(def.PREF_INTERVAL_HLAVNI_SMYCKA, def.PREF_MAX_HLAVNI_SMYCKA) * 1000;

        alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + 500, delayTime, alarmIntent);


        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        // Enable {@code SampleBootReceiver} to automatically restart the alarm when the
        // device is rebooted.
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

    }


    /**
     *
     * Pro jistotu každé ráno v 6:00
     * @param context
     */
    public void setEnsurance(Context context) {
        // Pro řídícího vyřazuji jakékoli časovače
        // Pozor na tento krok v budoucnosti

        SettingOp settingOp = new SettingOp(context);
        if (def.jeRidici() || settingOp.getBoolean(def.PREF_REGISTRACE_ZRUSENA, true)) return;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        // Set the alarm's trigger time to 6:00 a.m.
        calendar.set(Calendar.HOUR_OF_DAY, 6);
        calendar.set(Calendar.MINUTE, 00);

        if (serviceOp == null) serviceOp = new ServiceOp(context, serviceInt);
        alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        intent = new Intent(context, AlarmReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntent);

        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        // Enable {@code SampleBootReceiver} to automatically restart the alarm when the
        // device is rebooted.
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    // BEGIN_INCLUDE(cancel_alarm)
    public void cancelAlarm(Context context) {
        // If the alarm has been set, cancel it.
        alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        intent = new Intent(context, AlarmReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        if (alarmMgr!= null) {
            alarmMgr.cancel(alarmIntent);
        }
    }
    // END_INCLUDE(cancel_alarm)
}
