/*
 * Copyright (c) 2017 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;

import cz.fijfij.hlavni.sms.SmsItem;

/**
 * Created by jiri.kursky on 25.01.2017
 */


public class SmsParovani extends BroadcastReceiver {
    private PritelHandshake pritelHandshake = null;

    public SmsParovani(PritelHandshake pritelHandshake) {
        super();
        this.pritelHandshake = pritelHandshake;
    }


    interface PritelHandshake {
        void onPridejRodice(SmsItem smsItem);
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        SmsItem smsItem = new SmsItem(context);
        Bundle bundle = intent.getExtras();
        if (bundle == null) return;
        if (!def.jeRidici()) {
            pridejRodice(bundle, smsItem);
        }
    }

    void pridejRodice(Bundle bundle, SmsItem smsItem) {
        SmsMessage currentMessage;
        Object[] pdus = (Object[]) bundle.get("pdus");
        final Object[] pdusObj = (Object[]) bundle.get("pdus");
        int typSMS;
        for (int i = 0; i < pdusObj.length; i++) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                String format = bundle.getString("format");
                currentMessage = SmsMessage.createFromPdu((byte[]) pdus[i], format);
            } else {
                currentMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
            }
            String message = currentMessage.getDisplayMessageBody();
            typSMS = SmsItem.parseMessage(message, smsItem);
            if (typSMS == SmsItem.SMS_CHCI_SE_SPOJIT) {
                String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                smsItem.setCislo(phoneNumber);
                if (pritelHandshake != null) {
                    pritelHandshake.onPridejRodice(smsItem);
                }
                return;
            }
        }
    }
}
