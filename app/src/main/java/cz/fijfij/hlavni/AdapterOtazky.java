/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import cz.fijfij.R;
import cz.fijfij.hlavni.base.MyLog;

/**
 * Created by jiri.kursky on 07.11.2016.
 * Napojený adapter na Otazky
 */

class AdapterOtazky extends ArrayAdapter<String> {
    private final ProverPristupy proverPristupy;
    private Activity activity;
    private static final String TAG = "AO";


    AdapterOtazky(Activity activity, ProverPristupy proverPristupy) {
        super(activity, R.layout.list_parametry, proverPristupy.vratHlavicky(activity));
        this.proverPristupy = proverPristupy;
        this.activity = activity;
    }

    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_parametry, null, true);

        try {
            View prepinac=rowView.findViewById(R.id.prepinac);
            prepinac.setVisibility(View.GONE);
            TextView txtNazevParametru = (TextView) rowView.findViewById(R.id.txtNazevParametru);
            txtNazevParametru.setText(proverPristupy.vratHlavicku(position));
            TextView txtSubNazevParametru = (TextView) rowView.findViewById(R.id.txtSubNazevParametru);
            txtSubNazevParametru.setText(proverPristupy.vratVysvetleni(position));
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }

        return rowView;
    }
}

