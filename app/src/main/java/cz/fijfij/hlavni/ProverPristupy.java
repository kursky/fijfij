package cz.fijfij.hlavni;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;

import cz.fijfij.R;

/**
 * Created by jiri.kursky on 01.03.2017
 */

public class ProverPristupy {
    static final int P_READ_CONTACTS = 10;
    static final int P_SEND_SMS = 20;
    static final int P_READ_SMS = 30;
    static final int P_ACCESS_FINE_LOCATION = 40;
    private ArrayList<Integer> arDlgTitle = new ArrayList<>();
    private ArrayList<String> arHlavicky = new ArrayList<>();
    private ArrayList<PristupItem> arPristupItem = new ArrayList<>();
    private Class<?> cls;
    private boolean novaVerze = true;

    private Activity activity;


    public ProverPristupy(Activity activity, SettingOp settingOp) {
        this.activity = activity;
        PristupItem pristupItem;

        addDefinition(new PristupItem(Manifest.permission.SEND_SMS, def.PREF_PRAVA_SEND_SMS,
                P_SEND_SMS, R.string.title_permission_sms, R.string.confirm_sms, R.string.why_access_sms, settingOp));
        addDefinition(new PristupItem(Manifest.permission.READ_CONTACTS, def.PREF_PRAVA_READ_CONTACTS,
                P_READ_CONTACTS, R.string.title_permission_contacts, R.string.confirm_contacts, R.string.why_access_contacts, settingOp));
        if (!settingOp.getBoolean(def.PREF_JE_RIDICI, false)) {
            pristupItem = addDefinition(new PristupItem(Manifest.permission.ACCESS_FINE_LOCATION, def.PREF_PRAVA_ACCESS_FINE_LOCATION,
                    P_ACCESS_FINE_LOCATION, R.string.title_access_fine_location, R.string.confirm_access_fine_location, R.string.why_location, settingOp));
            pristupItem.setFatalni(true);
        }

    }

    private PristupItem addDefinition(PristupItem pristupItem) {
        if (pristupItem.neniPovolenPristup(activity)) {
            arPristupItem.add(pristupItem);
            arDlgTitle.add(pristupItem.getDlgTitle());
        } else {
            pristupItem.setPovoleno(true);
        }
        return pristupItem;
    }


    /**
     * Nejsou vydefinovaná povolení pro aplikaci
     * @return
     */
    public boolean chybiPovoleni() {
        // Marshmallow
        // M = 23
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return false;
        }
        for (PristupItem pristupItem : arPristupItem) {
            if (pristupItem.neniPovolenPristup(activity)) {
                return true;
            }
        }
        return false;
    }

    void execute(Class<?> cls) {
        this.cls = cls;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            SledovaniActivit.startActivity(activity, 1000, cls);
            return;
        }
        for (PristupItem pristupItem : arPristupItem) {
            if (pristupItem.neniPovolenPristup(activity)) {
                pristupItem.kontrola(activity);
                // Může se zavolat jen jednou - až po odpovědi lze volat další
                return;
            }
        }
    }


    /**
     * Voláno z hlavní aktivity
     *
     * @param requestCode uživatelsky definovaný kód
     * @param permissions požadavky
     */
    @Nullable
    PristupItem onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        PristupItem pristupItem = vratPristupItem(requestCode);
        if (pristupItem != null) {
            pristupItem.nastav(grantResults);
            // Nepovoleno a bez toho to nepůjde
            if (!pristupItem.getPovoleno() && pristupItem.getFatalni() && pristupItem.bylaVysvetlivka()) {
                return pristupItem;
            }
        }

        for (PristupItem pi : arPristupItem) {
            if (pi.vyrizeno || pi.prefPovoleno()) continue;
            pi.kontrola(activity);
            return pi;
        }
        if (cls != null) SledovaniActivit.startActivity(activity, 1000, cls);
        return null;
    }

    ArrayList<String> vratHlavicky(Activity activity) {
        if (arHlavicky.size() > 0) return arHlavicky;
        for (int i : arDlgTitle) {
            arHlavicky.add(activity.getString(i));
        }
        return arHlavicky;
    }

    int vratHlavicku(int position) {
        return position < arDlgTitle.size() ? arDlgTitle.get(position) : 0;
    }

    int vratVysvetleni(int position) {
        if (position >= arPristupItem.size()) return 0;
        PristupItem pristupItem = arPristupItem.get(position);
        return pristupItem.getExplanation();
    }

    @Nullable
    private PristupItem vratPristupItem(int requestCode) {
        for (PristupItem pristupItem : arPristupItem) {
            if (requestCode == pristupItem.getCallBack()) return pristupItem;
        }
        return null;
    }
}

class PristupItem {
    private String permission;
    public String pref;
    private int callBack;
    private int dlgTitle;
    private int dlgText;
    private int explanation;
    private SettingOp settingOp;
    boolean vyrizeno = false;
    private boolean bylaVysvetlivka = false;
    private boolean fatalni = false; // Bez toho nebude program fungovat
    private boolean povoleno=false;

    PristupItem(@NonNull final String permission, @NonNull String pref,
                final int callBack, int dlgTitle, int dlgText, int explanation, SettingOp settingOp) {
        this.permission = permission;
        this.pref = pref;
        this.callBack = callBack;
        this.dlgTitle = dlgTitle;
        this.dlgText = dlgText;
        this.explanation = explanation;
        this.settingOp = settingOp;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            settingOp.putBoolean(pref, true);
        }
    }

    int getDlgTitle() {
        return dlgTitle;
    }

    int getExplanation() {
        return explanation;
    }

    int getCallBack() {
        return callBack;
    }

    public boolean neniPovolenPristup(Activity activity) {
        return (ContextCompat.checkSelfPermission(activity,
                permission)
                != PackageManager.PERMISSION_GRANTED);
    }

    boolean kontrola(final Activity activity) {
        //if (settingOp.getBoolean(pref, false) || vyrizeno) return true;
        if (neniPovolenPristup(activity)) {

            // Vysvětlující dialog, který je poslední možností, jak přesvědčit
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    permission)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle(dlgTitle);
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setMessage(dlgText);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @TargetApi(Build.VERSION_CODES.M)
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        activity.requestPermissions(
                                new String[]
                                        {permission}
                                , callBack);
                    }
                });
                builder.show();
                bylaVysvetlivka = true;
                return false;
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{permission}, callBack);
                vyrizeno = true;
                return false;
            }
        }
        return true;
    }

    void nastav(int[] grantResults) {
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            povoleno = true;
            settingOp.putBoolean(pref, true);
            // permission was granted, yay! Do the
            // contacts-related task you need to do.

        } else {
            povoleno = false;
            settingOp.putBoolean(pref, false);
        }
        vyrizeno = true;
    }

    boolean prefPovoleno() {
        return settingOp.getBoolean(pref, false);
    }

    void setFatalni(boolean fatalni) {
        this.fatalni = fatalni;
    }

    boolean getPovoleno() {
        return povoleno;
    }

    boolean getFatalni() {
        return fatalni;
    }

    boolean bylaVysvetlivka() {
        return bylaVysvetlivka;
    }

    public String getPermission() {
        return permission;
    }

    public void setPovoleno(boolean povoleno) {
        this.povoleno = povoleno;
    }
}


