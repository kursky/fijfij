package cz.fijfij.hlavni;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.ArrayList;

import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.service.FijService;
import cz.fijfij.hlavni.sms.SmsItem;

/**
 * Created by jiri.kursky on 5.8.2016.
 * operace nad tabulkami
 */
public class BufferOp extends SQLiteOpenHelper {
    private Context context;
    private static final String DATABASE_NAME = "infoo.db";
    private static final int DATABASE_VERSION = 3;
    static final String TAG = FijService.class.getSimpleName();

    public SQLiteDatabase mDatabase = null;
    public static final boolean DB_WRITABLE = true;
    public static final boolean DB_READONLY = false;
    static private String sentAction = null;


    final static String TBL_LOG = "tbl_log";
    final static String TBL_PRATELE = "pratele";
    static final String TBL_OTAZKY = "otazky";
    private static final String TBL_OFFLINEBUFFER = "offlinebuffer";
    public static final String TBL_SOURADNICE = "souradnice"; // Načtené souřadnice přátel
    public static final String TBL_NAZVY_BODU = "nazvy_bodu";
    public static final String TBL_AKTIVITY_BODU = "aktivity_bodu"; // Na daném bodě proběhla změna uživatelem - reaguje na to služba

    private final static String COL_PRIMARY_ID = "id integer primary key";
    private final static String COL_LONGITUDE = def.PREF_LONGITUDE + " text";
    private final static String COL_LATITUDE = def.PREF_LATITUDE + " text";
    private final static String COL_PRITEL_ID = def.CL_PRITEL_ID + " integer";
    private final static String COL_BATTERY_PERCENT = def.PREF_BATTERY_PERCENT + " text";
    private final static String COL_VERSION = def.CL_VERSION + " text";
    private final static String COL_TYP_ZJISTENI_LOKACE = def.PREF_TYP_ZJISTENI_LOKACE + " integer default 0";
    private final static String COL_SMS_ID = def.CL_SMS_ID + " text";

    private final static String CT_SOURADNICE = "create table " + TBL_SOURADNICE
            + " (" + COL_PRIMARY_ID + ", " + COL_PRITEL_ID + ", cas text, " + COL_LONGITUDE + "," + COL_LATITUDE + ", cas_prepsani text, "+ COL_SMS_ID  +
            ", idZdroj integer, nezobrazovat integer," + COL_TYP_ZJISTENI_LOKACE + ")";

    private final static String CT_OTAZKY = "create table " + TBL_OTAZKY +
            " (" + COL_PRIMARY_ID + ", idDtb integer, pritelId integer, typ integer, otazka text, odpoved text," +
            "cas text, odpovedCas text, " +
            "idOdpoved integer default 0)";

    private final static String CT_NAZVY_BODU = "create table " + TBL_NAZVY_BODU +
            " (" + COL_PRIMARY_ID + ", " + COL_LONGITUDE + " , " + COL_LATITUDE + ", nazev text)";

    private final static String CT_AKTIVITY_BODU = "create table " + TBL_AKTIVITY_BODU +
            " (" + COL_PRIMARY_ID + ", " + def.PREF_USER_ID + " integer," + COL_LONGITUDE + ", " + COL_LATITUDE + ", "
            + def.PREF_SMS_POVOLENO + " int, " + def.PREF_ZMENA + " int)";

    private final static String CT_PRATELE = "create table " + TBL_PRATELE + " (" + def.PREF_USER_ID + " integer, " +
            def.PREF_NICK + " text, " + def.PREF_PHONE_NUMBER + " text, " + def.PREF_POUZE_SMS + "  integer, " +
            def.PREF_NO_RESTART_SERVICE + " integer, " + def.PREF_SMS_POVOLENO + " integer, " + def.PREF_JE_RIDICI + " integer," +
            def.PREF_POVOLENO_WIFI + " integer," + def.PREF_DATA_POVOLENA + " integer," + def.PREF_PIN + " integer," +
            def.PREF_SPOJENI + " text," + def.PREF_SPOJOVACI_KLIC + " text," +
            def.PREF_POSLEDNI_CAS + " text," + def.PREF_POSLEDNI_LATITUDE + " text," + def.PREF_POSLEDNI_LONGITUDE + " text," +
            COL_BATTERY_PERCENT + "," + COL_VERSION + ")";

    private final static String CT_OFFLINEBUFFER = "create table " + TBL_OFFLINEBUFFER + " (" + COL_PRIMARY_ID + ", action text)";
    private final static String CT_LOG = "create table " + TBL_LOG + "(" + COL_PRIMARY_ID + ", pritelId integer, typ text, tag text, zprava text)";

    private ArrayList<Tabulka> arTabulky = new ArrayList<>();


    public BufferOp(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        registrujTabulky();
        kontrolujTabulky();
    }

    public Context getContext() {
        return context;
    }

    private void registrujTabulky() {
        arTabulky.clear();
        arTabulky.add(new Tabulka(TBL_LOG, CT_LOG));
        arTabulky.add(new Tabulka(TBL_PRATELE, CT_PRATELE));
        arTabulky.add(new Tabulka(TBL_OFFLINEBUFFER, CT_OFFLINEBUFFER));
        arTabulky.add(new Tabulka(TBL_OTAZKY, CT_OTAZKY));
        arTabulky.add(new Tabulka(TBL_SOURADNICE, CT_SOURADNICE));
        arTabulky.add(new Tabulka(TBL_NAZVY_BODU, CT_NAZVY_BODU));
        arTabulky.add(new Tabulka(TBL_AKTIVITY_BODU, CT_AKTIVITY_BODU));
    }

    public boolean maData(String tabulka) {
        return numberOfRows(tabulka) > 0;
    }


    /**
     *
     * @param idZdroj
     * @param pritelId koho se týká
     * @return null při chybě, nejčastěji se jedná o změněnou tabulku (přidání/odebrání) sloupce
     */
    @Nullable
    private Cursor vratUdajDB(String idZdroj, String pritelId) {
        String sql = "select * from " + TBL_SOURADNICE + " where pritelId='" + pritelId + "' and " +
                "idZdroj=" + idZdroj;
        Cursor retVal = null;
        try {
            retVal = mDatabase.rawQuery(sql, null);
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        return retVal;
    }

    /**
     * Přidání souřadnic při načtení ze serveru, kontroluje sice tabulku
     * pomocí
     * @param b zdrojové bundle
     * @param prvni první záznam
     * @return vrací false při chybě
     */
    public boolean addCoordinates(Bundle b, Boolean prvni) {
        openDatabase(BufferOp.DB_WRITABLE);
        String idZdroj = b.getString("ID", "");
        if (idZdroj.equals("")) return true;
        String pritelId = b.getString("pritelId", "");
        Cursor res = vratUdajDB(idZdroj, pritelId);
        //
        if (res==null) {
            resetTabulky(TBL_SOURADNICE);
            res = vratUdajDB(idZdroj, pritelId);
            if (res==null) return false;
        }
        if (!res.moveToFirst()) {
            ContentValues values = new ContentValues();
            values.put(def.CL_ID_ZDROJ, idZdroj); // zdroj nesloučené tabulky
            values.put(def.CL_PRITEL_ID, pritelId);
            values.put(def.CL_CAS, b.getString(def.RET_CAS));
            values.put(def.CL_CAS_PREPSANI, b.getString(def.RET_CAS_PREPSANI));
            values.put(def.PREF_TYP_ZJISTENI_LOKACE, b.getString(def.RET_TYP_ZJISTENI_LOKACE));
            values.put(def.PREF_LONGITUDE, b.getString(def.RET_LONGITUDE));
            values.put(def.PREF_LATITUDE, b.getString(def.RET_LATITUDE));
            values.put(def.CL_NEZOBRAZOVAT, 0);
            try {
                mDatabase.insert(TBL_SOURADNICE, null, values);
            } catch (Exception e) {
                MyLog.e(TAG, e);
                return false;
            }
        } else if (prvni) {
            ContentValues values = new ContentValues();
            values.put(def.PREF_LONGITUDE, b.getString(def.RET_LONGITUDE));
            values.put(def.PREF_LATITUDE, b.getString(def.RET_LATITUDE));
            values.put(def.PREF_TYP_ZJISTENI_LOKACE, b.getString(def.RET_TYP_ZJISTENI_LOKACE));
            values.put(def.CL_CAS_PREPSANI, b.getString(def.RET_CAS_PREPSANI));
            try {
                mDatabase.update(TBL_SOURADNICE, values, "pritelId=" + pritelId + " and idZdroj=" + idZdroj, null);
            } catch (Exception e) {
                MyLog.e(TAG, e);
                return false;
            }
        }
        closeDatabase(res);
        return true;
    }


    public String getRecordsString() {
        kontrolujTabulku(TBL_LOG);
        openDatabase(DB_READONLY);
        Cursor res = mDatabase.rawQuery("select * from " + TBL_LOG, null);
        res.moveToFirst();
        String lastLog;
        if (res.isAfterLast()) return "";
        try {
            lastLog = res.getString(res.getColumnIndex("tag"));
            lastLog += " " + res.getString(res.getColumnIndex("zprava"));
        } catch (Exception e) {
            return "";
        }
        while (!res.isAfterLast()) {
            lastLog += "\n";
            lastLog = res.getString(res.getColumnIndex("tag"));
            lastLog += " " + res.getString(res.getColumnIndex("zprava"));
            res.moveToNext();
        }
        res.close();
        return lastLog;
    }

    public void dokonciPrikaz() {
        if (sentAction == null) return;
        openDatabase(DB_WRITABLE);
        mDatabase.execSQL("delete from " + TBL_OFFLINEBUFFER + " where action='" + sentAction + "'");
        sentAction = null;
    }

    public ArrayList<String> getAllActions() {
        ArrayList<String> array_list = new ArrayList<>();

        openDatabase(false);
        Cursor res = mDatabase.rawQuery("select distinct action from " + TBL_OFFLINEBUFFER, null);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            array_list.add(res.getString(res.getColumnIndex("action")));
            res.moveToNext();
        }
        res.close();
        return array_list;
    }

    public boolean openDatabase(boolean writable) {
        boolean retVal = true;
        try {
            if (writable) {
                if (mDatabase == null || !mDatabase.isOpen()) {
                    mDatabase = getWritableDatabase();
                }

                if (mDatabase.isReadOnly()) {
                    mDatabase.close();
                    mDatabase = getWritableDatabase();
                }
            } else {
                if (mDatabase == null || !mDatabase.isOpen()) {
                    mDatabase = getReadableDatabase();
                }

                if (!mDatabase.isReadOnly()) {
                    mDatabase.close();
                    mDatabase = getReadableDatabase();
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, e);
            retVal = false;
        }
        return retVal;
    }

    void kontrolujTabulku(String tabulka) {
        if (isTableExists(tabulka)) return;
        for (Tabulka t : arTabulky) {
            if (tabulka.equals(t.getNazev())) {
                openDatabase(DB_WRITABLE);
                mDatabase.execSQL(t.getDef());
                return;
            }
        }
    }

    private void kontrolujTabulky() {
        for (Tabulka t : arTabulky) {
            kontrolujTabulku(t.getNazev());
        }
    }

    void resetTabulky(String tabulka) {
        for (Tabulka t : arTabulky) {
            if (tabulka.equals(t.getNazev())) {
                openDatabase(DB_WRITABLE);
                mDatabase.execSQL("DROP TABLE IF EXISTS " + t.getNazev());
                try {
                    mDatabase.execSQL(t.getDef());
                } catch (Exception e) {
                    MyLog.e(TAG, e);
                }
                closeDatabase();
                return;
            }
        }
    }

    public void resetTabulek() {
        if (!openDatabase(DB_WRITABLE)) return;
        for (Tabulka t : arTabulky) {
            mDatabase.execSQL("DROP TABLE IF EXISTS " + t.getNazev());
            mDatabase.execSQL(t.getDef());
        }
    }


    private boolean isTableExists(String tableName) {
        openDatabase(DB_READONLY);
        Cursor cursor = mDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (Tabulka t : arTabulky) {
            db.execSQL(t.getDef());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            if (oldVersion<2) db.execSQL("ALTER TABLE " + TBL_SOURADNICE + " ADD COLUMN " + COL_TYP_ZJISTENI_LOKACE);
            db.execSQL("ALTER TABLE " + TBL_PRATELE+ " ADD COLUMN " + COL_BATTERY_PERCENT);
            db.execSQL("ALTER TABLE " + TBL_PRATELE+ " ADD COLUMN " + COL_VERSION);
        }
    }

    private int numberOfRows(String table) {
        openDatabase(DB_READONLY);
        return (int) DatabaseUtils.queryNumEntries(mDatabase, table);
    }

    private boolean alreadyInBuffer(String fullAction) {
        openDatabase(DB_READONLY);
        Cursor res = mDatabase.rawQuery("select * from " + TBL_OFFLINEBUFFER + " where action='" + fullAction + "'", null);
        if (res == null) return false;
        res.moveToFirst();
        if (res.isAfterLast()) return false;
        String s = res.getString(res.getColumnIndex("id"));
        if (s.equals("")) return false;
        int pocet = res.getCount();
        res.close();
        return (pocet >= 1);
    }

    /**
     * Přímé přidání do bufferu
     *
     * @param fullAction celý odesílaný příkaz
     */
    public boolean addToBuffer(String fullAction) {
        if (alreadyInBuffer(fullAction)) return false;
        openDatabase(DB_WRITABLE);
        ContentValues contentValues = new ContentValues();
        contentValues.put("action", fullAction);
        mDatabase.insert(TBL_OFFLINEBUFFER, null, contentValues);
        closeDatabase();
        return true;
    }

    private Long vratDtbOdpoved(String idRecord) {
        openDatabase(DB_READONLY);
        Cursor res = mDatabase.rawQuery("select * from " + TBL_OTAZKY + " where idDtb='" + idRecord + "'", null);
        if (res == null) return 0L;
        res.moveToFirst();
        if (res.isAfterLast()) return 0L;
        Long retVal = res.getLong(res.getColumnIndex("id"));
        closeDatabase(res);
        return retVal;
    }

    public void zapisOdpovedNaOtazku(Bundle result) {
        String id = result.getString("MSG_ID");
        String odpoved = result.getString("RESPONSE");
        if (id == null || id.equals("")) return;
        String odpovedCas = result.getString("RESPONSED_TIME");
        ContentValues values = new ContentValues();
        values.put("odpoved", odpoved);
        values.put("odpovedCas", odpovedCas);
        openDatabase(DB_WRITABLE);
        mDatabase.update(TBL_OTAZKY, values, "ID=" + id, null);
        closeDatabase();
    }

    void vymazNetUdaj(String idZdroj, String pritelId) {
        openDatabase(DB_WRITABLE);

        ContentValues values = new ContentValues();
        values.put("nezobrazovat", 1);
        try {
            mDatabase.update(TBL_SOURADNICE, values, "pritelId=" + pritelId + " and idZdroj=" + idZdroj, null);
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        closeDatabase();
    }

    void vymazatSouradnice(String pritelId) {
        openDatabase(DB_WRITABLE);
        try {
            mDatabase.execSQL("delete from " + TBL_SOURADNICE + " where pritelId='" + pritelId + "'");
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        closeDatabase();
    }

    private class Tabulka {
        private final String nazev;
        private final String def;

        Tabulka(String nazev, String def) {
            this.def = def;
            this.nazev = nazev;
        }

        public String getNazev() {
            return nazev;
        }

        public String getDef() {
            return def;
        }
    }


    //// Operace s SMS

    /**
     * Zapíše odeslanou SMS spolu s identifikátorem tvořeným časem jde o žádost souřadnic
     *
     * @param osoba
     * @param smsID
     */
    public void zapisSMSOdeslano(Osoba osoba, String smsID) {
        openDatabase(DB_WRITABLE);
        String pritelId = osoba.getUserId();
        Cursor res = mDatabase.rawQuery("select * from " + TBL_SOURADNICE + " where pritelId='" + pritelId +
                "' and smsID='" + smsID + "' order by id desc", null);
        if (res.moveToFirst()) {
            res.close();
            return;
        }
        ContentValues value = new ContentValues();
        value.put("pritelId", pritelId);
        value.put("cas", def.todaySQL());
        value.put("smsID", smsID);
        try {
            mDatabase.insert(TBL_SOURADNICE, null, value);
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        closeDatabase(res);
    }


    public void zapisOdeslanaSMS(SmsItem smsItem) {
        openDatabase(DB_WRITABLE);
        String pritelId = smsItem.getUserId();
        String smsID = smsItem.getSmsID();
        Cursor res;
        try {
            res = mDatabase.rawQuery("select * from " + TBL_SOURADNICE + " where pritelId='" + pritelId +
                    "' and smsID='" + smsID + "' order by id desc", null);
            if (res == null) return;
        } catch (Exception e) {
            MyLog.e(TAG, e);
            return;
        }
        if (res.moveToFirst()) {
            String casPrepsani = res.getString(res.getColumnIndex("cas_prepsani"));
            if (TextUtils.isEmpty(casPrepsani)) {
                ContentValues value = new ContentValues();
                value.put("cas_prepsani", def.todaySQL());
                try {
                    mDatabase.update(TBL_SOURADNICE, value, "pritelId=" + pritelId + " and smsID='" + smsID + "'", null);
                } catch (Exception e) {
                    MyLog.e(TAG, e);
                }
            }
        } else {
            ContentValues value = new ContentValues();
            value.put("pritelId", pritelId);
            value.put("cas", def.todaySQL());
            value.put("smsID", smsID);
            value.put("cas_prepsani", def.todaySQL());
            try {
                mDatabase.insert(TBL_SOURADNICE, null, value);
            } catch (Exception e) {
                MyLog.e(TAG, e);
            }
        }
        closeDatabase(res);
    }

    public void closeDatabase(@Nullable Cursor res) {
        if (mDatabase == null) return;
        if (res != null) res.close();
        mDatabase.close();
        mDatabase = null;
    }

    public void closeDatabase() {
        closeDatabase(null);
    }
}