/*
 * Copyright (c) 2017 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.widget.Toast;

import org.json.JSONObject;

import cz.fijfij.R;
import cz.fijfij.hlavni.service.FijService;
import cz.fijfij.hlavni.zapouzdreno.SmsControl;

/**
 * Created by jiri.kursky on 28.01.2017
 * Služba zajišťující příjem SMS podřízeného telefonu
 */

public class LoginService extends Service {
    public static final String TAG = "LoginTag";
    private SettingOp settingOp;
    private Context context;
    public final Messenger mMessenger = new Messenger(new LoginService.IncomingHandler());

    public class IncomingHandler extends Handler { // Handler of incoming messages from clients.

        @Override
        public void handleMessage(Message msg) {

        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        def.factory(this);
        settingOp = new SettingOp(this);
        showNotification();
    }

    private void showNotification() {
        Context context=this.getApplicationContext();
        Intent notificationIntent = new Intent(context, def.getMainActivity(context));
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.setAction("android.intent.action.MAIN");
        notificationIntent.addCategory(TAG);

        final PendingIntent pi = PendingIntent
                .getActivity(this, 0, notificationIntent, 0); // didn't work: PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentTitle(getString(R.string.app_name));
        builder.setContentText(getString(R.string.sluzba_podnazev_login));
        builder.setSmallIcon(R.mipmap.ic_launcher_service);
        builder.setOngoing(true);
        builder.setContentIntent(pi);
        builder.setAutoCancel(false);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(FijService.LOGIN_NOTIFY_ID, builder.build());
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        IntentFilter smsFilter;
        smsFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        smsFilter.setPriority(1000);
        SMSReceive smsReceive = new SMSReceive(this);
        registerReceiver(smsReceive, smsFilter);
        Handler startHandler = new Handler();
        context=this;
        startHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                zjistiPredregistraci();
            }
        }, 1000);
        return Service.START_STICKY;
    }

    private void zjistiPredregistraci() {
        String action = def.EX_ACTION + def.CMD_PREDREGISTRACE;
        action+=def.EX_ANDROID_ID+def.android_id;
        AsyncService asService = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
                Bundle bundle = def.transferJSON(result);
                String userId = bundle.getString(def.RET_USER_ID, "");
                if (!TextUtils.isEmpty(userId)) {
                    smsOdeslana();
                    Intent intentCekani = new Intent(context, ActivityCekaniSpojeni.class);
                    intentCekani.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intentCekani);
                }
            }
        }, this, def.CMD_PREDREGISTRACE, action);
        asService.execute();
    }

    void smsOdeslana() {
        Handler startHandler = new Handler();
        startHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                checkServer();
            }
        }, 10000);
    }

    // Zkoumá, jestli již nedošlo k registraci
    private void checkServer() {
        String action = settingOp.getActionString(def.CMD_UDAJE_USER);
        final Context context = this;
        AsyncService asService = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
                Bundle bundle = def.transferJSON(result);
                String userId = bundle.getString(def.RET_USER_ID, "");
                Handler startHandler = new Handler();
                if (TextUtils.isEmpty(userId)) {
                    startHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            checkServer();
                        }
                    }, 5000);
                } else {
                    Intent intent = new Intent(context, Splash.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    startHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            stopSelf();
                        }
                    }, 500);
                }
            }
        }, this, def.CMD_UDAJE_USER, action);
        asService.execute();
    }

    class SMSReceive extends BroadcastReceiver {
        SmsControl smsControl;
        LoginService loginService;

        public SMSReceive(LoginService loginService) {
            super();
            smsControl = new SmsControl(loginService);
            this.loginService = loginService;
        }

        @Override
        public void onReceive(final Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            smsControl.pridejRodice(bundle, new AsyncService.TaskListener() {
                @Override
                public void onFinished(String nameAction, JSONObject result) {
                    // Odeslána SMS rodičovi a teď se bude ve smyčce čekat, dojde-li k registraci
                    loginService.smsOdeslana();
                    Toast.makeText(getBaseContext(), R.string.byla_odeslana_odpoved, Toast.LENGTH_LONG).show();
                    Intent intentCekani = new Intent(context, ActivityCekaniSpojeni.class);
                    intentCekani.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentCekani);

                }
            });
        }
    }
}