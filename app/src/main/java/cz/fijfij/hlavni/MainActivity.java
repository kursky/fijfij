package cz.fijfij.hlavni;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import cz.fijfij.R;
import cz.fijfij.hlavni.fragments.PrehledOsobFragment;

public class MainActivity extends FragmentActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Platí pouze pro malé, ale nechal jsem to tu
        if (findViewById(R.id.fragment_container) != null) {
            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            PrehledOsobFragment prehledOsob = new PrehledOsobFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            prehledOsob.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, prehledOsob).commit();
        }

    }

    @Override
    public void onBackPressed() {
        def.otazkaUkoncitProgram(this);
    }
}
