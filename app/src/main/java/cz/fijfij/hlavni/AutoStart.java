/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import cz.fijfij.hlavni.service.FijService;

/**
 * Created by jiri.kursky on 15.11.2016
 * Spouští se při startu - služba
 */

public class AutoStart extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
        }
        SettingOp settingOp = new SettingOp(context);
        if (settingOp.getBoolean(def.PREF_PRVNI_SPUSTENI, true) || settingOp.getBoolean(def.PREF_REGISTRACE_ZRUSENA, true)) return;
        if (!settingOp.getBoolean(def.PREF_NO_RESTART_SERVICE, false)) {
            Intent service = new Intent(context.getApplicationContext(), FijService.class);
            service.addCategory(FijService.TAG);
            context.startService(service);
        }
    }

}
