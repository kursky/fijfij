package cz.fijfij.hlavni;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;

import cz.fijfij.hlavni.service.FijService;

/**
 * Created by jiri.kursky on 09.02.2017
 */

public class MyPreferenceFragment extends PreferenceFragment {
    protected ServiceOp serviceOp = null;

    @Override
    public void onResume() {
        super.onResume();
        if (serviceOp == null) serviceOp = new ServiceOp(getActivity(), null);
        serviceOp.doBindService();
    }

    @Override
    public void onPause() {
        if (serviceOp != null) {
            serviceOp.doUnbindService();
            serviceOp = null;
        }
        super.onPause();
    }

    protected void sendMessage(int msg, @Nullable Bundle bundle) {
        if (serviceOp==null) return;
        if (bundle==null) {
            serviceOp.sendMessageToService(msg);
        }
        else {
            serviceOp.sendMessageToService(msg, bundle);
        }

    }


    protected void sendZmenaParametru(Bundle bundle) {
        sendMessage(FijService.MSG_ZMENA_PARAMETRU, bundle);
    }

}
