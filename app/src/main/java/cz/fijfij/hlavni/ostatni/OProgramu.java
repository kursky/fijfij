/*
 * Copyright (c) 2017 Jiri Kursky
 */

package cz.fijfij.hlavni.ostatni;

import android.os.Bundle;
import android.widget.TextView;

import cz.fijfij.R;
import cz.fijfij.hlavni.BaseActivity;
import cz.fijfij.hlavni.SettingOp;
import cz.fijfij.hlavni.def;

/**
 * Created by jiri.kursky on 05.01.2017
 * O programu
 */

public class OProgramu extends BaseActivity {

    public OProgramu() {
        super(R.layout.o_programu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SettingOp settingOp = new SettingOp(this);
        def.nastavText(this, R.id.txtVerze, R.string.version_name, def.getVersionName(this));
        def.nastavText(this, R.id.txtId, R.string.your_id, def.userId());
        def.nastavText(this, R.id.txtNick, R.string.your_nick, settingOp.getString(def.PREF_NICK));
        def.nastavText(this, R.id.txtAndroidId, R.string.android_id, def.android_id);
        def.nastavText(this, R.id.txtNasMail, R.string.our_email, R.string.email_app);
        TextView txt = (TextView) findViewById(R.id.txtPoznamka);

        if (def.emulator) {
            if (def.hideTextView(txt, false)) {
                txt.setText(R.string.rezim_emulator);
            }
        } else {
            def.hideTextView(txt, true);
        }
    }
}
