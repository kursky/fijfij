package cz.fijfij.hlavni;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ListView;

import cz.fijfij.R;

/**
 * Created by jiri.kursky on 01.03.2017
 * Druhý splash který pokládá otázky týkající se pro přístup ke službám 6.0
 */

public class Otazky extends Activity {
    private ProverPristupy proverPristupy;
    private View viewOtazky;
    private View viewFatalni;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SettingOp settingOp = new SettingOp(this);
        proverPristupy = new ProverPristupy(this, settingOp);
        setContentView(R.layout.activity_otazky);
        View viewNormalniSplash = findViewById(R.id.ll_normalniSplash);
        viewOtazky = findViewById(R.id.ll_otazky);
        viewFatalni = findViewById(R.id.ll_fatalni);
        viewFatalni.setVisibility(View.GONE);
        def.nastavText(this, R.id.txtVerze, R.string.version_name, def.getVersionName(this));
        if (proverPristupy.chybiPovoleni()) {
            viewNormalniSplash.setVisibility(View.GONE);
            viewOtazky.setVisibility(View.VISIBLE);
        } else {
            viewNormalniSplash.setVisibility(View.VISIBLE);
            viewOtazky.setVisibility(View.GONE);
        }
        AdapterOtazky adapterOtazky = new AdapterOtazky(this, proverPristupy);
        ListView listView = (ListView) findViewById(R.id.listParametru);
        listView.setAdapter(adapterOtazky);
        execProverPristupy();
    }

    private void execProverPristupy() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                proverPristupy.execute(Splash.class);
            }
        }, 1000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        PristupItem pristupItem = proverPristupy.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (pristupItem == null) return;

        // Fatální záležitost - bez toho to prostě nepůjde
        if (!pristupItem.getPovoleno() && pristupItem.getFatalni() && pristupItem.bylaVysvetlivka()) {
            viewOtazky.setVisibility(View.GONE);
            viewFatalni.setVisibility(View.VISIBLE);
        }
    }

    public void znovuPristup(View view) {
        SledovaniActivit.startActivity(this, 1000, Otazky.class);
    }

    public void konec(View view) {
        def.konec();
    }
}
