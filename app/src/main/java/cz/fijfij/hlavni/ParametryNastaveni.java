/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cz.fijfij.R;
import cz.fijfij.hlavni.nastaveni.ParametrItem;

/**
 * Created by jiri.kursky on 31.12.2016.
 * Základ pro nastavení parametru
 */

public class ParametryNastaveni extends BaseActivity {
    protected EditText dlgEditText;
    protected Osoba osoba;
    protected PrateleOp prateleOp;
    protected BufferOp bufferOp;
    private int typNastaveni;
    protected AlertDialog dialog;

    public ParametryNastaveni(int contentView, int typNastaveni) {
        super(contentView, true);
        this.typNastaveni = typNastaveni;
    }

    public void nastavHodnotuDialogem(ParametrItem parametrItem, String hodnota) {
        String klic = parametrItem.getKlic();
        if (klic.equals(def.PREF_PHONE_NUMBER)) {
            vyberTelCisla();
            return;
        }
        dlgEditText = parametrItem.setDlgEditText(this, hodnota);
        dialog = ParametrOp.getAlertDialog(this, parametrItem);
        Button theButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        if (klic.equals(def.PREF_NICK)) {
            theButton.setOnClickListener(new NickListener(dialog));
            return;
        }

        if (klic.equals(def.PREF_PHONE_NUMBER)) {
            theButton.setOnClickListener(new PhoneListener(dialog));
            return;
        }

        if (klic.equals(def.PREF_PIN)) {
            theButton.setOnClickListener(new PINListener(dialog));
            return;
        }
        defineNastaveni(klic, theButton);
    }

    protected void defineNastaveni(String klic, Button theButton) {

    }


    void nastavenPin() {
    }

    void vyberTelCisla() {
    }

    void novyNick(String nick) {
        osoba.setNick(nick);
    }

    class NickListener implements View.OnClickListener {
        private final Dialog dialog;

        NickListener(Dialog dialog) {
            this.dialog = dialog;
        }

        @Override
        public void onClick(View v) {
            String mValue = dlgEditText.getText().toString();
            if (TextUtils.isEmpty(mValue)) {
                Toast.makeText(getBaseContext(), R.string.hodnota_musi_byt_zadana, Toast.LENGTH_SHORT).show();
                return;
            }
            if (prateleOp.vratPriteleNick(mValue) != null) {
                Toast.makeText(getBaseContext(), R.string.nick_existuje, Toast.LENGTH_SHORT).show();
                return;
            }
            novyNick(mValue);
            dialog.dismiss();
        }
    }

    class PhoneListener implements View.OnClickListener {
        private final Dialog dialog;

        PhoneListener(Dialog dialog) {
            this.dialog = dialog;
        }

        @Override
        public void onClick(View v) {
            String mValue = dlgEditText.getText().toString();
            if (TextUtils.isEmpty(mValue)) {
                Toast.makeText(getBaseContext(), R.string.hodnota_musi_byt_zadana, Toast.LENGTH_SHORT).show();
                return;
            }
            if (prateleOp.vratPriteleDlePhoneNumber(mValue) != null) {
                Toast.makeText(getBaseContext(), R.string.telefon_existuje, Toast.LENGTH_SHORT).show();
                return;
            }
            osoba.setPhoneNumber(mValue);
            dialog.dismiss();
        }
    }

    class PINListener implements View.OnClickListener {
        private final Dialog dialog;

        PINListener(Dialog dialog) {
            this.dialog = dialog;
        }

        @Override
        public void onClick(View v) {
            String mValue = dlgEditText.getText().toString();
            if (TextUtils.isEmpty(mValue)) {
                Toast.makeText(getBaseContext(), R.string.hodnota_musi_byt_zadana, Toast.LENGTH_SHORT).show();
                return;
            }
            osoba.setPIN(mValue);
            dialog.dismiss();
            nastavenPin();
        }
    }

    protected String vratHodnotuString(String klic) {
        return osoba != null ? osoba.getKeyValue(klic) : "";
    }

    /**
     * Kliknutí na item vse nad tim obsluhuje dialog, pokud se jedna o string
     */
    protected AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view,
                                int position, long id) {
            final ParametrItem parametrItem = def.parametrOp.getParametrItem(position, typNastaveni);
            int typ = parametrItem.getTyp();
            if (typ != ParametrItem.PAR_STRING) return;
            nastavHodnotuDialogem(parametrItem, vratHodnotuString(parametrItem.getKlic()));
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bufferOp = new BufferOp(this);
        onCreateNextStep();
    }

    protected void onCreateNextStep() {
        prateleOp = new PrateleOp(bufferOp);
        prateleOp.nactiOffline(); // zde je otazka, jestli to neudělat on-line
    }
}