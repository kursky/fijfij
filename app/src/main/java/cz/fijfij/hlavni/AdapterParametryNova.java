/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import cz.fijfij.R;
import cz.fijfij.hlavni.nastaveni.ParametrItem;

/**
 * Created by jiri.kursky on 03.12.2016
 * adapter pro nastaveni parametru AdapterParametryNova
 */

public class AdapterParametryNova extends ArrayAdapter<String> {
    private Activity activity;
    private final ListView listView;
    private Osoba osoba;

    AdapterParametryNova(Activity activity, Osoba osoba, ListView listView) {
        super(activity, R.layout.list_parametry, def.parametrOp.getAllKeys(ParametrOp.PARS_CLEN_VIDITELNE));
        this.activity = activity;
        this.osoba = osoba;
        this.listView = listView;
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_parametry, null, true);
        if (osoba == null) return rowView;
        TextView txtNazevParametru = (TextView) rowView.findViewById(R.id.txtNazevParametru);
        TextView txtSubParametr = (TextView) rowView.findViewById(R.id.txtSubNazevParametru);
        Switch prepinac = (Switch) rowView.findViewById(R.id.prepinac);
        ParametrItem parametrItem = def.parametrOp.getParametrItem(position, ParametrOp.PARS_CLEN_VIDITELNE);
        txtNazevParametru.setText(parametrItem.getNazev());

        String klic = parametrItem.getKlic();
        switch (parametrItem.getTyp()) {
            case ParametrItem.PAR_STRING:
                txtSubParametr.setText(osoba.getKeyValue(klic));
                prepinac.setVisibility(View.INVISIBLE);
                break;
            case ParametrItem.PAR_BOOLEAN:
                txtSubParametr.setVisibility(View.GONE); // není potřeba definovat text u přepínače, ten se definuje vlevo, vypadá to blbě s ním
                prepinac.setChecked(osoba.getKeyValueBool(klic));
                prepinac.setVisibility(View.VISIBLE);

                prepinac.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        int position = listView.getPositionForView(buttonView);
                        ParametrItem parametrItem = def.parametrOp.getParametrItem(position, ParametrOp.PARS_CLEN);
                        String klic = parametrItem.getKlic();
                        switch (parametrItem.getTyp()) {
                            case ParametrItem.PAR_BOOLEAN:
                                osoba.setKeyValue(klic, isChecked);
                                break;
                        }
                    }
                });
                break;
        }
        return rowView;
    }
}
