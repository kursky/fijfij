/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;

import cz.fijfij.R;
import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.fragments.PrehledOsobFragment;
import cz.fijfij.hlavni.service.FijService;
import cz.fijfij.hlavni.zapouzdreno.SmsControl;

/**
 * Created by jiri.kursky on 10.12.2016.
 * Zde se zadefinuje, jake ma mít parametry nový přítel a vytvoří se konfigurační sms nebo kód, který se přenáší do druhého zařízení
 *
 * V tuto chvíli sms zakázáno, byly chyby při vlastním párování
 */

public class ActivityParametryNoveho extends ParametryNastaveni {
    private static final int ZISKANI_KODU = 10;
    private static final int ZRUS_KOD = 20;
    private static final int HLAVNI_AKTIVITA = 30;
    private static final int OTEVRI_SEZNAM = 40;
    private TextView txtKodNapoveda;
    private TextView txtVlastniKod;
    private Button btnVygenerujKod;
    private Button btnZrusKod;
    private SettingOp settingOp;
    private String spojovaciKlic;
    private RelativeLayout rlSeznamParametru;
    private boolean kodZrusen = false;
    private ServiceOp serviceOp;
    private String novyPritelId;
    private boolean isRunning;
    private ProgressBar waitSpinnerCode;
    private TextView txtSpinner;
    private Button btnOdesliSMS;


    public ActivityParametryNoveho() {
        super(R.layout.activity_nova_osoba, ParametrOp.PARS_CLEN_VIDITELNE);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isRunning = true;

        serviceOp = new ServiceOp(this, new ServiceOp.ServiceInt() {
            @Override
            public void onServiceConnected() {
                serviceOp.registerClient();
            }

            @Override
            public void onServiceDisconnected() {
                serviceOp.unRegisterClient();
            }

            @Override
            public boolean receivedMessage(Message msg) {
                return false;
            }
        });
        txtKodNapoveda = (TextView) findViewById(R.id.txtKodNapoveda);
        txtVlastniKod = (TextView) findViewById(R.id.txtVlastniKod);
        btnVygenerujKod = (Button) findViewById(R.id.btnVygenerujKod);
        btnZrusKod = (Button) findViewById(R.id.btnZrusKod);
        btnOdesliSMS = (Button) findViewById(R.id.btnPotvrzeniKodu);
        btnOdesliSMS.setVisibility(View.GONE); // @TODO zde pak odstranit
        rlSeznamParametru = (RelativeLayout) findViewById(R.id.rl_seznamParametru);
        rlSeznamParametru.setVisibility(View.VISIBLE);
        txtSpinner = (TextView) findViewById(R.id.txtSpinnner);
        waitSpinnerCode = (ProgressBar) findViewById(R.id.waitSpinnerCode);

        obrazovka();

        settingOp = new SettingOp(this);
        osoba = new Osoba();
        osoba.setDefaultPritel();
    }

    @Override
    public void onResume() {
        super.onResume();
        ListView listView = (ListView) findViewById(R.id.listParametruNova);
        AdapterParametryNova adapterParametryNova = new AdapterParametryNova(this, osoba, listView);
        adapterParametryNova.notifyDataSetChanged();
        listView.setAdapter(adapterParametryNova);
        listView.setOnItemClickListener(onItemClickListener);
        zrusCekej();
        serviceOp.doBindService();
        isRunning = true;
    }

    /**
     * Odešle žádost na druhy telefon
     */
    public void odesliSMSKonfiguraci(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);

        builder.setTitle(R.string.info);
        builder.setMessage(R.string.chyba_verze);
        builder.setNegativeButton(R.string.dlgOK, null);
        builder.show();
    }

    /**
     * @TODO nefunguje
     * @param view
     */
    public void _odesliSMSKonfiguraci(View view) {
        String nick = osoba.getNick();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        if (TextUtils.isEmpty(nick)) {
            builder.setTitle(R.string.info);
            builder.setMessage(R.string.chybi_nick);
            builder.setNegativeButton(R.string.dlgOK, null);
            builder.show();
            return;
        }

        String phoneNumber = osoba.getPhoneNumber();
        if (TextUtils.isEmpty(phoneNumber)) {
            builder.setTitle(R.string.info);
            builder.setMessage(R.string.neni_nastaveno_cislo);
            builder.setNegativeButton(R.string.dlgOK, null);
            builder.show();
            return;
        }

        final Context ctx = this;

        String action = vratParametryNastaveni(def.CMD_SMS_PREDNASTAV);
        final AsyncService asZiskani = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
                if (result == null) {
                    return;
                }
                try {
                    SmsControl smsControl = new SmsControl(ctx);
                    smsControl.odesliZadostRodice(osoba.getPhoneNumber());
                } catch (Exception e) {
                    MyLog.e("UL", e);
                }
                def.hlavniStranka(ctx);
            }
        }, this, "spojeni", action);
        builder.setTitle(R.string.odeslani_sms_konfigurace);
        builder.setMessage(R.string.nezapomente_spustit_aplikaci);

        builder.setNegativeButton(R.string.zpet, null);
        builder.setPositiveButton(R.string.dlgOK, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                txtKodNapoveda.setVisibility(View.GONE);
                txtVlastniKod.setVisibility(View.GONE);
                btnOdesliSMS.setVisibility(View.GONE);
                btnVygenerujKod.setVisibility(View.GONE);
                btnZrusKod.setVisibility(View.GONE);
                txtSpinner.setVisibility(View.VISIBLE);
                txtSpinner.setText(R.string.cekam_na_odpoved);
                waitSpinnerCode.setVisibility(View.VISIBLE);
                rlSeznamParametru.setVisibility(View.GONE);
                asZiskani.execute();
            }
        });
        builder.show();

    }


    /**
     * Stisk tlačítka pro vygenerování kódu
     */
    public void vygenerujKod(View view) {
        if (chybiItem()) return;
        nastavCekej(ZISKANI_KODU);
        isRunning = true;
        cekejSmyckouNaPotvrzeni();
    }


    /**
     * Kontroluje zadefinované udaje
     *
     * @return true pokud něco chybí
     */
    private boolean chybiItem() {
        boolean retVal = false;
        String nick = osoba.getNick();
        if (TextUtils.isEmpty(nick)) {
            retVal = true;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.vygeneruj_kod);
            builder.setCancelable(true);
            builder.setMessage(R.string.chybi_nick);
            builder.setPositiveButton(R.string.dlgOK, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    return;
                }
            });
            builder.show();
        }
        String phoneNumber = osoba.getPhoneNumber();
        if (!retVal) {
            if (TextUtils.isEmpty(phoneNumber) && osoba.getKeyValueBool(def.PREF_SMS_POVOLENO)) {
                retVal = true;
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.info);
                builder.setCancelable(true);
                builder.setMessage(R.string.nevyplneno_telefonni_cislo);
                builder.setPositiveButton(R.string.dlgAno, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        nastavCekej(ZISKANI_KODU);
                        isRunning = true;
                        cekejSmyckouNaPotvrzeni();
                    }
                });
                builder.setNegativeButton(R.string.dlgNe, null);
                builder.show();
            }
        }
        return retVal;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String phoneNumber = data.getStringExtra(def.PREF_PHONE_NUMBER);
                osoba.setPhoneNumber(phoneNumber);
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    private String vratParametryNastaveni(String akce) {
        String action = def.EX_ACTION + akce;
        action += def.EX_ANDROID_ID + def.android_id;
        action += def.EX_USERID + def.userId();
        action += def.EX_PHONE_NUMBER + osoba.getPhoneNumber();
        action += def.EX_NICK + osoba.getNickUri();
        action += def.EX_JE_RIDICI + "0";
        action += def.EX_POVOLENO_WIFI + osoba.getKeyValueInt(def.PREF_POVOLENO_WIFI);
        action += def.EX_DATA_POVOLENA + osoba.getKeyValueInt(def.PREF_DATA_POVOLENA);
        action += def.EX_SMS_POVOLENO + osoba.getKeyValueInt(def.PREF_SMS_POVOLENO);
        action += def.EX_POUZE_SMS + osoba.getKeyValueInt(def.PREF_POUZE_SMS);
        action += def.EX_OSTATNI_NASTAVENI + osoba.getKeyValue(def.PREF_OSTATNI_NASTAVENI);
        action += def.EX_PIN + osoba.getPIN();
        action += def.exTimeStamp();
        return action;
    }

    /**
     * Zde jsou základní aktivity - například getContacts
     * @param zdrojVolani identifikátor, který se předal při nastavCekej
     */
    @Override
    public void cekaniNastaveno(int zdrojVolani) {
        Intent intent;
        switch (zdrojVolani) {
            case OTEVRI_SEZNAM:
                otevriTelefonniSeznam();
                break;
            case ZISKANI_KODU:
                // Pri ziskani kodu se zakládá nový přítel
                // a čeká se na potvrzení
                String action = vratParametryNastaveni(def.CMD_SPOJOVACI_KLIC);
                AsyncService asZiskani = new AsyncService(new AsyncService.TaskListener() {
                    @Override
                    public void onFinished(String nameAction, JSONObject result) {
                        if (result == null) {
                            nastavCekej(HLAVNI_AKTIVITA);
                            return;
                        }
                        try {
                            spojovaciKlic = result.getString(def.RET_SPOJOVACI_KLIC);
                            novyPritelId = result.getString(def.RET_PRITELID);
                            if (!TextUtils.isEmpty(spojovaciKlic)) {
                                txtKodNapoveda.setVisibility(View.VISIBLE);
                                txtKodNapoveda.setText(R.string.vygenerovan_kod);
                                txtVlastniKod.setVisibility(View.VISIBLE);
                                txtVlastniKod.setText(spojovaciKlic);
                                rlSeznamParametru.setVisibility(View.GONE);
                                btnVygenerujKod.setVisibility(View.GONE);
                                btnZrusKod.setVisibility(View.VISIBLE);
                                btnOdesliSMS.setVisibility(View.GONE);
                                kodZrusen = false;
                                waitSpinnerCode.setVisibility(View.VISIBLE);
                                txtSpinner.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            MyLog.e("UL", e);
                        }
                        zrusCekej();
                        cekejSmyckouNaPotvrzeni(); // Cekani na druhy telefon
                    }
                }, this, "spojeni", action);
                asZiskani.execute();
                break;
            case HLAVNI_AKTIVITA:
                def.hlavniStranka(this);
                break;
            case ZRUS_KOD:
                String actionZruseni = def.EX_ACTION + def.CMD_SPOJOVACI_KLIC_ZRUSENI;
                actionZruseni += def.EX_SPOJOVACI_KLIC + spojovaciKlic;
                actionZruseni += def.EX_PRITELID + novyPritelId;
                if (TextUtils.isEmpty(novyPritelId) || TextUtils.isEmpty(spojovaciKlic)) {
                    obrazovka();
                    zrusCekej();
                    return;
                }
                AsyncService asZruseni = new AsyncService(new AsyncService.TaskListener() {
                    @Override
                    public void onFinished(String nameAction, JSONObject result) {
                        obrazovka();
                        zrusCekej();
                    }
                }, this, "zruseni", actionZruseni);
                asZruseni.execute();
                break;
        }

    }

    private final Handler startHandler = new Handler();


    private void cekejSmyckouNaPotvrzeni() {
        Runnable runCheckServer = new Runnable() {
            @Override
            public void run() {
                _cekejSmyckouNaPotvrzeni();
            }
        };
        if (isRunning) startHandler.postDelayed(runCheckServer, 1000 * 15);
    }

    @Override
    public void onPause() {
        serviceOp.doUnbindService();
        isRunning = false;
        super.onPause();
    }


    /**
     * Důležitá procedura, která čeká na návrat přátel
     * Ceka se na potvrzeni - vrácení AID pritele, pokud se zadaří, načtou se nove pratele
     */
    boolean asExecute = true; // zabraňuje opětovnému volání v průběhu smyčky

    private void _cekejSmyckouNaPotvrzeni() {
        String action = settingOp.getActionString(def.CMD_PRITEL_ZAPSAN) + def.EX_SPOJOVACI_KLIC + spojovaciKlic + def.EX_PRITELID + novyPritelId;
        AsyncService asZiskani = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
                String pritelAid;
                if (result == null) {
                    zrusCekej();
                    return;
                }
                try {
                    try {
                        pritelAid = result.getString(def.RET_ANDROID_ID);
                    } catch (Exception e) {
                        asExecute = true;
                        cekejSmyckouNaPotvrzeni();
                        return;
                    }
                    if (TextUtils.isEmpty(pritelAid)) {
                        asExecute = true;
                        cekejSmyckouNaPotvrzeni();
                        return;
                    }

                    // Zadefinované přátelství
                    osoba.setUserId(novyPritelId);

                    // Toto se vola az po settingu
                    final PrateleOp prateleOp = new PrateleOp(bufferOp, new PrateleOp.CallBackListener() {
                        @Override
                        public void onFinished() {
                            txtKodNapoveda.setVisibility(View.VISIBLE);
                            txtVlastniKod.setVisibility(View.GONE);
                            txtVlastniKod.setText("");
                            rlSeznamParametru.setVisibility(View.VISIBLE);
                            txtKodNapoveda.setText(R.string.kod_uspesne_prijat);
                            serviceOp.sendMessageToService(FijService.MSG_ZMENA_PARAMETRU_PRITEL);
                            PrehledOsobFragment.prateleNacteni = false;
                            nastavCekej(HLAVNI_AKTIVITA);
                        }
                    });

                    settingOp.sendUserParametres(osoba, true, new SettingOp.OdeslanyZmeny() {
                        @Override
                        public void onStart() {
                        }

                        @Override
                        public void onFinished() {
                            prateleOp.execute();
                        }
                    });
                    asExecute = false;
                } catch (Exception e) {
                    MyLog.e("UL", e);
                }
            }
        }, this, "pritel", action);
        if (asExecute) {
            asExecute = false;
            asZiskani.execute();
        } else if (kodZrusen) {
            txtKodNapoveda.setVisibility(View.VISIBLE);
            txtVlastniKod.setVisibility(View.GONE);
            txtVlastniKod.setText("");
            btnVygenerujKod.setVisibility(View.VISIBLE);
            btnZrusKod.setVisibility(View.GONE);
            rlSeznamParametru.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Stisk tlačítka zruseni kódu
     */
    public void zrusKod(View view) {
        isRunning = false;
        kodZrusen = true;
        btnZrusKod.setVisibility(View.GONE);
        btnVygenerujKod.setVisibility(View.GONE);
        nastavCekej(ZRUS_KOD);
    }

    private void obrazovka() {
        btnOdesliSMS.setVisibility(View.GONE); // @TODO zde opravit
        txtKodNapoveda.setVisibility(View.GONE);
        txtVlastniKod.setVisibility(View.GONE);
        btnVygenerujKod.setVisibility(View.VISIBLE);
        btnZrusKod.setVisibility(View.GONE);
        waitSpinnerCode.setVisibility(View.GONE);
        txtSpinner.setVisibility(View.GONE);
        rlSeznamParametru.setVisibility(View.VISIBLE);
    }

    @Override
    void vyberTelCisla() {
        nastavCekej(OTEVRI_SEZNAM);
    }

    /**
     * Voláno z čekací procedury
     */
    private void otevriTelefonniSeznam() {
        intent = new Intent(this, ActivitySeznam.class);
        intent.putExtra(def.PREF_PHONE_NUMBER, osoba.getPhoneNumber());
        startActivityForResult(intent, 1);
    }

}
