package cz.fijfij.hlavni;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import cz.fijfij.BuildConfig;
import cz.fijfij.R;
import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.lokace.LatLngItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

/**
 * Created by jiri.kursky on 5.8.2016
 * Základní definice
 * Původně zde byl pokus získat e-mail - nelze, telefonní číslo - nelze
 */
public class def {

    static final int C_MIN_VERZE_ONLINE = 78; // minimální číslo verze, od které funguje on-line přenos

    public static final String PREF_USER_ID = "userId";
    public static final String PREF_VERSION_CODE = "versionCode";
    public static final String PREF_PIN = "pin";
    public static final String PREF_PHONE_NUMBER = "phoneNumber";
    public static final String PREF_POUZE_SMS = "pouzeSMS";
    public static final String PREF_SMS_POVOLENO = "SMSPovoleno";
    public static final String PREF_JE_RIDICI = "jeRidici";

    public static final String PREF_PRVNI_SPUSTENI = "prvniSpusteni"; // úplně první spuštění pro login a ostatní
    public static final String PREF_PRVNI_NAHRANI_SMS = "prvniNahraniSMS"; // Pokud true, budou všechny sms označené jako nahrané
    public static final String PREF_NASTAV_PRISTUPY = "nastavPristupy"; // budou vyžadovány přístupy
    private static final String PREF_ODEMCENY_PIN = "odemcenyPIN";
    public static final String PREF_RYCHLOSTNI_LIMIT = "rychlostniLimit";
    public static final String PREF_INTERVAL_HLAVNI_SMYCKA = "intervalHlavniSmycka";
    static final String PREF_NO_RESTART_SERVICE = "noRestart";
    public static final String PREF_LATITUDE = "latitude";
    public static final String PREF_LONGITUDE = "longitude";
    static final String PREF_SETUP_TIMESTAMP = "setupTimeStamp";
    static final String PREF_POVOLENO_WIFI = "povolenoWiFi";
    static final String PREF_DATA_POVOLENA = "dataPovolena";
    public static final String PREF_NICK = "nick";
    public static final String PREF_REGISTRACE_ZRUSENA = "registraceZrusena";
    static final String PREF_POSLEDNI_CAS = "posledniCas";
    static final String PREF_OSTATNI_NASTAVENI = "ostatniNastaveni";
    public static final String PREF_EMAIL = "email";
    static final String PREF_POSLEDNI_LATITUDE = "posledniLatitude";
    static final String PREF_POSLEDNI_LONGITUDE = "posledniLongitude";
    public static final String PREF_POLOHA_NAZEV = "polohaNazev";
    public static final String PREF_POLOHA_IN = "polohaVstup";
    public static final String PREF_POLOHA_OUT = "polohaVystup";
    public static final String PREF_LAT_LNG = "LatLng";
    public static final String PREF_ZMENA = "zmenaParametru";
    public static final String PREF_SPOJOVACI_KLIC = "spojovaciKlic";
    public static final String PREF_SPOJENI = "spojeni";
    public static final String PREF_NEZOBRAZOVAT_BEH = "nezobrazovatBeh";
    public static final String PREF_NEZOBRAZOVAT_VSEOBECNE_INFORMACE = "nezobrazovatVseobecneInformace";
    public static final String PREF_KLIC_ZMENY = "klicZmeny"; // Užívá se pouze při volání msg ze settingu
    public static final String PREF_TYP_ZJISTENI_LOKACE = "typZjisteniLokace";
    public static final String PREF_BATTERY_PERCENT = "baterie";
    public static final String PREF_MAPA_ZOOM = "mapaZoom";
    public static final String PREF_PRVNI_NASTAVENI = "prvniNastaveni";
    public static final String PREF_RYCHLOST = "rychlost";
    public static final String PREF_CAS_SOURADNIC = "casSouradnic";
    public static final String PREF_ZPRAVA = "zpravaZeSluzby";
    public static final String PREF_SLUZBA_NASTAVENA = "sluzbaNastavena";

    public static final Double LOKACE_TOLEROVANA_VZDALENOST = 80.0; // Okruh tolerance pro pojmenování bodu

    // Co je povoleno systémem
    static final String PREF_PRAVA_SEND_SMS = "pravaSMSPovolena";
    static final String PREF_PRAVA_READ_CONTACTS = "pravaKontaktyPovoleny";
    static final String PREF_PRAVA_ACCESS_FINE_LOCATION = "pravaJemnaLokace";


    // Jednotlivé columns v tabulkách
    // pokud nejsou zadefinované v PREF
    static final String CL_PRITEL_ID = "pritelId";
    static final String CL_CAS = "cas";
    public static final String CL_CAS_PREPSANI = "cas_prepsani";
    public static final String CL_SMS_ID = "smsID"; // identifikátor SMS
    static final String CL_ID_ZDROJ = "idZdroj";
    static final String CL_NEZOBRAZOVAT = "nezobrazovat";
    static final String CL_VERSION = "verze";

    public static final int SERVICE_NOTIFY_ID = 110;
    public static final int SERVICE_INFO_ID = 120;


    public static final int ERR_NENI_ZAPNUTA_SIT = 10;
    public static final int ERR_CHYBA_KOMUNIKACE = 20;
    public static final int ERR_REGISTRACE_ZRUSENA = 30;

    public static final String EX_ACTION = "&action=";
    public static final String EX_ANDROID_ID = "&senderid=";
    public static final String EX_USERID = "&userid=";
    static final String EX_EMAIL = "&email=";
    public static final String EX_PHONE_NUMBER = "&phoneNumber=";
    static final String EX_NICK = "&sender=";
    public static final String EX_PRITELID = "&pritelid=";
    public static final String EX_MSG_TEXT = "&msgText=";
    static final String EX_JE_RIDICI = "&jeRidici=";
    static final String EX_POVOLENO_WIFI = "&povolenoWiFi=";
    static final String EX_DATA_POVOLENA = "&dataPovolena=";
    static final String EX_SMS_POVOLENO = "&smspovoleno=";
    static final String EX_POUZE_SMS = "&pouzesms=";
    static final String EX_SENT_TIME = "&sentTime=";
    static final String EX_SENT_TIME_SEC = "&sentTimeSec=";
    static final String EX_LATITUDE = "&latitude=";
    static final String EX_LONGITUDE = "&longitude=";
    static final String EX_AOS = "&osverze=";
    static final String EX_TIMESTAMP = "&timeStamp=";
    static final String EX_SPOJOVACI_KLIC = "&spojovaciKlic=";
    static final String EX_PIN = "&pin=";
    static final String EX_OSTATNI_NASTAVENI = "&ostatni=";
    public static final String EX_AR_LAT = "&arLat=";
    public static final String EX_AR_LNG = "&arLng=";
    public static final String EX_AR_NAZEV_BODU = "&arNazevBodu=";
    static final String EX_NEPRIRAZENE = "&neprirazene=1";
    static final String EX_TYP_ZJISTENI_LOKACE = "&typZjisteniLokace=";
    static final String EX_ZALOZIL = "&zalozil=";
    static final String EX_BATTERY_PERCENT = "&baterie=";
    static final String EX_VERSION = "&verze=";
    public static final String EX_COUNTRY = "&zeme=";
    static final String EX_CAS_SOURADNIC = "&casSouradnic=";
    static final String EX_CAS_PRECTENO = "&casPrecteno=";
    static final String EX_TYP_VYSTRAHY = "&typVystrahy=";
    public static final String EX_TOKEN = "&fbt=";
    public static final String EX_ALERT_ID = "&alertId=";


    // Návratové hodnoty

    static final String RET_USER_ID = "USERID";
    public static final String RET_NICK = "NICK";
    static final String RET_PHONE_NUMBER = "PHONE_NUMBER";
    static final String RET_PIN = "PIN";
    static final String RET_BAN = "BAN";
    static final String RET_POUZE_SMS = "POUZE_SMS";
    static final String RET_SMS_POVOLENO = "SMS_POVOLENO";
    static final String RET_JE_RIDICI = "JE_RIDICI";
    static final String RET_POVOLENO_WIFI = "POVOLENO_WIFI";
    static final String RET_DATA_POVOLENA = "DATA_POVOLENA";
    static final String RET_NO_RESTART_SERVICE = "NO_RESTART_SERVICE";
    static final String RET_POSLEDNI_LONGITUDE = "POSLEDNI_LONGITUDE";
    static final String RET_POSLEDNI_LATITUDE = "POSLEDNI_LATITUDE";
    static final String RET_POSLEDNI_CAS = "POSLEDNI_CAS";
    public static final String DB_OSTATNI_NASTAVENI = "OSTATNI_NASTAVENI";
    public static final String RET_EMAIL = "EMAIL";
    static final String RET_KLIC = "KLIC";
    static final String RET_SPOJENI = "SPOJENI";
    static final String RET_CAS = "CAS";
    static final String RET_CAS_PREPSANI = "CAS_PREPSANI";
    public static final String RET_PRITELID = "PRITELID";
    public static final String RET_ANDROID_ID = "AID";
    public static final String RET_CAS_ZADANI = "CAS_ZADANI";
    public static final String RET_USERID = "USERID";
    static final String RET_SPOJOVACI_KLIC = "SPOJOVACI_KLIC";
    static final String RET_TIMESTAMP = "TIMESTAMP";
    static final String RET_LONGITUDE = "LONGITUDE";
    static final String RET_LATITUDE = "LATITUDE";
    static final String RET_BATTERY_PERCENT = "BATERIE";
    public static final String RET_ID = "ID";
    static final String RET_TYP_ZJISTENI_LOKACE = "TYP_ZJISTENI_LOKACE";
    static final String RET_TIME = "TIME";
    public static final String RET_VERSION_CODE = "VERSION_CODE";
    public static final String RET_ONLINE_CONTROL = "ONLINE_CONTROL";
    public static final String RET_ONLINE_CONTROL_STOP = "ONLINE_CONTROL_STOP";
    static final String RET_ERROR = "ERROR";
    public static final String RET_KONEC = "KONEC";
    static final String RET_AKTUALNI_STAV = "AKTUALNI_STAV"; // je co číst
    public static final String RET_CAS_PRECTENO = "CAS_PRECTENO";
    public static final String RET_CAS_SOURADNIC = "CAS_SOURADNIC";
    public static final String RET_WARNINGS = "WARNINGS";
    public static final String RET_MSG_ID = "MSG_ID";
    public static final String RET_MSG_TEXT = "MSG_TEXT";
    public static final String RET_DOTAZ = "DOTAZ";

    public static final String AR_GLUE = "~";
    private static final String NULOVY_CAS = "0000-00-00 00:00:00";
    static final String CMD_PREDREGISTRACE = "cmd_predregistrace";


    public static final int W_PREKROCENA_RYCHLOST = 10;
    public static final int W_OBECNA = 20;
    public static final String C_OSOBA = "osoba";


    private static final int PRIPOJENI_WIFI = 10;
    private static final int PRIPOJENI_NETWORK = 20;
    private static final int PRIPOJENI_ZADNE = 30;

    public static final int ST_BAN = 10;
    public static final Integer PREF_MAX_HLAVNI_SMYCKA = 60 * 3;



    public static String android_id;
    static String PIN = "";
    static String sUrl = BuildConfig.serverInfo; // interface pro komunikaci
    public static String sUrlApk = "http://atlantus.cz/cloud/infoo/bin/fijfij.apk"; // bude potřeba později - nemazat!
    public static Integer cisloSestaveniServer = null;
    public static Osoba osoba = null; // udaje o vlastním uživateli ve formátu Osoba

    public static final String CMD_POSLI_NOTIFIKACI = "posli_notifikaci";
    public static final String CMD_ZADEFINOVANY_TOKEN = "fb_token";
    public static final String CMD_CHECK = "check";
    public static final String CMD_VERSION_CODE = "cmd_aktualni_verze"; // Vrátí aktuální verzi zapsanou v serveru - jedná se versionCode
    static final String CMD_PRITEL_ZAPSAN = "cmd_pritel_zapsan";
    static final String CMD_NOVY_UZIVATEL = "cmd_novy_user";
    public static final String CMD_SPOJ_UZIVATELE = "cmd_spoj_uzivatele";
    static final String CMD_UDAJE_USER = "cmd_udaje_user";
    static final String CMD_UPDATE_USER = "cmd_update_user";
    static final String CMD_NACTI_PRATELE = "cmd_nacti_pratele";
    static final String CMD_VYMAZ_PRITELE = "cmd_vymaz_pritele";
    public static final String CMD_ZRUSIT_REGISTRACI = "cmd_zrus_registraci";
    public static final String CMD_NAVRAT_VSECH_SOURADNIC = "cmd_navrat_vsech_souradnic";
    public static final String CMD_SPOJOVACI_KLIC = "cmd_spojovaci_klic";
    public static final String CMD_SMS_PREDNASTAV = "cmd_sms_prednastav";
    static final String CMD_SPOJOVACI_KLIC_ZRUSENI = "cmd_spojovaci_klic_zrus";
    static final String CMD_PAROVANI = "cmd_parovani";
    static final String CMD_SEND_HASH = "cmd_send_hash"; // byl zadán spojovací klíč u slave
    public static final String CMD_NASTAV_BOD = "cmd_nastav_bod";
    static final String CMD_SMS_HASH = "cmd_sms_hash"; // je znám android_id pro daného přítele
    public static final String CMD_ONLINE_START = "cmd_online_start";
    public static final String CMD_OVER_ZADOST = "cmd_over_zadost";
    public static final String CMD_VRAT_ONLINE = "cmd_vrat_online";
    public static final String CMD_ONLINE_STOP = "cmd_online_stop";
    public static final String CMD_DATA_ONLINE = "cmd_data_online";
    public static final String CMD_ONLINE_START_PRITEL = "cmd_online_start_pritel";
    public static final String CMD_VYSTRAHA = "cmd_vystraha";
    public static final String CMD_POSLI_SOS = "cmd_sos";
    public static final String CMD_PRECTENY_ALERT = "cmd_precteny_alert";
    public static final String CMD_BEE_GEES= "cmd_bee_gees"; // dotaz, zda-li dané zařízení žije - Staying Alive
    public static final String CMD_BEE_GEES_ANSWER = "cmd_staying_alive";

    public static final int LOKACE_PROVIDER_ZADNY = 10;
    public static final int LOKACE_PROVIDER_GPS = 20;
    public static final int LOKACE_PROVIDER_NETWORK = 30;

    public static ParametrOp parametrOp;
    private static boolean wasDone = false;
    private static final String NULOVE_DATUM = "30-11-0002 00:00:00";
    public static boolean emulator = false; // komunikuji přes emulátor - jiná IP adresa pro lokální volání
    public static boolean debug = false; // zapnutý debug - zobrazuje navíc informace
    public static boolean novaVerze = false;
    public static PrateleOp prateleOp = null;

    // získaná aktuální pozice
    public static LatLngItem actualLocation = null;


    /**
     * Inicializační funkce
     *
     * @param context context
     */
    public static void factory(Context context) {
        if (wasDone) return;
        // Tyto funkce jsem byl nucen vynechat
        //phoneNumber = getPhoneNumber(context);
        //if (TextUtils.isEmpty(phoneNumber)) phoneNumber = "";
        android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        parametrOp = new ParametrOp();
        wasDone = true;
        emulator = BuildConfig.emulator == 1;
        debug = BuildConfig.debug == 1;
    }


    /**
     * Vrací email
     *
     * @param context Context
     * @return ucet
     */
    @Nullable
    private static Account getAccount(Context context) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            AccountManager accountManager = AccountManager.get(context);
            Account[] accounts = accountManager.getAccounts();
            for (Account account : accounts) {
                if (account.type.equals("com.google")) {
                    return account;
                }
            }
        }
        return null;
    }

    /**
     * Necháno z historických důvodů spíš nefunguje než funguje
     *
     * @param context context
     * @return vlastní telefonní číslo
     */
    private static String getPhoneNumber(@Nullable Context context) {
        if (context == null) return "";
        TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String phoneNumber = "";
        try {
            phoneNumber = tMgr.getLine1Number();
            if (phoneNumber == "null") phoneNumber = "";
        } catch (Exception e) {
            phoneNumber = "";
        }


        SettingOp settingOp = new SettingOp(context);
        String sPhoneNumber = settingOp.getString(PREF_PHONE_NUMBER, "");

        if (TextUtils.isEmpty(phoneNumber)) return sPhoneNumber;
        if (phoneNumber.equals(sPhoneNumber)) return sPhoneNumber;

        settingOp.putString(PREF_PHONE_NUMBER, phoneNumber);
        settingOp.nastavRazitko();

        return phoneNumber;

    }

    public static String vratCas(Message msg, String key) {
        String s = msg.getData().getString(key, "");
        if (nulovyCas(s)) s = "";
        return s;
    }

    /**
     * Vrati vzdalenost mezi souřadnicemi v metrech
     *
     * @param lat1 latitude 1
     * @param lng1 longitude 1
     * @param lat2 latitude 2
     * @param lng2 longitude 2
     * @return vzdalenost v metrech
     */
    static double vzdalenostSouradnic(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //metry
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return (float) (earthRadius * c);
    }

    public static double vzdalenostSouradnic(@Nullable Location location1, @Nullable Location location2) {
        if (location1 == null || location2 == null) return 0;
        return vzdalenostSouradnic(location1.getLatitude(), location1.getLongitude(), location2.getLatitude(), location2.getLongitude());
    }

    public static double vzdalenostSouradnic(@Nullable Location location, double lat2, double lng2) {
        if (location == null) return 0;
        return vzdalenostSouradnic(location.getLatitude(), location.getLongitude(), lat2, lng2);
    }

    /**
     * Nastaví základní údaje o uživateli
     *
     * @param osoba uzivatel
     */
    public static void nastavUzivatele(Osoba osoba) {
        if (osoba == null) return;
        def.osoba = new Osoba(osoba);
    }

    public static boolean vlastniCislo(String cislo) {
        return false;
        /*
        String phoneNumber = def.osoba.getPhoneNumber();
        if (phoneNumber.equals(cislo)) return true;
        int rozdil = cislo.length() - phoneNumber.length();
        if (rozdil == 0) return false;
        if (rozdil > 0) {
            String pCislo = cislo.substring(rozdil);
            return pCislo.equals(phoneNumber);
        } else {
            String pCislo = phoneNumber.substring(phoneNumber.length() + rozdil);
            return pCislo.equals(cislo);
        }
        */
    }

    /**
     * Vrací standardní telefonní číslo
     *
     * @param number zdrojové číslo
     * @return číslo ve standardním tvaru
     */
    public static String phoneNumberStandard(String number) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return PhoneNumberUtils.formatNumber(number, def.getCountry());
        } else {
            //Deprecated method
            return PhoneNumberUtils.formatNumber(number);
        }
    }

    public static boolean getOdemcenyPIN(Context context) {
        SettingOp settingOp = new SettingOp(context);
        return settingOp.getBoolean(PREF_ODEMCENY_PIN, false);
    }

    public static void setOdemcenyPIN(Context context, boolean value) {
        SettingOp settingOp = new SettingOp(context);
        settingOp.putBoolean(PREF_ODEMCENY_PIN, value);
    }

    static boolean dlgNejdeSit(Context context) {
        if (def.isNetworkAllowed(new SettingOp(context))) return false;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.dialog);
        builder.setCancelable(true);
        builder.show();
        return true;
    }

    public static void dlgAlert(Context context, String sMsg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.dialog);
        builder.setCancelable(true);
        builder.setMessage(sMsg);
        builder.setNegativeButton(R.string.dlgOK, null);
        builder.show();
    }

    public static void dlgAlert(Context context, int msg) {
        dlgAlert(context, context.getString(msg));
    }

    /**
     * Definuje format datumu pro mySQL
     *
     * @return format MySQL
     */
    private static SimpleDateFormat getMySQLformat() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.GERMANY);
    }

    /**
     * Definuje format datumu pro mySQL
     *
     * @return format MySQL
     */
    private static SimpleDateFormat getMySQLformatDate() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.GERMANY);
    }

    private static SimpleDateFormat getMySQLformatTime() {
        return new SimpleDateFormat("HH:mm:ss", Locale.GERMANY);
    }

    /**
     * Definuje format datumu pro mySQL
     *
     * @return český formát
     */
    private static SimpleDateFormat getCzechFormat() {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.GERMANY);
    }


    /**
     * Kontroluje nulové datum MySQL
     *
     * @param s vstupní čas
     * @return true pokud je nulovy cas
     */
    public static boolean nulovyCas(@Nullable String s) {
        return TextUtils.isEmpty(s) || s.equals(def.NULOVY_CAS);
    }

    /**
     * Dnešní datum ve formátu MySQL
     *
     * @return dnešní datum
     */
    public static String todaySQL() {
        SimpleDateFormat sdf = getMySQLformat();
        return sdf.format(new Date());
    }

    /**
     * Převod na formát date Android z MySQL
     *
     * @param s datum ve tvaru MySQL
     */
    @Nullable
    static Date stringToDateTime(String s) {
        Date retVal;
        SimpleDateFormat sdf = getMySQLformat();
        try {
            retVal = sdf.parse(s);
        } catch (ParseException e) {
            return null;
        }
        return retVal;
    }


    /**
     * Převod na formát date Android z MySQL
     *
     * @param mysqlDate datum ve tvaru MySQL
     */
    @Nullable
    static Date stringToDate(String mysqlDate) {
        Date retVal;
        SimpleDateFormat sdf = getMySQLformatDate();
        try {
            retVal = sdf.parse(mysqlDate);
        } catch (ParseException e) {
            return null;
        }
        return retVal;
    }

    /**
     * Převod na formát date Android z MySQL
     *
     * @param mysqlDate datum ve tvaru MySQL
     */
    @Nullable
    static Date stringToTime(String mysqlDate) {
        Date retVal;
        SimpleDateFormat sdf = getMySQLformatTime();
        try {
            retVal = sdf.parse(mysqlDate);
        } catch (ParseException e) {
            return null;
        }
        return retVal;
    }

    /**
     * Převod datumu z MySQL do českého formátu
     *
     * @param mysqlDate zdrojový čas v MySQL
     * @return String
     */
    @NonNull
    public static String dateMySQLczech(String mysqlDate) {
        if (TextUtils.isEmpty(mysqlDate)) return "";
        Date date = stringToDateTime(mysqlDate);
        if (date == null) return "";
        SimpleDateFormat sdf = getCzechFormat();
        String retVal = sdf.format(date);
        if (retVal.equals(NULOVE_DATUM)) retVal = "";
        return retVal;
    }


    /**
     * Vrací "dnes", "today"
     *
     * @param mysqlDate datum ve formě mysqlDate
     */
    @NonNull
    public static String verbalDateTime(Context context, String mysqlDate) {
        Date date = stringToDate(mysqlDate);
        if (date == null) return "";

        DateFormat df = getMySQLformatDate();
        String realDate = df.format(date);
        String todayDate = df.format(Calendar.getInstance().getTime());
        if (todayDate.equals(realDate)) {
            date = stringToDateTime(mysqlDate);
            if (date == null) return dateMySQLczech(mysqlDate);
            SimpleDateFormat sdf = getMySQLformatTime();
            String sTime = sdf.format(date);
            return context.getString(R.string.verbalToday) + " " + sTime;
        }
        return dateMySQLczech(mysqlDate);
    }


    static String zobrazNulovyCas(String s) {
        return nulovyCas(s) ? "" : s;
    }

    /**
     * Vlastní převod JSON na bundle
     *
     * @param b    výstupní bundle
     * @param json zdrojové json
     * @return výsledné bundle totožné s json
     */
    static Bundle addJSON(Bundle b, JSONObject json) {
        if (json == null) return null;
        Iterator<String> iter = json.keys();
        Object value;
        while (iter.hasNext()) {
            String key = iter.next();
            try {
                value = json.get(key);
                if (value instanceof String) {
                    String s = json.getString(key);
                    if (s.equals("null")) s = "";
                    b.putString(key, s);
                } else if (value instanceof Integer) {
                    Integer i = json.getInt(key);
                    b.putInt(key, i);
                } else if (value instanceof Long) {
                    Long l = json.getLong(key);
                    b.putLong(key, l);
                }
            } catch (JSONException e) {
                return null;
            }
        }
        return b;
    }

    /**
     * Převádí bezpečně string na integer
     *
     * @param s vstupní string
     * @return int
     */
    @NonNull
    public static int parseInt(String s) {
        if (TextUtils.isEmpty(s)) return 0;
        return Integer.parseInt(s);
    }

    /**
     * Prevadi bezpecne JSON na bundle bundle vytvori jako new
     *
     * @param json vstupni JSON
     * @return bundle
     */
    @Nullable
    public static Bundle transferJSON(@Nullable JSONObject json) {
        if (json == null) return null;
        return addJSON(new Bundle(), json);
    }

    static void showKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        }
    }

    static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    static void hideKeyboard(Context context) {
        hideKeyboard((Activity) context);
    }

    /**
     * Porovnává dva datumy, používá se pro casove razitko
     *
     * @param prvni prvni datum v MySQL formátu
     * @param druhe druhe datum v MySQL formátu
     * @return true, pokud je prvni novější
     */
    static boolean datumPrvniNovejsi(String prvni, String druhe) {
        Date dPrvni = stringToDateTime(prvni);
        Date dDruhe = stringToDateTime(druhe);
        if (dPrvni == null || dDruhe == null) return false;
        return dPrvni.after(dDruhe);
    }

    /**
     * Generuje smsID
     * měl by být pro daného uživatele jednoznačný, protože je generovaný v milisekundách
     *
     * @return smsID
     */
    @NonNull
    public static String vratSMSID() {
        return ((Long) System.currentTimeMillis()).toString();
    }

    /**
     * Porovnává dvě telefonní čísla
     *
     * @param cislo1 první
     * @param cislo2 druhé
     * @return true pokud jsou stejné
     */
    static boolean porovnejTelCislo(Context context, String cislo1, String cislo2) {
        return PhoneNumberUtils.compare(context, cislo1, cislo2);
    }

    /**
     * Funkce na zjištění připojení k datům
     *
     * @return int konstanty připojení
     */
    static int checkStatusData(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnectedOrConnecting()) {
            return PRIPOJENI_WIFI;
        } else if (mobile.isConnectedOrConnecting()) {
            return PRIPOJENI_NETWORK;
        } else {
            return PRIPOJENI_ZADNE;
        }
    }

    /**
     * Je-li k dispozici nějaká sit zároveň v kombinaci s povolenými parametry
     *
     * @return true pokud je k dispozici
     */
    public static boolean isNetworkAllowed(SettingOp settingOp) {
        int status = checkStatusData(settingOp.getContext());
        if (status == PRIPOJENI_ZADNE) return false;
        if (status == PRIPOJENI_WIFI && settingOp.getBoolean(def.PREF_POVOLENO_WIFI, false)) return true;
        return status == PRIPOJENI_NETWORK && settingOp.getBoolean(def.PREF_DATA_POVOLENA, false);
    }


    /**
     * Zkoumá, je-li vůbec síť k dispozici povolena
     *
     * @return vrací true pokud ano
     */
    public static boolean isNetworkAvailable(Context context) {
        return (checkStatusData(context) != PRIPOJENI_ZADNE);
    }

    /**
     * Vrací status řídícího
     *
     * @return true pokud je řídící
     */
    public static boolean jeRidici() {
        if (osoba == null) {
            MyLog.e("DEF", "Dotaz na ridici pri nezadefinovane osobe");
            return false;
        }
        return osoba.getKeyValueBool(PREF_JE_RIDICI);
    }

    /**
     * Bezpečné navrácení id osoby
     */
    @NonNull
    public static String userId() {
        if (osoba == null) return "";
        return osoba.getUserId();
    }

    /**
     * Nastaví bezpečně userId do definice
     *
     * @param userId požadovaná hodnota
     */
    static void setUserId(String userId) {
        if (osoba == null) osoba = new Osoba();
        osoba.setUserId(userId);
    }

    static void setUserParams(Bundle bundle, int typOsoby) {
        if (osoba == null) osoba = new Osoba();
        osoba.nastavParametryBundleAll(bundle, typOsoby);
    }

    static String exTimeStamp() {
        return EX_TIMESTAMP + todaySQL();
    }

    public static String setPIN(Bundle bundle) {
        PIN = bundle.getString(def.RET_PIN, "");
        return PIN;
    }

    public static void setPIN(String aPIN) {
        PIN = aPIN;
    }

    static String getPIN() {
        return PIN;
    }

    public static boolean maPIN() {
        return !TextUtils.isEmpty(PIN);
    }

    public static String getText(Context context, int text, String s) {
        return String.format(context.getString(text), s);
    }

    /**
     * @param activity daný context
     * @param id       pole textu aktivity
     * @param text     text z resources
     * @param s        parametr, který se dosadí do %s
     */
    public static void nastavText(Activity activity, int id, int text, String s) {
        String txt = vytahniText(activity, text, s);
        nastavText(activity, id, txt);
    }

    /**
     * Vytáhne text z resource a dosadí za %s
     *
     * @param text vybráno z resource
     * @param s    dosazená hodnota
     * @return vrací string s formátováním
     */
    @NonNull
    static String vytahniText(Activity activity, int text, String s) {
        return String.format(activity.getString(text), s);
    }


    @Nullable
    public static TextView nastavText(Activity activity, int id, String text) {
        TextView textView = (TextView) activity.findViewById(id);
        if (textView == null) return null;
        textView.setText(text);
        return textView;
    }


    public static void nastavText(Activity activity, int id, int text, int cislo) {
        String s = activity.getString(cislo);
        nastavText(activity, id, text, s);
    }

    @Nullable
    static Bitmap retrieveContactPhoto(Context context, String number) {
        if (TextUtils.isEmpty(number)) return null;
        ContentResolver contentResolver = context.getContentResolver();
        String contactId = null;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID};

        Cursor cursor =
                contentResolver.query(
                        uri,
                        projection,
                        null,
                        null,
                        null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
            }
            cursor.close();
        }

        Bitmap photo = null;
        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(contactId)));

            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream);
                inputStream.close();
            }
        } catch (Exception e) {
            MyLog.e("S", e);
        }
        return photo;
    }

    /**
     * Vrací z kontaktů fotografii
     * Byly zaznamenány pády této funkce, proto byl nastaven try
     */
    @Nullable
    public static Bitmap getPhoto(Context context, Osoba o) {
        Bitmap retVal;
        try {
            retVal = retrieveContactPhoto(context, o.getPhoneNumber());
        } catch (Exception e) {
            retVal = null;
        }
        return retVal;
    }

    @Nullable
    static View findViewByName(Activity activity, String name) {
        int id = activity.getResources().getIdentifier(name, "id", activity.getPackageName());
        if (id == 0) return null;
        return activity.findViewById(id);
    }

    static String utf8encode(String s) {
        String retVal = s;
        try {
            retVal = URLEncoder.encode(retVal, "utf-8");
        } catch (Exception e) {
            retVal = s;
        }
        return retVal;
    }

    static String utf8decode(String s) {
        String retVal = s;
        try {
            retVal = URLDecoder.decode(retVal, "utf-8");
        } catch (Exception e) {
            retVal = s;
        }
        return retVal;
    }

    @NonNull
    public static Boolean hideTextView(@Nullable TextView txt, boolean hide) {
        if (txt == null) return false;
        txt.setVisibility(hide ? View.GONE : View.VISIBLE);
        return true;
    }

    public static int vratTypParametru() {
        return def.jeRidici() ? ParametrOp.PARS_RIDICI : ParametrOp.PARS_CLEN;
    }

    @Nullable
    static String getNick() {
        if (osoba == null) return null;
        return osoba.getNick();
    }

    public static String getVersionName(Context context) {
        String versionName = "";
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionName = packageInfo.versionName;
            if (isXLargeTablet(context)) versionName += " tablet - Beta";
        } catch (Exception e) {
            MyLog.e("SOP", e);
        }
        return versionName;
    }

    public static int getVersionCode(Context context) {
        int versionCode = 0;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionCode = packageInfo.versionCode;
        } catch (Exception e) {
            MyLog.e("SOP", e);
        }
        return versionCode;
    }

    public static int resultGetInt(JSONObject result, String key, int defaultValue) {
        if (result == null) return defaultValue;
        int retVal;
        try {
            retVal = result.getInt(key);
        } catch (Exception e) {
            retVal = defaultValue;
        }
        return retVal;
    }

    /**
     * @return Vrací zemi lokalizace, v případě emulátoru je to vždy CZ
     */
    @NonNull
    public static String getCountry() {
        return Locale.getDefault().getCountry();
    }

    static String bezHacku(String s) {
        if (TextUtils.isEmpty(s)) return "";
        String retVal = "";
        if (s.length() > 1) {
            for (int i = 0; i < s.length(); ++i) {
                retVal += bezHacku("" + s.charAt(i));
            }
            return retVal;
        }
        switch (s) {
            case "ó":
                return ("o");
            case "ě":
            case "é":
                return ("e");
            case "š":
                return ("s");
            case "č":
                return ("c");
            case "ř":
                return ("r");
            case "ž":
                return ("z");
            case "á":
            case "ä":
                return ("a");
            case "ý":
                return ("y");
            case "í":
                return ("i");
            case "ù":
            case "ú":
            case "û":
            case "ü":
            case "ů":
                return ("u");
            case "ň":
                return ("n");
            case "ď":
                return ("d");
            case "ť":
                return ("t");
            case "Ó":
                return ("O");
            case "Ě":
            case "É":
                return ("E");
            case "Š":
                return ("S");
            case "Č":
                return ("C");
            case "Ř":
                return ("R");
            case "Ž":
                return ("Z");
            case "Á":
            case "Ä":
                return ("A");
            case "Ý":
                return ("Y");
            case "Í":
                return ("I");
            case "Ù":
            case "Ú":
            case "Ü":
            case "Ů":
                return ("U");
            case "Ň":
                return ("N");
            case "Ď":
                return ("D");
            case "Ť":
                return ("T");
        }
        return s;
    }

    static void restartApp(Activity activity) {
        Intent i = new Intent(activity, Splash.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(i);
    }

    public static void konec() {
        Runtime.getRuntime().exit(0);
    }

    public static double parseDouble(@Nullable String s) {
        if (TextUtils.isEmpty(s)) return 0.0;
        double retVal = 0;
        try {
            retVal = Double.parseDouble(s);
        } catch (Exception e) {
            retVal = 0;
        }
        return retVal;
    }

    static int getCurrentTimeSec() {
        return Math.round(Calendar.getInstance().getTimeInMillis() / 1000);
    }

    static boolean orientationPortrait(Context context) {
        return context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    public static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static void otazkaUkoncitProgram(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.info);
        builder.setCancelable(true);
        if (def.isNetworkAvailable(activity)) {
            builder.setMessage(R.string.ukoncit_program);
            builder.setPositiveButton(R.string.dlgAno, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    konec();
                }
            });
            builder.setNegativeButton(R.string.dlgNe, null);
        } else {
            builder.setMessage(R.string.pouze_sit_opet);
            builder.setPositiveButton(R.string.dlgOK, null);
        }
        builder.show();
    }

    public static Class<?> getMainActivity(Context context) {
        return isXLargeTablet(context) && jeRidici() ? MainActivityLarge.class : MainActivity.class;
    }

    public static void hlavniStranka(Context context) {
        Intent intent = new Intent(context, getMainActivity(context));
        context.startActivity(intent);
    }

    public static String startCmd(String cmd) {
        return EX_ACTION + cmd + EX_ANDROID_ID + def.android_id;
    }
}