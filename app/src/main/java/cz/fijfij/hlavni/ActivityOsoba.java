/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.fijfij.R;
import cz.fijfij.hlavni.service.FijService;
import cz.fijfij.hlavni.zapouzdreno.MapsActivity;
import cz.fijfij.hlavni.zapouzdreno.SmsControl;

/**
 * Created by jiri.kursky on 01.12.2016.
 * Hlavní stránka se seznamem daných osob
 */

public class ActivityOsoba extends BaseActivity {
    private static final int OTEVRI_MAPU = 10;
    private static final int ODESLI_DOTAZ = 30;
    private static final int VYMAZ_UDAJE = 40;
    private static final int DOPLN_SMS = 50;

    private SmsControl smsControl;
    private CoordinatesOp coordinatesOp = null;
    private AdapterOsoba adapterOsoba;
    private AdapterZadost adapterZadost;
    private ServiceOp serviceOp = null;
    private ListView listGeneral;
    private ListView listZadost;
    private String pritelId;
    private BufferOp bufferOp;
    private SettingOp settingOp;
    private SwipeRefreshLayout swipeLayout;
    private PrateleOp prateleOp;
    private int position;

    public ActivityOsoba() {
        super(R.layout.activity_osoba);
    }

    private final AsyncService.TaskListener souradniceVraceny = new AsyncService.TaskListener() {
        @Override
        public void onFinished(String nameAction, JSONObject result) {
            swipeLayout.setRefreshing(false);
            if (result == null) {
                updateStav();
                return;
            }
            coordinatesOp.clear();
            adapterOsoba.clear();
            try {
                Osoba pritel = vratPritele();
                JSONArray c = result.getJSONArray("DATA");
                for (int i = 0; i < c.length(); i++) {
                    JSONObject udaj = c.getJSONObject(i);
                    Bundle b = def.transferJSON(udaj);
                    if (b != null) {
                        b.putString("pritelId", pritel.getKeyValue(def.PREF_USER_ID));
                        coordinatesOp.add(b, i == 0);
                    }
                }
            } catch (Exception e) {
                Toast.makeText(getBaseContext(), "Chyba v komunikaci", Toast.LENGTH_LONG).show();
            }
            updateStav();
        }
    };

    @Override
    public void cekaniNastaveno(int zdrojVolani) {
        switch (zdrojVolani) {
            case OTEVRI_MAPU:
                SledovaniActivit.startActivity(this, 0, MapsActivity.class);
                return;
            case VYMAZ_UDAJE:
                bufferOp.vymazatSouradnice(pritelId);
                updateStav();
                return;
            case DOPLN_SMS:
                smsControl.doplnArchivRidici(pritelId);
                updateStav();
                return;
            case ODESLI_DOTAZ:
                odesliDotaz();
        }
    }

    Osoba vratPritele() {
        Osoba pritel;
        if (position >= 0) {
            pritel = prateleOp.vratPritele(position);
            if (pritel != null) pritelId = pritel.getUserId();
        } else {
            pritelId = intent.getStringExtra(def.PREF_USER_ID);
            pritel = prateleOp.vratPriteleId(pritelId);
        }
        return pritel;
    }


    /**
     * Součástí je updateStav
     */
    private void odesliDotaz() {
        if (def.isNetworkAvailable(this)) {
            Osoba pritel = vratPritele();
            String action = settingOp.getActionString(def.CMD_NAVRAT_VSECH_SOURADNIC) + def.EX_PRITELID + pritel.getUserId();
            AsyncService as = new AsyncService(souradniceVraceny, this, "VsechnySouradnice", action);
            as.execute();
        } else {
            updateStav();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        bufferOp = new BufferOp(this);
        prateleOp = new PrateleOp(bufferOp, null);

        intent = getIntent();
        position = intent.getIntExtra("position", -1);
        final Osoba pritel = vratPritele();

        super.onCreate(savedInstanceState);
        smsControl = new SmsControl(prateleOp);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                nastavCekejNeSpinner(ODESLI_DOTAZ);
            }
        });

        settingOp = new SettingOp(this);

        coordinatesOp = new CoordinatesOp(pritelId, bufferOp);
        coordinatesOp.aktualizuj(true);
        adapterOsoba = new AdapterOsoba(this, coordinatesOp);
        adapterZadost = new AdapterZadost(this, coordinatesOp);
        listZadost = (ListView) findViewById(R.id.listZadost);
        listZadost.setAdapter(adapterZadost);

        listGeneral = (ListView) findViewById(R.id.listGeneral);
        listGeneral.setAdapter(adapterOsoba);
        listGeneral.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // Zde se nastavují souřadnice
                if (coordinatesOp.jsouNaMape(position)) {
                    //MapsActivity.factory(pritel, coordinatesOp, position);
                }
                nastavCekej(OTEVRI_MAPU);
            }
        });
        registerForContextMenu(listGeneral);
    }


    /**
     * Definice option menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.osoba, menu);
        return true;
    }


    /**
     * Vyber na zĂˇkladÄ› options (tri teÄŤky nahore vpravo)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.vymazat_vsechny) {
            nastavCekej(VYMAZ_UDAJE);
        } else if (id == R.id.doplnit_sms) { // doplnit sms udaje
            nastavCekej(DOPLN_SMS);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * KontextovĂ© menu, pri dlouhĂ©m stisku na jmene
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.listGeneral) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.udalost_menu_list, menu);
        }
    }

    /**
     * Výběr z kontextového menu
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.delete:
                CoordinatesItem ci = coordinatesOp.getCoordinatesItem(info.position);
                if (ci != null) {
                    String idZdroj = ci.getIdZdroj();
                    String smsID = ci.getSmsID();
                    if (!TextUtils.isEmpty(smsID)) {
                        smsControl.vymazSMSUdaj(smsID, pritelId);
                        updateStav();
                    }
                    if (!TextUtils.isEmpty(idZdroj)) {
                        bufferOp.vymazNetUdaj(idZdroj, pritelId);
                        updateStav();
                    }
                }
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void registerClient() {
        if (serviceOp != null) serviceOp.registerClient();
    }

    @Override
    public void onResume() {
        super.onResume();
        Osoba pritel = vratPritele();
        if (serviceOp == null) {
            serviceOp = new ServiceOp(this, serviceIntStart);
            serviceOp.doBindService();
        }
        zrusCekej();
        registerClient();
        if (pritel != null) {
            FloatingActionButton fb = (FloatingActionButton) findViewById(R.id.btnPotvrzeniKodu);
            if (fb != null) {
                fb.setVisibility(pritel.getKeyValueBool(def.PREF_SMS_POVOLENO) ? View.VISIBLE : View.GONE);
            }
            swipeLayout.setEnabled(pritel.getKeyValueBool(def.PREF_POVOLENO_WIFI) || pritel.getKeyValueBool(def.PREF_DATA_POVOLENA));
        } else
            swipeLayout.setEnabled(false);
        nastavCekej(ODESLI_DOTAZ);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (serviceOp != null) {
            serviceOp.unRegisterClient();
            serviceOp.doUnbindService();
            serviceOp = null;
        }
    }

    // Pouziva se pri doruceni sms
    private ServiceOp.ServiceInt serviceIntStart = new ServiceOp.ServiceInt() {
        @Override
        public void onServiceConnected() {
            registerClient();
        }

        @Override
        public void onServiceDisconnected() {
            serviceOp.unRegisterClient();
        }

        @Override
        public boolean receivedMessage(Message msg) {
            switch (msg.what) {
                case FijService.MSG_NOVA_SMS:
                    updateStav();
                    break;
            }
            return false;
        }
    };

    /**
     * Zobrazuje nový stav načtených souřadnic
     */
    private void updateStav() {
        Osoba pritel = vratPritele();
        if (pritel == null) return;
        if (coordinatesOp == null) coordinatesOp = new CoordinatesOp(pritel.getUserId(), bufferOp);
        coordinatesOp.aktualizuj(true);
        adapterOsoba = new AdapterOsoba(this, coordinatesOp);
        adapterOsoba.notifyDataSetChanged(); // aktualizace vlastnĂ­ho listView
        listGeneral.setAdapter(adapterOsoba);
        adapterZadost = new AdapterZadost(this, coordinatesOp);
        adapterZadost.notifyDataSetChanged();
        listZadost.setAdapter(adapterZadost);
        RelativeLayout llPoslane = (RelativeLayout) findViewById(R.id.llPoslane);
        if (coordinatesOp.getArNepotvrzene().size() == 0) {
            llPoslane.setVisibility(View.GONE);
        } else {
            llPoslane.setVisibility(View.VISIBLE);
        }
        swipeLayout.setRefreshing(false);
        zrusCekej();
    }


    /**
     * OdeĹˇle ĹľĂˇdost o souĹ™adnice pĹ™es sms spolu s dialogem, mĂˇ-li
     */
    public void odesliSMS(View view) {
        final Osoba pritel = vratPritele();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.info);
        builder.setCancelable(true);

        if (TextUtils.isEmpty(pritel.getPhoneNumber())) {
            builder.setMessage(R.string.neni_nastaveno_cislo);
            builder.setNegativeButton(R.string.dlgOK, null);
        } else {
            builder.setMessage(R.string.skutecne_odeslat_sms);
            builder.setPositiveButton(R.string.dlgAno, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    smsControl.askCoor(pritel);
                    updateStav();
                }
            });
            builder.setNegativeButton(R.string.dlgNe, null);
        }
        builder.show();
    }

    /**
     * Do titulu se dopise o koho jde
     */
    @Override
    public void nastavActionBar() {
        if (actionBar == null) return;
        Osoba pritel = vratPritele();
        if (pritel != null) {
            actionBar.setSubtitle(pritel.getNick());
        }
    }
}