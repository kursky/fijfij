/*
 * Copyright (c) 2017 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import cz.fijfij.R;

/**
 * Created by jiri.kursky on 08.01.2017.
 *
 */

public class AdapterSeznam extends ArrayAdapter<String> {
    private Activity activity;
    private ArrayList<String> contactNames;
    private ArrayList<String> phoneNumbers;
    private ArrayList<Bitmap> photos;

    AdapterSeznam(Activity activity, ArrayList<String> contactNames, ArrayList<String> phoneNumbers, ArrayList<Bitmap> photos) {
        super(activity, R.layout.list_single_osoba, contactNames);
        this.activity = activity;
        this.contactNames = contactNames;
        this.phoneNumbers = phoneNumbers;
        this.photos=photos;
    }

    @Override
    @NonNull
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_single, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txtNick);
        String contact = contactNames.get(position);
        txtTitle.setText(contact);
        TextView txtSubText = (TextView) rowView.findViewById(R.id.txtPosledniCas);
        String phoneNumber=phoneNumbers.get(position);
        txtSubText.setText(phoneNumber);
        Bitmap photo=photos.get(position);
        if (photo!=null) {
            ImageView img=(ImageView) rowView.findViewById(R.id.imgAvatar);
            img.setImageBitmap(photo);
        }
        return rowView;
    }
}