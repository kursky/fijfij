/*
 * Copyright (c) 2017 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import org.json.JSONObject;

import cz.fijfij.R;

public class ActivityCekaniSpojeni extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        def.factory(this);
        setContentView(R.layout.activity_cekani_spojeni);
    }

    public void zrusCekani(View view) {
        final Context context=this;
        AsyncService as = new AsyncService(
                new AsyncService.TaskListener() {
                    @Override
                    public void onFinished(String nameAction, JSONObject result) {
                        startActivity(new Intent(context, UvodniLogin.class));
                    }
                }, getBaseContext(), def.CMD_ZRUSIT_REGISTRACI);
        as.execute();
    }
}
