/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import cz.fijfij.R;
import cz.fijfij.hlavni.nastaveni.ParametrItem;
import cz.fijfij.hlavni.service.FijService;

/**
 * Created by jiri.kursky on 03.12.2016
 * Viditelné parametry pro editaci přítele
 */

public class AdapterPritelKarta extends ArrayAdapter<String> {
    private Activity activity;
    private ServiceOp serviceOp;
    private final ListView listView;
    private Osoba osoba;
    private PrateleOp prateleOp;

    AdapterPritelKarta(Activity activity, ServiceOp serviceOp, Osoba osoba, ListView listView) {
        super(activity, R.layout.list_parametry, def.parametrOp.getAllKeys(ParametrOp.PARS_CLEN_VIDITELNE));
        this.activity = activity;
        this.serviceOp = serviceOp;
        this.listView = listView;
        this.osoba = osoba;
        this.prateleOp = new PrateleOp(new BufferOp(activity), null);
    }



    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_parametry, null, true);
        if (osoba == null) return rowView;
        TextView txtNazevParametru = (TextView) rowView.findViewById(R.id.txtNazevParametru);
        TextView txtSubParametr = (TextView) rowView.findViewById(R.id.txtSubNazevParametru);
        Switch prepinac = (Switch) rowView.findViewById(R.id.prepinac);
        ParametrItem parametrItem = def.parametrOp.getParametrItem(position, ParametrOp.PARS_CLEN_VIDITELNE);
        txtNazevParametru.setText(parametrItem.getNazev());

        String klic = parametrItem.getKlic();
        switch (parametrItem.getTyp()) {
            case ParametrItem.PAR_STRING:
                txtSubParametr.setText(osoba.getKeyValue(klic));
                prepinac.setVisibility(View.INVISIBLE);
                break;
            case ParametrItem.PAR_BOOLEAN:
                txtSubParametr.setVisibility(View.GONE); // není potřeba text u přepínače, ten se definuje vlevo, vypadá to blbě
                prepinac.setChecked(osoba.getKeyValueBool(klic));
                prepinac.setVisibility(View.VISIBLE);

                prepinac.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        int position = listView.getPositionForView(buttonView);
                        ParametrItem parametrItem = def.parametrOp.getParametrItem(position, ParametrOp.PARS_CLEN_VIDITELNE);
                        String klic = parametrItem.getKlic();
                        switch (parametrItem.getTyp()) {
                            case ParametrItem.PAR_BOOLEAN:
                                osoba.setKeyValue(klic, isChecked);
                                break;
                        }
                        prateleOp.setParametres(osoba);
                        serviceOp.sendMessageToService(FijService.MSG_ZMENA_PARAMETRU_PRITEL, osoba.getBundle());
                        // Odeslani sluzbe, ze se zmenili parametry
                    }
                });
                break;
        }
        return rowView;
    }
}
