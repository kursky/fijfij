package cz.fijfij.hlavni;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.service.FijService;

/**
 * Created by jiri.kursky on 5.8.2016.
 * Slouží ke spojení s FijService
 * Má v sobě přímo bind
 */

public class ServiceOp {
    private static final String TAG = "SOP";
    private Messenger msgService = null;
    private Messenger mMessenger;
    private Context context;
    private boolean mIsBound = false;
    private ServiceConnection mConnection;
    private ServiceInt serviceInt;


    public void registerClient() {
        sendMessageToService(FijService.MSG_REGISTER_CLIENT, 0, null);
    }

    public void unRegisterClient() {
        sendMessageToService(FijService.MSG_UNREGISTER_CLIENT, 0, null);
    }

    public interface ServiceInt {
        void onServiceConnected();

        void onServiceDisconnected();

        boolean receivedMessage(Message msg);
    }

    public ServiceConnection getMConnection() {
        return mConnection;
    }

    public ServiceOp(Context context, final ServiceInt serviceInt) {
        this.context = context;
        mMessenger = new Messenger(new IncomingHandler());
        this.serviceInt = serviceInt;
        mIsBound = false;
        mConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                msgService = new Messenger(service);
                mIsBound = true;
                if (serviceInt != null) serviceInt.onServiceConnected();
            }

            public void onServiceDisconnected(ComponentName className) {
                // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
                msgService = null;
                mIsBound = false;
                if (serviceInt != null) serviceInt.onServiceDisconnected();
            }
        };
    }


    public boolean isBounded() {
        return mIsBound;
    }


    public void doBindService() {
        if (mIsBound) return;
        context.bindService(new Intent(context, FijService.class), mConnection, Context.BIND_AUTO_CREATE);
    }

    /**
     * Unbind
     */
    public void doUnbindService() {
        if (mIsBound) {
            // Detach our existing connection.
            context.unbindService(mConnection);
            mIsBound = false;
        }
    }


    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            if (serviceInt.receivedMessage(msg)) return;
            super.handleMessage(msg);

        }
    }

    public void sendMessageToService(int cmd, int arg1, @Nullable Bundle b) {
        if (mIsBound) {
            if (msgService != null) {
                try {
                    Message msg = Message.obtain(null, cmd, arg1, 0);
                    msg.replyTo = mMessenger;
                    if (b != null) msg.setData(b);
                    msgService.send(msg);
                } catch (RemoteException e) {
                    MyLog.e(TAG, e);
                }
            }
        }
    }

    public void sendMessageToService(int cmd) {
        sendMessageToService(cmd, 0, null);
    }

    public void sendMessageToService(int cmd, Bundle bundle) {
        sendMessageToService(cmd, 0, bundle);
    }

    public void sendMessageToService(int cmd, int arg1) {
        sendMessageToService(cmd, arg1, null);
    }

    public void sendMessageToService(int cmd, String arg) {
        sendMessageToService(cmd, def.parseInt(arg), null);
    }

    public void startService() {
        try {
            Intent intent=new Intent(context.getApplicationContext(), FijService.class);
            intent.addCategory(TAG);
            context.startService(intent);

        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
    }

    public void stopService() {
        try {
            doUnbindService();
            Intent intent=new Intent(context.getApplicationContext(), FijService.class);
            intent.addCategory(TAG);
            context.stopService(intent);
            FijService.cancelNotification(context);
            mIsBound = false;
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
    }

}
