/*
 * Copyright (c) 2017 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by jiri.kursky on 10.01.2017
 * class zobrazující čekání - přepínánín mezi obrazovkami
 */

public class CekaniDef {
    private Activity activity;
    private DefCekani defCekani;
    private static final int CEKEJ_TIME = 500;

    public interface DefCekani {
        void nastaveno(int zdrojVolani);
    };

    public interface PoNastaveni {
        void akce();
    }

    public CekaniDef(Activity activity, @Nullable DefCekani defCekani) {
        this.activity=activity;
        this.defCekani=defCekani;
    }


    void doNastavCekej(boolean ano) {
        View llAktivni;
        View llCekej;

        llAktivni = def.findViewByName(activity,"ll_aktivni");
        llCekej = def.findViewByName(activity, "ll_cekej");
        if (llCekej != null) llCekej.setVisibility((ano) ? View.VISIBLE : View.GONE);
        if (llAktivni != null) llAktivni.setVisibility((ano) ? View.GONE : View.VISIBLE);
    }

    public void zrus() {
        doNastavCekej(false);
    }

    void nastaveno(int zdrojVolani) {
        if (defCekani!=null) {
            defCekani.nastaveno(zdrojVolani);
        }
        else {
            zrus();
        }
    }

    void nastav(final int zdrojVolani) {
        schovejKlavesnici();
        doNastavCekej(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                    nastaveno(zdrojVolani);
            }
        }, CEKEJ_TIME);
    }

    private void schovejKlavesnici() {
        View view = activity.getCurrentFocus();

        // Schování klávesnice
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void nastav(@NonNull final PoNastaveni poNastaveni) {
        schovejKlavesnici();
        doNastavCekej(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                poNastaveni.akce();
            }
        }, CEKEJ_TIME);
    }

    /**
     * Stejně jako @link nastavCekej ale nezobrazuje čekací spinnner
     *
     * @param zdrojVolani parametr, ktery se pote predava cekaniNastaveno
     */
    void neSpinner(final int zdrojVolani) {
        schovejKlavesnici();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                nastaveno(zdrojVolani);
            }
        }, CEKEJ_TIME);
    }

    void nastavCekejTime(final Integer zdrojVolani, int time) {
        schovejKlavesnici();
        doNastavCekej(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                nastaveno(zdrojVolani);
            }
        }, time);

    }
}
