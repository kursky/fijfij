/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni.nastaveni;


import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.support.v7.app.ActionBar;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.MenuItem;

import cz.fijfij.R;
import cz.fijfij.hlavni.AppCompatPreferenceActivity;
import cz.fijfij.hlavni.MyPreferenceFragment;
import cz.fijfij.hlavni.SledovaniActivit;
import cz.fijfij.hlavni.def;
import cz.fijfij.hlavni.service.FijService;

import java.util.List;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends AppCompatPreferenceActivity {
    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static boolean frameActive;


    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String key = preference.getKey();
            String stringValue = value.toString();
            if (key.equals(def.PREF_PHONE_NUMBER)) stringValue = def.phoneNumberStandard(stringValue);

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);


            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }
    };


    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
    }


    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return def.isXLargeTablet(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        if (def.jeRidici()) {
            loadHeadersFromResource(R.xml.pref_headers_ridici, target);
        } else {
            loadHeadersFromResource(R.xml.pref_headers, target);
        }
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || GeneralPreferenceFragment.class.getName().equals(fragmentName)
                || ServicePreferenceFragment.class.getName().equals(fragmentName)
                || WarningPreferenceFragment.class.getName().equals(fragmentName)
                || SMSPreferenceFragment.class.getName().equals(fragmentName);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (frameActive) {
                return super.onOptionsItemSelected(item);
            } else {
                def.hlavniStranka(this);
                return true;
            }
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends MyPreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            frameActive = true;
            if (def.jeRidici()) {
                addPreferencesFromResource(R.xml.pref_vseobecne_ridici);
            } else {
                addPreferencesFromResource(R.xml.pref_vseobecne_clen);
            }
            setHasOptionsMenu(true);


            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.

            bindPreferenceSummaryToValue(findPreference(def.PREF_PHONE_NUMBER));
            EditTextPreference nickName = (EditTextPreference) findPreference(def.PREF_NICK);
            if (nickName != null) {
                nickName.setEnabled(def.jeRidici());
                bindPreferenceSummaryToValue(findPreference(def.PREF_NICK));
            }

            EditTextPreference email = (EditTextPreference) findPreference(def.PREF_EMAIL);
            if (email != null) {
                email.setEnabled(true);
                bindPreferenceSummaryToValue(findPreference(def.PREF_EMAIL));
            }

        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onStop() {
            frameActive = false;
            super.onStop();
        }


        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                /*
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                */
                SledovaniActivit.startActivity(getActivity(), 0, SettingsActivity.class);
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Bundle bundle = new Bundle();
            int typ = def.parametrOp.getParameterType(key);
            if (typ == ParametrItem.PAR_STRING) {
                String hodnota = sharedPreferences.getString(key, "");
                /*
                if (key.equals(def.PREF_PHONE_NUMBER)) {
                    hodnota=def.phoneNumberStandard(hodnota);
                    sharedPreferences.edit().putString(key, hodnota).apply();
                }
                */
                bundle.putString(key, hodnota);
            } else if (typ == ParametrItem.PAR_BOOLEAN) {
                boolean hodnota = sharedPreferences.getBoolean(key, false);
                bundle.putInt(key, hodnota ? 1 : 0);
            }
            bundle.putString(def.PREF_KLIC_ZMENY, key);
            sendZmenaParametru(bundle);
        }
    }

    /**
     * This fragment shows notification preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class SMSPreferenceFragment extends MyPreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            frameActive = true;
            addPreferencesFromResource(R.xml.pref_comm);
            setHasOptionsMenu(true);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                SledovaniActivit.startActivity(getActivity(), 0, SettingsActivity.class);
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Bundle bundle = new Bundle();
            bundle.putString(def.PREF_KLIC_ZMENY, key);
            sendZmenaParametru(bundle);
        }

        @Override
        public void onStop() {
            frameActive = false;
            super.onStop();
        }
    }

    /**
     * This fragment shows notification preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class WarningPreferenceFragment extends MyPreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            frameActive = true;
            addPreferencesFromResource(R.xml.pref_warning);
            setHasOptionsMenu(true);
            bindPreferenceSummaryToValue(findPreference(def.PREF_RYCHLOSTNI_LIMIT));
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                SledovaniActivit.startActivity(getActivity(), 0, SettingsActivity.class);
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        }

        @Override
        public void onStop() {
            frameActive = false;
            super.onStop();
        }
    }

    /**
     * This fragment shows data and sync preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class ServicePreferenceFragment extends MyPreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            frameActive = true;
            int preferencesResId = def.jeRidici() ? R.xml.pref_service_ridici : R.xml.pref_service;
            addPreferencesFromResource(preferencesResId);
            setHasOptionsMenu(true);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            if (!def.jeRidici()) {
                bindPreferenceSummaryToValue(findPreference(def.PREF_INTERVAL_HLAVNI_SMYCKA));
            }
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                SledovaniActivit.startActivity(getActivity(), 0, SettingsActivity.class);
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        @Override
        public void onStop() {
            frameActive = false;
            super.onStop();
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
            super.onPause();
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Bundle bundle = new Bundle();
            bundle.putString(def.PREF_KLIC_ZMENY, key);
            if (key.equals(def.PREF_INTERVAL_HLAVNI_SMYCKA)) {
                sendMessage(FijService.MSG_INTERVAL_HLAVNI_SMYCKA, null);
            } else {
                if (key.equals(def.PREF_NEZOBRAZOVAT_BEH)) {
                    boolean nezobrazovatBeh = sharedPreferences.getBoolean(key, false);
                    if (nezobrazovatBeh) {
                        FijService.cancelNotification(getActivity());
                    }
                    bundle.putBoolean(def.PREF_NEZOBRAZOVAT_BEH, nezobrazovatBeh);
                }
            }
            sendZmenaParametru(bundle);
        }
    }
}
