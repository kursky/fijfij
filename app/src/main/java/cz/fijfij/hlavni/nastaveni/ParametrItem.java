/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni.nastaveni;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.text.InputType;
import android.widget.EditText;

/**
 * Created by jiri.kursky on 03.12.2016.
 * obecna položka - parametr nebo editovatelná hodnota
 */

public class ParametrItem {
    private String retKlic;
    private String sPocatecniHodnota;
    private Boolean bPocatecniHodnota;
    private boolean zakazEditace = false;
    private String klic;
    private int nazev;
    private int typ;
    private int maxDelka = 20;
    private boolean nezobrazovat = false;
    private EditText dlgEditText = null;


    public static final int PAR_BOOLEAN = 1;
    public static final int PAR_STRING = 2;
    private int inputType = InputType.TYPE_TEXT_FLAG_CAP_SENTENCES;
    private boolean pouzeRidici = false;
    private boolean pouzeClen = false;
    private String valueString;
    private Boolean valueBoolean;

    public ParametrItem() {
    }

    public ParametrItem(String klic, @Nullable String retKlic, int nazev, int typ, String pocatecniHodnota, int maxDelka) {
        this.klic = klic;
        this.retKlic = retKlic;
        this.nazev = nazev;
        this.typ = typ;
        this.sPocatecniHodnota = pocatecniHodnota;
        this.maxDelka = maxDelka;
    }

    public ParametrItem(String klic, @Nullable String retKlic, int nazev, int typ, Boolean pocatecniHodnota) {
        this.klic = klic;
        this.nazev = nazev;
        this.typ = typ;
        this.retKlic = retKlic;
        this.bPocatecniHodnota = pocatecniHodnota;
        valueBoolean = pocatecniHodnota;
    }

    public ParametrItem(String klic, String retKlic) {
        this.klic = klic;
        this.typ = PAR_STRING;
        this.sPocatecniHodnota = "";
        this.retKlic = retKlic;
    }

    public int getNazev() {
        return nazev;
    }

    public int getTyp() {
        return typ;
    }

    public String getKlic() {
        return klic;
    }

    public boolean getDefaultBool() {
        return bPocatecniHodnota;
    }

    public int getDefaultBoolInt() {
        return bPocatecniHodnota ? 1 : 0;
    }

    public String getDefaultString() {
        if (sPocatecniHodnota == null) sPocatecniHodnota = "";
        return sPocatecniHodnota;
    }

    @Nullable
    public String getRetKlic() {
        return retKlic;
    }


    public boolean getNezobrazovat() {
        return nezobrazovat;
    }

    public ParametrItem setInputType(int inputType) {
        this.inputType = inputType;
        return this;
    }

    int getInputType() {
        return inputType;
    }

    public ParametrItem setZakazEditace() {
        zakazEditace = true;
        return this;
    }


    public EditText setDlgEditText(Context context, String hodnota) {
        dlgEditText = new EditText(context);
        if (maxDelka > 0) {
            dlgEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxDelka)});
        }
        dlgEditText.setText(hodnota);
        dlgEditText.setInputType(getInputType());
        dlgEditText.selectAll();
        return dlgEditText;
    }

    public EditText setDlgEditText(Context context) {
        return setDlgEditText(context, "");
    }

    public EditText getDlgEditText() {
        return dlgEditText;
    }

    public void setNazev(int nazev) {
        this.nazev = nazev;
    }

    public ParametrItem setPouzeClen() {
        pouzeClen = true;
        return this;
    }

    public ParametrItem setNezobrazovat() {
        nezobrazovat = true;
        return this;
    }

    public ParametrItem setRidiciPouze() {
        pouzeRidici = true;
        return this;
    }

    public boolean getPouzeRidici() {
        return pouzeRidici;
    }

    public boolean getPouzeClen() {
        return pouzeClen;
    }

    public String getValueString() {
        return valueString;
    }

    public boolean getValueBoolean() {
        return valueBoolean;
    }

    public void setValueBoolean(Boolean yes) {
        valueBoolean = yes;
    }

    public void setValueString(String valueString) {
        if (valueString == null) valueString = "";
        this.valueString = valueString;
    }

    public void setValueString(double valueString) {
        setValueString(Double.toString(valueString));
    }

    public boolean getZakazEditace() {
        return zakazEditace;
    }
}
