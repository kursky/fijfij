package cz.fijfij.hlavni;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import cz.fijfij.R;
import cz.fijfij.hlavni.lokace.LatLngItem;

import java.util.ArrayList;

/**
 * Created by jiri.kursky on 5.8.2016.
 * přehled přátel adapter
 */
public class AdapterPratele extends ArrayAdapter<String> {
    private BufferOp bufferOp;
    private final Activity context;
    private final ArrayList<Osoba> arOsoba;


    public AdapterPratele(Activity activity, PrateleOp prateleOp) {
        super(activity, def.jeRidici() ? R.layout.list_single_pozice : R.layout.list_single, prateleOp.arNick());
        bufferOp = new BufferOp(activity);
        this.context = activity;
        arOsoba = prateleOp.getArPratele();
    }

    @Override
    @NonNull
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(def.jeRidici() ? R.layout.list_single_pozice : R.layout.list_single, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txtNick);
        if (position >= arOsoba.size()) return rowView;

        Osoba osoba = arOsoba.get(position);
        txtTitle.setText(osoba.getNick());
        TextView txtNazevMista = (TextView) rowView.findViewById(R.id.txtNazevMista);
        if (txtNazevMista != null) txtNazevMista.setText("");
        TextView txtPosledniAktivita = (TextView) rowView.findViewById(R.id.txtPosledniCas);
        if (txtPosledniAktivita != null) txtPosledniAktivita.setText("");

        TextView txtBaterie = (TextView) rowView.findViewById(R.id.txtBaterie);
        if (txtBaterie != null) txtBaterie.setText("");

        ImageView imgBaterie = (ImageView) rowView.findViewById(R.id.imgBattery);
        if (imgBaterie != null) imgBaterie.setVisibility(View.GONE);

        // Zkoumá se, jestli je spojený
        if (osoba.spojeniNepotvrzeno()) {
            if (txtNazevMista != null) {
                rowView.setBackgroundColor(Color.argb(45, 45, 45, 45));
                String nazevBodu = (def.jeRidici()) ? String.format(context.getString(R.string.par_klic_s), osoba.getString(def.PREF_SPOJOVACI_KLIC)) : "";
                txtNazevMista.setText(nazevBodu);
                if (txtPosledniAktivita != null) txtPosledniAktivita.setText(osoba.getPhoneNumber());
            }

        } else {
            if (txtNazevMista != null) {
                if (def.jeRidici()) {
                    String latitude = osoba.getString(def.PREF_POSLEDNI_LATITUDE);
                    String longitude = osoba.getString(def.PREF_POSLEDNI_LONGITUDE);
                    if (!TextUtils.isEmpty(latitude)) {
                        try {
                            LatLngItem latLngItem = new LatLngItem(latitude, longitude, bufferOp);
                            String nazevBodu = latLngItem.vratNazevBodu();
                            if (nazevBodu != null) {
                                txtNazevMista.setText(nazevBodu);
                            } else {
                                txtNazevMista.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            txtNazevMista.setVisibility(View.GONE);
                        }
                    }
                }
            }
            if (txtPosledniAktivita != null) txtPosledniAktivita.setText(def.verbalDateTime(getContext(),osoba.getPosledniCas()));
            String baterka = osoba.getBaterie();

            if (!TextUtils.isEmpty(baterka)) {
                if (txtBaterie != null) txtBaterie.setText(osoba.getBaterie());
                if (imgBaterie != null) {
                    int iBaterka = osoba.getBaterieInt();
                    int srcBaterka = R.drawable.battery_50;
                    if (iBaterka > 80) {
                        srcBaterka = R.drawable.battery_100;
                    } else if (iBaterka > 60) {
                        srcBaterka = R.drawable.battery_80;
                    } else if (iBaterka > 40) {
                        srcBaterka = R.drawable.battery_50;
                    } else if (iBaterka > 20) {
                        srcBaterka = R.drawable.battery_20;
                    } else
                        srcBaterka = R.drawable.battery_0;

                    imgBaterie.setImageResource(srcBaterka);
                    imgBaterie.setVisibility(View.VISIBLE);
                }

            }
        }


        Bitmap photo = def.getPhoto(context, osoba);
        if (photo != null) {
            ImageView img = (ImageView) rowView.findViewById(R.id.imgAvatar);
            img.setImageBitmap(photo);
        }
        return rowView;
    }
}