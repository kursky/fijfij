/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONObject;

import cz.fijfij.R;
import cz.fijfij.hlavni.base.Err;

/**
 * Created by jiri.kursky on 16.12.2016
 * za pomoci stack overflow
 */


public class PinEntryView extends Activity {
    String userEntered;
    String userPin = "8888";

    private long then;
    private int longClickDuration = 5000; //for long click to trigger after 5 seconds

    final int PIN_LENGTH = 4;
    boolean keyPadLockedFlag = false;
    private Context context;

    TextView titleView;

    TextView pinBox0;
    TextView pinBox1;
    TextView pinBox2;
    TextView pinBox3;


    TextView statusView;

    Button button0;
    Button button1;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;
    Button button7;
    Button button8;
    Button button9;
    Button button10;
    Button buttonExit;
    Button buttonDelete;
    EditText passwordInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userPin = def.getPIN();
        String nick = def.getNick();

        context = this;
        userEntered = "";


        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.pin_layout);

        TextView txtNick = (TextView) findViewById(R.id.txtNick);
        if (TextUtils.isEmpty(nick)) {
            txtNick.setVisibility(View.GONE);
        } else {
            txtNick.setText(nick);
        }

        ImageView imageView = (ImageView) findViewById(R.id.logo);
        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    then = (long) System.currentTimeMillis();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    if ((System.currentTimeMillis() - then) > longClickDuration) {
                        zasliSOS();
                    }
                }
                return true;
            }
        });

        //Typeface xpressive=Typeface.createFromAsset(getAssets(), "fonts/XpressiveBold.ttf");

        statusView = (TextView) findViewById(R.id.statusView);
        passwordInput = (EditText) findViewById(R.id.txtZadanyPin);
        //backSpace = (ImageView) findViewById(R.id.imageView);
        buttonExit = (Button) findViewById(R.id.buttonExit);
        /*
        backSpace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passwordInput.setText(passwordInput.getText().toString().substring(0, passwordInput.getText().toString().length() - 2));
            }
        });
        */
        buttonExit.setText("");
        buttonExit.setOnClickListener(new View.OnClickListener() {
                                          public void onClick(View v) {


                                              //Exit app
                                              /*
                                              Intent i = new Intent();
                                              i.setAction(Intent.ACTION_MAIN);
                                              i.addCategory(Intent.CATEGORY_HOME);
                                              context.startActivity(i);
                                              finish();
                                              */

                                          }

                                      }
        );
        //buttonExit.setTypeface(xpressive);

        passwordInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    def.hideKeyboard(context);

                }
            }
        });


        buttonDelete = (Button) findViewById(R.id.buttonDeleteBack);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
                                            public void onClick(View v) {

                                                if (keyPadLockedFlag == true) {
                                                    return;
                                                }

                                                if (userEntered.length() > 0) {
                                                    userEntered = userEntered.substring(0, userEntered.length() - 1);
                                                    passwordInput.setText("");
                                                }


                                            }

                                        }
        );

        //titleView = (TextView) findViewById(R.id.logo);
        //titleView.setTypeface(xpressive);


        View.OnClickListener pinButtonHandler = new View.OnClickListener() {
            public void onClick(View v) {

                if (keyPadLockedFlag == true) {
                    return;
                }

                Button pressedButton = (Button) v;


                if (userEntered.length() < PIN_LENGTH) {
                    userEntered = userEntered + pressedButton.getText();
                    Log.v("PinView", "User entered=" + userEntered);

                    //Update pin boxes
                    passwordInput.setText(passwordInput.getText().toString() + "*");
                    passwordInput.setSelection(passwordInput.getText().toString().length());

                    if (userEntered.length() == PIN_LENGTH) {
                        //Check if entered PIN is correct
                        if (userEntered.equals(userPin)) {
                            statusView.setTextColor(Color.BLACK);
                            statusView.setText(R.string.spravne);
                            Log.v("PinView", "Correct PIN");
                            def.setOdemcenyPIN(context, true);
                            finish();
                        } else {
                            statusView.setTextColor(Color.RED);
                            statusView.setText(R.string.chybny_pin);
                            keyPadLockedFlag = true;
                            Log.v("PinView", "Wrong PIN");

                            new LockKeyPadOperation().execute("");
                        }
                    }
                } else {
                    //Roll over
                    passwordInput.setText("");

                    userEntered = "";

                    statusView.setText("");

                    userEntered = userEntered + pressedButton.getText();
                    Log.v("PinView", "User entered=" + userEntered);

                    //Update pin boxes
                    passwordInput.setText("8");

                }


            }
        };


        button0 = (Button) findViewById(R.id.button0);
        //button0.setTypeface(xpressive);
        button0.setOnClickListener(pinButtonHandler);

        button1 = (Button) findViewById(R.id.button1);
        //button1.setTypeface(xpressive);
        button1.setOnClickListener(pinButtonHandler);

        button2 = (Button) findViewById(R.id.button2);
        //button2.setTypeface(xpressive);
        button2.setOnClickListener(pinButtonHandler);


        button3 = (Button) findViewById(R.id.button3);
        //button3.setTypeface(xpressive);
        button3.setOnClickListener(pinButtonHandler);

        button4 = (Button) findViewById(R.id.button4);
        //button4.setTypeface(xpressive);
        button4.setOnClickListener(pinButtonHandler);

        button5 = (Button) findViewById(R.id.button5);
        //button5.setTypeface(xpressive);
        button5.setOnClickListener(pinButtonHandler);

        button6 = (Button) findViewById(R.id.button6);
        //button6.setTypeface(xpressive);
        button6.setOnClickListener(pinButtonHandler);

        button7 = (Button) findViewById(R.id.button7);
        //button7.setTypeface(xpressive);
        button7.setOnClickListener(pinButtonHandler);

        button8 = (Button) findViewById(R.id.button8);
        //button8.setTypeface(xpressive);
        button8.setOnClickListener(pinButtonHandler);

        button9 = (Button) findViewById(R.id.button9);
        //button9.setTypeface(xpressive);
        button9.setOnClickListener(pinButtonHandler);


        buttonDelete = (Button) findViewById(R.id.buttonDeleteBack);
        //buttonDelete.setTypeface(xpressive);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                def.hideKeyboard(context);
            }
        }, 500);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //App not allowed to go back to Parent activity until correct pin entered.
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.activity_pin_entry_view, menu);
        return true;
    }


    private class LockKeyPadOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            for (int i = 0; i < 2; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            statusView.setText("");

            //Roll over
            passwordInput.setText("");
            ;

            userEntered = "";

            keyPadLockedFlag = false;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    /**
     * Funkce, která zasílá SOS - zatím nic, nutno dopracovat, pokud nejsou souřadnice apod.
     */
    private void zasliSOS() {
        Context context = this;
        String action = def.startCmd(def.CMD_POSLI_SOS);
        action += def.EX_USERID + def.userId();
        PrateleOp prateleOp=def.prateleOp;

        // Fatální záležitost nejsou vrácení přátelé
        if (prateleOp==null) return;

        String prateleUsersId=prateleOp.getUsersId();
        if (prateleUsersId==null) return;
        action += def.EX_PRITELID + prateleUsersId;
        action += def.EX_MSG_TEXT + getString(R.string.SOS);

        AsyncService asSOS
                = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
            }
        }, context, def.CMD_POSLI_SOS, action);
        asSOS.execute();
    }
}
