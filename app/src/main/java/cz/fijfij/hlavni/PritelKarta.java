/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;


import org.json.JSONObject;

import cz.fijfij.R;
import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.fragments.PrehledOsobFragment;
import cz.fijfij.hlavni.service.FijService;

/**
 * Created by jiri.kursky on 07.11.2016.
 * Toto je setting přítele
 * #setting  #pratele
 * vyvolává se dlouhým klikem na jméně. Svázáno s AdapterPritelKarta
 */

public class PritelKarta extends ParametryNastaveni {
    private static final String TAG = "PK";
    private ServiceOp serviceOp;
    private static final int OTEVRI_SEZNAM = 10;
    private static int position;
    private boolean zmenaParametru = false;


    public PritelKarta() {
        super(R.layout.activity_pritel_karta, ParametrOp.PARS_CLEN_VIDITELNE);
    }

    ServiceOp.ServiceInt serviceInt = new ServiceOp.ServiceInt() {

        @Override
        public void onServiceConnected() {
            serviceOp.registerClient();
            zrusCekej();
        }

        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public boolean receivedMessage(Message msg) {
            if (msg.what== FijService.MSG_REGISTER_CLIENT && zmenaParametru) {
                serviceOp.sendMessageToService(FijService.MSG_ZMENA_PARAMETRU_PRITEL, osoba.getBundle());
                zmenaParametru = false;
            }
            return false;
        }
    };

    @Override
    void nastavenPin() {
        prateleOp.setParametres(osoba);
        serviceOp.sendMessageToService(FijService.MSG_ZMENA_PARAMETRU_PRITEL, osoba.getBundle());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        position = intent.getIntExtra("position", -1);
        super.onCreate(savedInstanceState);
        nastavCekej();
    }


    @Override
    protected void onResume() {
        super.onResume();
        zrusCekej();
        osoba = prateleOp.vratPritele(position);
        if (osoba == null) return;
        serviceOp = new ServiceOp(this, serviceInt);
        ListView listView = (ListView) findViewById(R.id.listParametruNova);
        AdapterPritelKarta adapterPritelKarta = new AdapterPritelKarta(this, serviceOp, osoba, listView);
        adapterPritelKarta.notifyDataSetChanged();
        listView.setAdapter(adapterPritelKarta);
        listView.setOnItemClickListener(onItemClickListener);
        serviceOp.doBindService();
    }

    @Override
    protected void onPause() {
        serviceOp.sendMessageToService(FijService.MSG_KONEC_POTVRZENI_SMS, 0, null);
        serviceOp.unRegisterClient();
        serviceOp.doUnbindService();
        super.onPause();
    }

    private PrateleOp.CallBackListener prateleCallBack = new PrateleOp.CallBackListener() {
        @Override
        public void onFinished() {
            serviceOp.sendMessageToService(FijService.MSG_AKTUALIZACE_PRATEL);
            finish();
        }
    };

    private final AsyncService.TaskListener asyncVymazani = new AsyncService.TaskListener() {
        @Override
        public void onFinished(String nameAction, JSONObject result) {
            BufferOp bufferOp = new BufferOp(getBaseContext());
            PrateleOp prateleOp = new PrateleOp(bufferOp, prateleCallBack);
            prateleOp.execute();
            PrehledOsobFragment.prateleNacteni = false;
        }
    };

    /**
     * Toto je zatím nefunkční. Špatně to ruší u člena rodiny
     */
    public void smazUzivatele(View view) {
        if (osoba == null) return;
        final String pritelId = osoba.getUserId();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.info);
        builder.setCancelable(true);
        builder.setMessage(R.string.skutecne_smazat);
        builder.setPositiveButton(R.string.dlgAno, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                _smazUzivatele();
            }

            private void _smazUzivatele() {
                BufferOp bufferOp = new BufferOp(PritelKarta.this);
                bufferOp.openDatabase(BufferOp.DB_WRITABLE);
                try {
                    bufferOp.mDatabase.execSQL("delete from " + BufferOp.TBL_PRATELE + " where userid='" + pritelId + "'");
                } catch (Exception e) {
                    MyLog.e(TAG, e);
                }
                bufferOp.closeDatabase();
                AsyncService as = new AsyncService(asyncVymazani, getBaseContext(), def.CMD_VYMAZ_PRITELE + def.EX_PRITELID + pritelId);
                as.execute();
            }
        });
        builder.setNegativeButton(R.string.dlgNe, null);
        builder.show();
    }

    @Override
    public void nastavActionBar() {
        if (actionBar == null) return;
        if (osoba != null) {
            actionBar.setSubtitle(osoba.getNick());
        }
    }

    @Override
    void novyNick(String nick) {
        super.novyNick(nick);
        serviceOp.sendMessageToService(FijService.MSG_ZMENA_PARAMETRU_PRITEL, osoba.getBundle());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String phoneNumber = data.getStringExtra(def.PREF_PHONE_NUMBER);
                osoba.setPhoneNumber(phoneNumber);
                prateleOp.setParametres(osoba);
                serviceOp.doBindService();
                zmenaParametru = true;
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public void cekaniNastaveno(int zdrojVolani) {
        if (zdrojVolani == OTEVRI_SEZNAM) {
            serviceOp.doUnbindService();
            Intent intent = new Intent(this, ActivitySeznam.class);
            intent.putExtra(def.PREF_PHONE_NUMBER, osoba.getPhoneNumber());
            startActivityForResult(intent, 1);
        }
    }

    @Override
    void vyberTelCisla() {
        nastavCekej(OTEVRI_SEZNAM);
    }
}
