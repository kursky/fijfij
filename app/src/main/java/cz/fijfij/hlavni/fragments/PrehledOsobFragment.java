package cz.fijfij.hlavni.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import cz.fijfij.R;
import cz.fijfij.hlavni.ActivityOsoba;
import cz.fijfij.hlavni.ActivityParametryNoveho;
import cz.fijfij.hlavni.AdapterPratele;
import cz.fijfij.hlavni.AsyncService;
import cz.fijfij.hlavni.BufferOp;
import cz.fijfij.hlavni.about.ActivityAbout;
import cz.fijfij.hlavni.base.Err;
import cz.fijfij.hlavni.service.AlarmReceiver;
import cz.fijfij.hlavni.service.FijService;
import cz.fijfij.hlavni.zapouzdreno.MapsActivity;
import cz.fijfij.hlavni.Osoba;
import cz.fijfij.hlavni.ParametrOp;
import cz.fijfij.hlavni.PinEntryView;
import cz.fijfij.hlavni.PrateleOp;
import cz.fijfij.hlavni.PritelKarta;
import cz.fijfij.hlavni.ServiceOp;
import cz.fijfij.hlavni.SettingOp;
import cz.fijfij.hlavni.nastaveni.SettingsActivity;
import cz.fijfij.hlavni.SledovaniActivit;
import cz.fijfij.hlavni.TestovaciStranka;
import cz.fijfij.hlavni.VytvorSpojeni;
import cz.fijfij.hlavni.def;

public class PrehledOsobFragment extends Fragment
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    private SettingOp settingOp;
    private ServiceOp serviceOp;
    private PrateleOp prateleOp;
    public static boolean prateleNacteni = false;
    private DrawerLayout drawerLayout;
    NavigationView navigationView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView txtNadpis;
    private Osoba vybranyPritel;
    private BufferOp bufferOp;
    private View llCekej;
    private View llAktivni;
    private AppCompatDelegate mDelegate;

    /**
     * Návratová funkce po načtení přátel
     */
    private PrateleOp.CallBackListener prateleCallBack = new PrateleOp.CallBackListener() {
        @Override
        public void onFinished() {
            prateleNacteni = true;
            swipeRefreshLayout.setRefreshing(false);
            zobrazPratele();
            posledniPrikazNaZacatku();
        }
    };


    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            prateleOp.executeVse();
        }
    };


    private void posledniPrikazNaZacatku() {
        def.prateleOp=prateleOp;
        if (def.jeRidici() || !def.debug) {
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_testovaci_stranka).setVisible(false);
        }

        if (def.maPIN() && !def.getOdemcenyPIN(getContext()) && !def.jeRidici() && !settingOp.getBoolean(def.PREF_PRVNI_SPUSTENI, true)) {
            SledovaniActivit.startActivity(getActivity(), 10, PinEntryView.class);
        }
    }

    private ServiceOp.ServiceInt serviceInt = new ServiceOp.ServiceInt() {
        @Override
        public void onServiceConnected() {
            serviceOp.registerClient();
            serviceOp.sendMessageToService(FijService.MSG_START_MAIN);
        }

        @Override
        public void onServiceDisconnected() {
            serviceOp.unRegisterClient();
        }

        @Override
        public boolean receivedMessage(Message msg) {
            switch (msg.what) {
                case FijService.MSG_STOP_SERVICE:
                    // Zastavuje se při zrušení registrace
                    serviceOp.stopService();
                    FijService.cancelNotification(getContext());
                    settingOp.putBoolean(def.PREF_PRVNI_SPUSTENI, true);
                    settingOp.putBoolean(def.PREF_REGISTRACE_ZRUSENA, true);
                    bufferOp.resetTabulek();
                    def.konec();
                    break;
                case FijService.MSG_PRITEL_PRIDAN:
                    Toast.makeText(getActivity(), R.string.pridan_novy_pritel, Toast.LENGTH_LONG).show();
                    swipeRefreshLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            swipeRefreshLayout.setRefreshing(true);
                            onRefreshListener.onRefresh();
                        }
                    });
                    break;
                case FijService.MSG_MAPA_PRIPRAVENA:
                    if (prateleOp == null || prateleOp.getSize() == 0) {
                        serviceOp.sendMessageToService(FijService.MSG_UDAJE_MAPY, -1);
                    } else {
                        if (vybranyPritel == null) vybranyPritel = prateleOp.vratPritele(0);
                        if (vybranyPritel!=null) {
                            serviceOp.sendMessageToService(FijService.MSG_UDAJE_MAPY, 0, vybranyPritel.getBundle());
                        }
                        else {
                            serviceOp.sendMessageToService(FijService.MSG_UDAJE_MAPY, 0);
                        }
                    }
                    break;
            }
            return false;
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (!def.isXLargeTablet(getActivity())) {
            getDelegate().installViewFactory();
            getDelegate().onCreate(savedInstanceState);
        }
        super.onCreate(savedInstanceState);
        FragmentActivity activity = getActivity();
        def.factory(activity);

        prateleNacteni = false;

        // Pozor zde se nesmí inicializovat, protože je volán vícekrát
        // def.setOdemcenyPIN(getContext(),def.jeRidici());
        bufferOp = new BufferOp(activity);
        if (settingOp == null) {
            settingOp = new SettingOp(activity);
            Osoba osoba = settingOp.vratParametryUzivatel(ParametrOp.PARS_ADMIN);
            def.nastavUzivatele(osoba);
            prateleOp = new PrateleOp(bufferOp, prateleCallBack);
        }
        obnovSluzbu();
        new ParametrOp();
    }

    /**
     * Jen bind na dané službě
     */
    private void obnovSluzbu() {
        if (serviceOp == null) {
            serviceOp = new ServiceOp(getActivity(), serviceInt);
            serviceOp.doBindService();
        }
    }


    private AppCompatDelegate getDelegate() {
        if (mDelegate == null) {
            mDelegate = AppCompatDelegate.create(getActivity(), null);
        }
        return mDelegate;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.prehled_osob,
                container, false);
        txtNadpis = (TextView) view.findViewById(R.id.txtNadpis);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

        drawerLayout = (DrawerLayout) view.findViewById(R.id.drawer_layout);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar_bar_main);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();


        navigationView = (NavigationView) view.findViewById(R.id.nav_view);
        llCekej = view.findViewById(R.id.ll_cekej);
        llCekej.setVisibility(View.VISIBLE);
        llAktivni = view.findViewById(R.id.ll_aktivni);
        llAktivni.setVisibility(View.GONE);
        navigationView.setNavigationItemSelectedListener(this);
        TextView txtNovaVerze = (TextView) view.findViewById(R.id.txtNovaVerze);
        if (txtNovaVerze != null) {
            txtNovaVerze.setVisibility(thereIsNewVersion() ? View.VISIBLE : View.GONE);
        }

        Button btnPridatPritele = (Button) view.findViewById(R.id.btnPridatPratele);
        btnPridatPritele.setOnClickListener(this);
        Button btnLock = (Button) view.findViewById(R.id.btnZamknout);
        btnLock.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPridatPratele:
                aktivitaNovyPritel();
                break;
            case R.id.btnZamknout:
                def.setOdemcenyPIN(getContext(), false);
                SledovaniActivit.startActivity(getActivity(), 10, PinEntryView.class);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        obnovSluzbu();
        if (def.jeRidici() || !prateleNacteni) prateleOp.executeVse();
        if (settingOp.getBoolean(def.PREF_PRVNI_NASTAVENI, false) && !def.jeRidici()) {
            settingOp.putBoolean(def.PREF_PRVNI_NASTAVENI, false);
            settingOp.putBoolean(def.PREF_NEZOBRAZOVAT_BEH, true);
            FijService.cancelNotification(getActivity());
        }
        if (prateleNacteni && !def.getOdemcenyPIN(getContext()) && !def.jeRidici() && def.maPIN()) {
            SledovaniActivit.startActivity(getActivity(), 10, PinEntryView.class);
        }
    }


    /**
     * Zde se nastavují i události pokud se klikne na daného přítele
     */
    private void nastavSeznam() {
        if (prateleOp.getSize() == 0) return;
        final FragmentActivity activity = getActivity();
        AdapterPratele adapterPratele = new AdapterPratele(activity, prateleOp);
        final ListView listView = (ListView) activity.findViewById(R.id.listPrateleHlavni);
        listView.setAdapter(adapterPratele);
        if ((prateleOp.getSize() > 0) && def.jeRidici()) {
            listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            listView.setItemChecked(0, true);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    for (int i = 0; i < listView.getChildCount(); i++) {
                        if (position == i) {
                            if (listView.getChildCount() > 1 && def.isXLargeTablet(getContext())) listView.getChildAt(i).setBackgroundColor(0xFFAFB9FF);
                        } else {
                            listView.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                        }
                    }
                    vybranyPritel = prateleOp.vratPritele(position);
                    if (vybranyPritel == null) return;
                    if (vybranyPritel.spojeniNepotvrzeno()) {
                        if (!vybranyPritel.spojeniSmsNepotvrzeno()) {
                            def.dlgAlert(activity, def.getText(getActivity(), R.string.zadat_kod, vybranyPritel.getString(def.PREF_SPOJOVACI_KLIC)));
                        }
                    } else {
                        if (def.isXLargeTablet(activity)) {
                            serviceOp.sendMessageToService(FijService.MSG_UDAJE_MAPY, 0, vybranyPritel.getBundle());
                        } else {
                            Intent intent = new Intent(activity, MapsActivity.class);
                            intent.putExtra(def.C_OSOBA, vybranyPritel.getBundle());
                            startActivity(intent);
                        }
                    }
                }

            });
        }
        registerForContextMenu(listView);
    }


    /**
     * Definice option menu
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
    }


    /**
     * Vyber na základě options (tři tečky nahoře vpravo)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // dle výběru se otevírá
        if (id == R.id.action_settings) {
            SledovaniActivit.startActivity(getActivity(), 0, SettingsActivity.class, Intent.FLAG_ACTIVITY_NO_HISTORY, SledovaniActivit.START_SERVICE);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Akce na základě menu
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        FragmentActivity activity = getActivity();

        if (id == R.id.nav_action_novy_pritel) {
            drawerLayout.closeDrawers();
            aktivitaNovyPritel();
            return true;
        } else if (id == R.id.nav_o_programu) {
            drawerLayout.closeDrawers(); // schováni menu
            //startActivity(new Intent(activity, OProgramu.class));
            startActivity(new Intent(activity, ActivityAbout.class));
            return true;
        } else if (id == R.id.nav_zrusit_registraci) {
            drawerLayout.closeDrawers(); // schováni menu
            zrusitRegistraci();
            return true;
        } else if (id == R.id.nav_nastaveni) {
            drawerLayout.closeDrawers(); // schováni menu
            startActivity(new Intent(activity, SettingsActivity.class));
            return true;
        } else if (id == R.id.nav_testovaci_stranka) {
            drawerLayout.closeDrawers(); // schováni menu
            startActivity(new Intent(activity, TestovaciStranka.class));
            return true;
            /*
        } else if (id == R.id.nav_test) {
            drawerLayout.closeDrawers(); // schovani menu
            String action=def.EX_ACTION+def.CMD_TEST;
            action+=def.EX_NICK+ Uri.encode("Štěpán");
            AsyncService as=new AsyncService(new AsyncService.TaskListener() {
                @Override
                public void onFinished(String nameAction, JSONObject result) {
                    if (result==null) return;
                    Bundle b=def.transferJSON(result);
                    String test=Uri.decode(b.getString("name")) ;
                }
            }, this, "Test", action, null);
            as.execute();
            return true;
            */

       /* } else if (id == R.id.nav_zastavit_sluzbu) {
            drawerLayout.closeDrawers(); // schovani menu
            //serviceOp.sendMessageToService(FijService.MSG_STOP_SERVICE);
            FijService.cancelNotification(this);
            return true;
            */
        } else if (id == R.id.nav_konec) {
            drawerLayout.closeDrawers(); // schování menu
            def.konec();
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Klik na tlačítko "Přidat přátele"
     */

    public void pridatPrateleClick(View view) {
        Intent intent = new Intent(getActivity(), ActivityParametryNoveho.class);
        startActivity(intent);
    }

    private void zobrazPratele() {
        FragmentActivity activity = getActivity();
        View seznamPratel = activity.findViewById(R.id.seznamPratel);
        View prazdnySeznam = activity.findViewById(R.id.prazdnySeznam);

        if (prateleOp.getSize() > 0) {
            seznamPratel.setVisibility(View.VISIBLE);
            prazdnySeznam.setVisibility(View.GONE);
        } else {
            seznamPratel.setVisibility(View.GONE);
            prazdnySeznam.setVisibility(View.VISIBLE);
        }

        if (llCekej != null) llCekej.setVisibility(View.GONE);
        if (llAktivni != null) llAktivni.setVisibility(View.VISIBLE);

        Button btnZamkni = (Button) activity.findViewById(R.id.btnZamknout);
        if (def.jeRidici()) {
            txtNadpis.setText(R.string.seznam_pratel);
            btnZamkni.setVisibility(View.INVISIBLE); // Zde se nevolí gone - jeho velikost určuje seznam pod ním
        } else {
            txtNadpis.setText(R.string.par_ridici);
            btnZamkni.setVisibility(def.maPIN() ? View.VISIBLE : View.INVISIBLE);
        }
        nastavSeznam();
    }

    @Override
    public void onDestroy() {
        zastavSluzbu();
        bufferOp.closeDatabase();
        super.onDestroy();
    }

    /**
     * Dlouhý stisk na jméně
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (v.getId() == R.id.listPrateleHlavni && def.jeRidici()) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            int position = info.position;
            Osoba osoba = prateleOp.vratPritele(position);
            MenuInflater inflater = getActivity().getMenuInflater();

            // menu při dlouhém stisku
            inflater.inflate(R.menu.udalost_menu_list_pritel, menu);

            MenuItem menuItem;

            // Kontrola jestli se jedná o spojeného přítele
            if (TextUtils.isEmpty(osoba.getString(def.PREF_SPOJOVACI_KLIC))) {
                menuItem = menu.findItem(R.id.nastaveni);
                menuItem.setVisible(true);
                menuItem = menu.findItem(R.id.details);
                menuItem.setVisible(true);


                // Funkce zrušit nám nějak nefunguje
                //menuItem = menu.findItem(R.id.zrusit);
                //menuItem.setVisible(false);
            } else {
                menuItem = menu.findItem(R.id.nastaveni);
                menuItem.setVisible(true);
                menuItem = menu.findItem(R.id.details);
                menuItem.setVisible(false); // nemůže mít detaily
                // Funkce zrušit nám nějak nefunguje
                //menuItem = menu.findItem(R.id.zrusit);
                //menuItem.setVisible(false);
            }
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        FragmentActivity activity = getActivity();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Intent intent;
        final Osoba osoba = prateleOp.vratPritele(info.position);

        switch (item.getItemId()) {
            case R.id.nastaveni:
                intent = new Intent(activity, PritelKarta.class);
                intent.putExtra("position", info.position);
                startActivity(intent);
                break;
            case R.id.details:
                intent = new Intent(activity, ActivityOsoba.class);
                intent.putExtra("position", info.position);
                startActivity(intent);
                break;
            case R.id.shortWarning:
                final EditText editText = new EditText(getContext());
                editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(150)});
                editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                editText.setText("");

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(R.string.message);
                builder.setCancelable(true);
                builder.setView(editText);
                builder.setNegativeButton(R.string.zpet, null);
                builder.setPositiveButton(R.string.dlgOK, null);
                final AlertDialog dialog = builder.create();
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialog.show();
                Button theButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                theButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String mValue = editText.getText().toString();
                        if (TextUtils.isEmpty(mValue)) {
                            Toast.makeText(getContext(), R.string.hodnota_musi_byt_zadana, Toast.LENGTH_LONG).show();
                            return;
                        }
                        zasliWarning(osoba, mValue);
                        dialog.dismiss();
                    }
                });
                /*
            case R.id.zobraz_mapu_online:
                ActivityMapOnLine.factory(osoba);
                SledovaniActivit.startActivity(activity, 0, ActivityMapOnLine.class);
                */

            /*
            case R.id.zrusit:
                Osoba osoba = prateleOp.vratPritele(info.position);
                String actionZruseni = def.EX_ACTION + def.CMD_SPOJOVACI_KLIC_ZRUSENI;
                actionZruseni += def.EX_SPOJOVACI_KLIC + osoba.getString(def.PREF_SPOJOVACI_KLIC);
                actionZruseni += def.EX_PRITELID + osoba.getUserId();
                AsyncService asZruseni = new AsyncService(new AsyncService.TaskListener() {
                    @Override
                    public void onFinished(String nameAction, JSONObject result) {
                        prateleOp.executeVse();
                    }
                }, this, "zruseni", actionZruseni);
                asZruseni.execute();
                */
        }
        return super.onContextItemSelected(item);
    }


    private void zasliWarning(Osoba osoba, String text) {
        String action = def.startCmd(def.CMD_POSLI_NOTIFIKACI);
        action+=def.EX_USERID+def.userId();
        action+=def.EX_PRITELID+osoba.getUserId();
        action+=def.EX_MSG_TEXT+text;

        AsyncService asWarning = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
                def.dlgAlert(getContext(), R.string.zprava_odeslana);
            }
        }
                , getContext(), def.CMD_POSLI_NOTIFIKACI, action);
        asWarning.setupErrorListener(new AsyncService.ErrorListener() {
            @Override
            public void onError(int errCode) {
                String s= Err.getErrString(getContext(), errCode);
                def.dlgAlert(getContext(), s);
            }
        });
        asWarning.execute();
    }


    /**
     * Odlogování uživatele
     */
    public void zrusitRegistraci() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.info);
        builder.setCancelable(true);
        if (def.isNetworkAvailable(getActivity())) {
            builder.setMessage(R.string.zrusit_registraci);
            builder.setPositiveButton(R.string.dlgAno, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    _executeZrusitRegistraci();
                }
            });
            builder.setNegativeButton(R.string.dlgNe, null);
        } else {
            builder.setMessage(R.string.pouze_sit_opet);
            builder.setPositiveButton(R.string.dlgOK, null);
        }
        builder.show();
    }


    /**
     * Přímý výkon vymazání registrace
     */
    private void _executeZrusitRegistraci() {
        final FragmentActivity activity = getActivity();
        AlarmReceiver alarm = new AlarmReceiver();
        alarm.cancelAlarm(getContext());

        AsyncService as = new AsyncService(
                new AsyncService.TaskListener() {
                    @Override
                    public void onFinished(String nameAction, JSONObject result) {
                        if (result != null) {
                            serviceOp.sendMessageToService(FijService.MSG_STOP_SERVICE);
                            def.konec();
                        } else
                            def.dlgAlert(activity, R.string.server_neodpovedel);
                    }
                }, getActivity(), def.CMD_ZRUSIT_REGISTRACI);
        as.execute();
    }

    @Override
    public void onPause() {
        zastavSluzbu();
        super.onPause();
        bufferOp.closeDatabase();
    }

    @Override
    public void onStop() {
        zastavSluzbu();
        bufferOp.closeDatabase();
        super.onStop();
    }


    private boolean thereIsNewVersion() {
        /*
        String newVersion = Jsoup.connect(
                        "https://play.google.com/store/apps/details?id="
                                + "Package" + "&hl=en")
                .timeout(30000)
                .userAgent(
                        "Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                .referrer("http://www.google.com").get()
                .select("div[itemprop=softwareVersion]").first()
                .ownText();
        return result != ConnectionResult.SUCCESS;
        */
        return false;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void zastavSluzbu() {
        if (serviceOp != null) {
            serviceOp.unRegisterClient();
            serviceOp.doUnbindService();
            serviceOp = null;
        }
    }

    private void aktivitaNovyPritel() {
        if (def.jeRidici()) {
            if (prateleOp.getSize() < 5) {
                Intent intent = new Intent(getActivity(), ActivityParametryNoveho.class);
                startActivity(intent);
            } else {
                def.dlgAlert(getActivity(), R.string.err_max_pocet);
            }
        } else {
            Intent intent = new Intent(getActivity(), VytvorSpojeni.class);
            startActivity(intent);
        }
    }
}