package cz.fijfij.hlavni.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.view.ContextThemeWrapper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import cz.fijfij.R;
import cz.fijfij.hlavni.ActivityOsoba;
import cz.fijfij.hlavni.AsyncService;
import cz.fijfij.hlavni.BufferOp;
import cz.fijfij.hlavni.CoordinatesItem;
import cz.fijfij.hlavni.CoordinatesOp;
import cz.fijfij.hlavni.service.FijService;
import cz.fijfij.hlavni.lokace.LatLngItem;
import cz.fijfij.hlavni.MainActivity;
import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.lokace.NastaveniPolohy;
import cz.fijfij.hlavni.Osoba;
import cz.fijfij.hlavni.PrateleOp;
import cz.fijfij.hlavni.ServiceOp;
import cz.fijfij.hlavni.SettingOp;
import cz.fijfij.hlavni.zapouzdreno.SmsControl;
import cz.fijfij.hlavni.def;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by jiri.kursky on 10.3.2017
 * Operace na mapě
 * 16.3.2017 - předěláno na fragment
 */

public class MapsFragment extends Fragment implements OnMapReadyCallback {
    public CoordinatesOp coordinatesOp;
    private GoogleMap mMap = null;
    private MapView mapView = null;
    public static Integer aktPosition;
    private Osoba osoba;
    private int maxPozice;
    private boolean mapIsVisible = false;
    private SmsControl smsControl;
    private ServiceOp serviceOp;
    private String markerTitle;
    private BufferOp bufferOp;
    private String defaultMarkerTitle;
    private LatLngItem latLngItem;
    private boolean cekaniZobrazeno;
    private SettingOp settingOp;
    private Intent intent;
    private AppCompatDelegate mDelegate;
    final static String ARG_POSITION = "position";
    private OnMapsFragmentCallback mCallBack;
    static final String TAG = MapsFragment.class.getSimpleName();
    private View llAktivni;
    private Activity activity;

    public interface OnMapsFragmentCallback {
        void onFragmentMapReady();
    }

    public void setOnMapsFragmentCallback(OnMapsFragmentCallback onMapsFragmentCallback) {
        mCallBack = onMapsFragmentCallback;
    }

    private void registerClient() {
        if (serviceOp != null) serviceOp.registerClient();
    }


    // Používá se pri doručení sms
    private ServiceOp.ServiceInt serviceIntStart = new ServiceOp.ServiceInt() {
        @Override
        public void onServiceConnected() {
            registerClient();
            serviceOp.sendMessageToService(FijService.MSG_MAPA_PRIPRAVENA);
        }

        @Override
        public void onServiceDisconnected() {
            serviceOp.unRegisterClient();
        }

        @Override
        public boolean receivedMessage(Message msg) {
            switch (msg.what) {
                case FijService.MSG_MAPA_PRIPRAVENA:
                    if (mCallBack != null) mCallBack.onFragmentMapReady();
                    break;
                case FijService.MSG_NOVA_SMS:
                    coordinatesOp.aktualizuj(true);
                    aktPosition = 0;
                    maxPozice = coordinatesOp.getSizeZpracovane() - 1;
                    SeekBar seekBar = getSeekBar();
                    seekBar.setMax(maxPozice);
                    seekBar.setProgress(maxPozice - aktPosition);
                    zapisPolohu();
                    zobrazCekaniNaSMS();
                    break;
                case FijService.MSG_UDAJE_MAPY:
                    aktPosition = msg.arg1;
                    if (msg.arg1 < 0) {
                        llAktivni.setVisibility(View.GONE);
                    } else {
                        llAktivni.setVisibility(View.VISIBLE);
                        Bundle bundle = msg.getData();
                        osoba = new Osoba(bundle);
                        defaultMarkerTitle = osoba.getNick();
                        coordinatesOp = new CoordinatesOp(osoba.getUserId(), bufferOp);
                        String action = settingOp.getActionString(def.CMD_NAVRAT_VSECH_SOURADNIC) + def.EX_PRITELID + osoba.getUserId();
                        AsyncService as = new AsyncService(souradniceVraceny, getContext(), def.CMD_NAVRAT_VSECH_SOURADNIC, action);
                        as.setupWatchDog(new AsyncService.WatchDogListener() {
                            @Override
                            public void onError() {
                                coordinatesOp.nactiUdaje(osoba);
                                nastavPolohu();
                            }
                        });
                        as.execute();
                    }
                    break;
            }
            return false;
        }
    };


    /**
     * Načítají se k přátelům poslední známé souřadnice
     */
    private final AsyncService.TaskListener souradniceVraceny = new AsyncService.TaskListener() {
        @Override
        public void onFinished(String nameAction, JSONObject result) {
            boolean error = result == null;
            if (!error) {
                coordinatesOp.clear();
                try {
                    JSONArray c = result.getJSONArray("DATA");
                    for (int i = 0; i < c.length(); i++) {
                        JSONObject udaj = c.getJSONObject(i);
                        Bundle b = def.transferJSON(udaj);
                        if (b != null) {
                            b.putString("pritelId", osoba.getKeyValue(def.PREF_USER_ID));
                            coordinatesOp.add(b, i == 0);
                        }
                    }
                } catch (Exception e) {
                    error = true;
                }
            }
            if (error) {
                coordinatesOp.nactiUdaje(osoba);
            } else {
                coordinatesOp.aktualizuj(true);
            }
            nastavPolohu();
        }
    };

    private final SeekBar.OnSeekBarChangeListener seekBarListener = (new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            zapisPolohu();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                aktPosition = maxPozice - progress;
            }
            if (mMap == null) return;
            Integer pozice = aktPosition + 1;
            if (fromUser) {
                TextView txtPozice = (TextView) activity.findViewById(R.id.txtPozice);
                txtPozice.setText(pozice.toString());
            }
            CoordinatesItem ci = getCoordinatesOp(aktPosition);
            if (ci == null) return;
            TextView txtCas = (TextView) activity.findViewById(R.id.txtCas);
            txtCas.setText(def.dateMySQLczech(ci.getCas()));
        }
    });

    private SeekBar getSeekBar() {
        SeekBar seekBar = (SeekBar) activity.findViewById(R.id.seekBar);
        return seekBar;
    }

    private CoordinatesItem getCoordinatesOp(int index) {
        return coordinatesOp.getCoordinatesItem(index);
    }

    int mCurrentPosition = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        activity = getActivity();
        // If activity recreated (such as from screen rotate), restore
        // the previous article selection set by onSaveInstanceState().
        // This is primarily necessary when in the two-pane layout.
        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
        }

        View mainView;
        if (def.isXLargeTablet(activity)) {
            // create ContextThemeWrapper from the original Activity Context with the custom theme
            final ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(activity, R.style.AppTheme);

            // clone the inflater using the ContextThemeWrapper
            LayoutInflater localInflanter = inflater.cloneInContext(contextThemeWrapper);
            mainView = localInflanter.inflate(R.layout.activity_maps_large, container, false);
        } else {
            // Inflate the layout for this fragment
            mainView = inflater.inflate(R.layout.activity_maps, container, false);
        }

        llAktivni = mainView.findViewById(R.id.ll_aktivni);
        llAktivni.setVisibility(View.GONE);

        SeekBar seekBar = (SeekBar) mainView.findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(seekBarListener);
        seekBar.setMax(maxPozice);
        seekBar.setVisibility(View.INVISIBLE);

        TextView txtPozice = (TextView) mainView.findViewById(R.id.txtPozice);
        txtPozice.setText("");
        TextView txtCekani = (TextView) mainView.findViewById(R.id.txtSmsOdeslano);

        TextView txtTypZjisteni = (TextView) mainView.findViewById(R.id.txtTypZjisteni);
        txtTypZjisteni.setText("");

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        mapView = (MapView) mainView.findViewById(R.id.map);
        mapView.setVisibility(View.INVISIBLE);
        mapView.onCreate(savedInstanceState);

        // Set the map ready callback to receive the GoogleMap object
        mapView.getMapAsync(this);

        try {
            MapsInitializer.initialize(activity);
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }


        if (!def.isXLargeTablet(activity)) {
            if (osoba != null) {
                ActionBar actionBar = getDelegate().getSupportActionBar();
                actionBar.setSubtitle(osoba.getNick());
            }
        }
        return mainView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        activity = getActivity();
        if (!def.isXLargeTablet(activity)) {
            getDelegate().installViewFactory();
            getDelegate().onCreate(savedInstanceState);
        }
        super.onCreate(savedInstanceState);
        Intent intent = activity.getIntent();
        if (!def.isXLargeTablet(activity)) osoba = new Osoba(intent.getBundleExtra(def.C_OSOBA));
        bufferOp = new BufferOp(activity);
        settingOp = new SettingOp(activity);
        setHasOptionsMenu(true);
        mMap = null;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);

        mapIsVisible = true;
        smsControl = new SmsControl(new PrateleOp(new BufferOp(activity)));
        obnovSluzbu();
        zobrazCekaniNaSMS();
        mapView.setVisibility(View.VISIBLE);
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(final Marker marker) {
                intent = new Intent(activity, NastaveniPolohy.class);
                LatLng latLng = marker.getPosition();
                intent.putExtra(def.PREF_LONGITUDE, latLng.longitude);
                intent.putExtra(def.PREF_LATITUDE, latLng.latitude);
                intent.putExtra(def.PREF_USER_ID, osoba.getUserId());
                startActivityForResult(intent, 1);
            }
        });

        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                if (mapIsVisible && latLngItem != null) latLngItem.setZoom(mMap.getCameraPosition().zoom, settingOp);
            }
        });

    }


    private void zastavSluzbu() {
        if (serviceOp != null) {
            serviceOp.unRegisterClient();
            serviceOp.doUnbindService();
            serviceOp = null;
        }
    }

    private void obnovSluzbu() {
        if (serviceOp == null) {
            serviceOp = new ServiceOp(activity, serviceIntStart);
        }
        serviceOp.doBindService();
    }


    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
        mapIsVisible = false;

    }

    private void zobrazCekaniNaSMS() {
        if (coordinatesOp == null) {
            zobrazCekaniNaSMS(false);
        } else {
            zobrazCekaniNaSMS(coordinatesOp.getArNepotvrzene().size() > 0);
        }
    }

    private void zobrazCekaniNaSMS(boolean ano) {
        cekaniZobrazeno = ano;
        View txtCekani = activity.findViewById(R.id.txtSmsOdeslano);
        txtCekani.setVisibility(ano ? View.VISIBLE : View.GONE);
    }

    private LatLng definujLatLngItem(LatLng latLng) {
        latLngItem = new LatLngItem(latLng, bufferOp, settingOp);
        markerTitle = latLngItem.vratNazevBodu();
        if (TextUtils.isEmpty(markerTitle)) markerTitle = defaultMarkerTitle;
        return latLng;
    }

    /**
     * Moc tomu nerozumím, ale musí to běžet na stejném vlákně
     */
    private void zapisPolohu() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                doZapisPolohu();
            }
        });
    }


    /**
     * Vlastní zápis polohy
     */
    private void doZapisPolohu() {
        if (mMap == null) return;
        if (maxPozice == -1) {
            maSouradnice(0);
            return;
        }
        if (aktPosition <= maxPozice) {
            CoordinatesItem ci = getCoordinatesOp(aktPosition);
            if (ci == null) return;
            Context context = getContext();
            try {
                setText(R.id.txtCas, def.verbalDateTime(context, ci.getCas()));
                setText(R.id.txtCasPrepsani, def.verbalDateTime(context, ci.getCasPrepsani()));
                Integer pozice = aktPosition + 1;
                setText(R.id.txtPozice, pozice.toString());
                setText(R.id.txtTypZjisteni, ci.sGetTypZjisteniLokace(context));
                if (maSouradnice(ci.getLatitude())) {
                    String cityName = null;
                    Geocoder gcd = new Geocoder(context, Locale.getDefault());
                    List<Address> addresses;
                    try {
                        addresses = gcd.getFromLocation(ci.getLatitude(),
                                ci.getLongitude(), 1);
                        if (addresses.size() > 0) {
                            Address address = addresses.get(0);
                            cityName = address.getLocality();
                            if (TextUtils.isEmpty(cityName)) cityName = address.getSubAdminArea();
                        }
                    } catch (IOException e) {
                        MyLog.e(TAG, e);
                    }

                    LatLng latLng = definujLatLngItem(new LatLng(ci.getLatitude(), ci.getLongitude()));
                    mMap.clear();
                    MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(markerTitle);
                    String txt = def.verbalDateTime(context, ci.getCasPrepsani());
                    if (!TextUtils.isEmpty(cityName)) txt += " " + cityName;
                    markerOptions.snippet(txt);
                    Bitmap photo = def.getPhoto(context, osoba);
                    if (photo != null) {
                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(photo));
                    }

                    mMap.addMarker(markerOptions).showInfoWindow();
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, latLngItem.zoom));
                }
            } catch (Exception e) {
                MyLog.e(TAG, e);
            }
        }
    }

    private boolean maSouradnice(double latitude) {
        SeekBar seekBar = getSeekBar();
        if (seekBar != null) {
            if (seekBar.getMax() > 1) {
                seekBar.setVisibility(View.VISIBLE);
            } else {
                seekBar.setVisibility(View.INVISIBLE);
            }
        }
        if (Math.round(latitude) == 0) {
            if (mapIsVisible) {
                TextView txt = (TextView) activity.findViewById(R.id.txtNejsouData);
                txt.setVisibility(View.VISIBLE);
                mapView.setVisibility(View.INVISIBLE);
                mapIsVisible = false;
            }
            return false;
        }
        if (!mapIsVisible) {
            TextView txt = (TextView) activity.findViewById(R.id.txtNejsouData);
            txt.setVisibility(View.GONE);
            mapView.setVisibility(View.VISIBLE);
            mapIsVisible = true;
        }
        return true;
    }


    /**
     * Definice option menu - tři tečky, možnost odeslání sms
     */

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.map, menu);
        MenuItem menuItem = menu.findItem(R.id.action_sms_poloha);
        menuItem.setVisible(osoba.getKeyValueBool(def.PREF_SMS_POVOLENO));
    }


    /**
     * Odešle sms a spustí čekací spinner
     */
    private void odesliSMS() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.info);
        builder.setCancelable(true);

        if (TextUtils.isEmpty(osoba.getPhoneNumber())) {
            builder.setMessage(R.string.neni_nastaveno_cislo);
            builder.setNegativeButton(R.string.dlgOK, null);
        } else {
            builder.setMessage(R.string.skutecne_odeslat_sms);
            builder.setPositiveButton(R.string.dlgAno, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {
                    if (cekaniZobrazeno) {
                        AlertDialog.Builder cBuilder = new AlertDialog.Builder(activity);
                        cBuilder.setTitle(R.string.info);
                        cBuilder.setMessage(R.string.repeat_sms);
                        cBuilder.setCancelable(true);
                        cBuilder.setPositiveButton(R.string.dlgAno, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                smsControl.askCoor(osoba);
                            }
                        });
                        cBuilder.setNegativeButton(R.string.dlgNe, null);
                        cBuilder.show();
                    } else {
                        smsControl.askCoor(osoba);
                        zobrazCekaniNaSMS(true);
                    }
                }
            });
            builder.setNegativeButton(R.string.dlgNe, null);
        }
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                zapisPolohu();
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    /**
     * Vyber na základě options (tři tečky nahoře vpravo)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_sms_poloha:    // dle výběru se otevírá
                odesliSMS();
                return true;
            case R.id.action_podrobnosti:
                intent = new Intent(activity, ActivityOsoba.class);
                intent.putExtra(def.PREF_USER_ID, osoba.getUserId());
                startActivity(intent);
                return true;
            case android.R.id.home:
                intent = new Intent(activity, MainActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private AppCompatDelegate getDelegate() {
        if (mDelegate == null) {
            mDelegate = AppCompatDelegate.create(activity, null);
        }
        return mDelegate;
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        obnovSluzbu();
    }

    @Override
    public void onStop() {
        mapView.onStop();
        zastavSluzbu();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        zastavSluzbu();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }


    private void nastavPolohu() {
        maxPozice = coordinatesOp.getSizeZpracovane() - 1;
        aktPosition = 0;
        SeekBar seekBar = getSeekBar();
        seekBar.setMax(maxPozice);
        seekBar.setProgress(maxPozice - aktPosition);
        zapisPolohu();
    }

    private void setText(int ridTxt, final String value) {
        TextView txt = (TextView) activity.findViewById(ridTxt);
        txt.setText(value);
    }
}

