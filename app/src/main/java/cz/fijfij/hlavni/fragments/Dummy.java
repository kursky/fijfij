package cz.fijfij.hlavni.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cz.fijfij.R;

/**
 * Created by jiri.kursky on 17.03.2017.
 */

public class Dummy extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dummy,
                container, false);
        return view;

    }
}
