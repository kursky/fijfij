package cz.fijfij.hlavni.zapouzdreno;


import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;

import cz.fijfij.R;
import cz.fijfij.hlavni.service.FijService;
import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.Osoba;
import cz.fijfij.hlavni.ServiceOp;
import cz.fijfij.hlavni.def;
import cz.fijfij.hlavni.fragments.MapsFragment;
import cz.fijfij.hlavni.MainActivity;

/**
 * Created by jiri.kursky on 10.3.2017
 * Operace na mapě
 * 16.3.2017 - předěláno na fragment volá se fragment a odtud je pak ovládání přes službu
 * Tato verze je pouze pro zobrazení na mobilu, pro tablet se používá jiné
 */

public class MapsActivity extends FragmentActivity implements MapsFragment.OnMapsFragmentCallback {
    private ServiceOp serviceOp = null;
    private Osoba vybranyPritel;
    static final String TAG = MapsActivity.class.getSimpleName();

    private ServiceOp.ServiceInt serviceInt = new ServiceOp.ServiceInt() {
        @Override
        public void onServiceConnected() {
            serviceOp.registerClient();
            serviceOp.sendMessageToService(FijService.MSG_UDAJE_MAPY, 0, vybranyPritel.getBundle());
        }

        @Override
        public void onServiceDisconnected() {
            serviceOp.unRegisterClient();
        }

        @Override
        public boolean receivedMessage(Message msg) {
            return false;
        }
    };

    public void onFragmentMapReady() {
        serviceOp = new ServiceOp(this, serviceInt);
        serviceOp.doBindService();
    }

    @Override
    public void onPause() {
        super.onPause();
        serviceOp.doUnbindService();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (serviceOp != null) serviceOp.doBindService();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Přenesení klienta přes intent
        Intent intent = getIntent();
        vybranyPritel = new Osoba(intent.getBundleExtra(def.C_OSOBA));


        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            MapsFragment mapsFragment = new MapsFragment();
            mapsFragment.setOnMapsFragmentCallback(this);

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            //prehledOsob.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, mapsFragment).commit();
        } else {
            MyLog.e(TAG, "Fatální chyba");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (def.isXLargeTablet(this)) return;
        Intent intent=new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}

