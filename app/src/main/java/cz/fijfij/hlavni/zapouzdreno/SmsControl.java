package cz.fijfij.hlavni.zapouzdreno;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.text.TextUtils;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.fijfij.hlavni.AsyncService;
import cz.fijfij.hlavni.BufferOp;
import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.Osoba;
import cz.fijfij.hlavni.PrateleOp;
import cz.fijfij.hlavni.SettingOp;
import cz.fijfij.hlavni.SpojeniOp;
import cz.fijfij.hlavni.def;
import cz.fijfij.hlavni.lokace.LatLngItem;
import cz.fijfij.hlavni.sms.SmsItem;

import static cz.fijfij.hlavni.sms.SmsItem.SMS_HASH;

/**
 * Created by jiri.kursky on 5.8.2016
 * Ovládáni SMS
 */
public class SmsControl {
    private static final String TAG = "SMS";
    public static final String TEST = "test";
    static boolean readSendRunning = false;

    public final Context context;
    private BufferOp bufferOp;
    private PrateleOp prateleOp;
    private ArrayList<SmsItem> arSmsItem = new ArrayList<>();

    public SmsControl(@NonNull PrateleOp prateleOp) {
        this.bufferOp = prateleOp.getBufferOp();
        this.context = bufferOp.getContext();
        this.prateleOp = prateleOp;
    }

    public SmsControl(@NonNull Context context) {
        this.context = context;
        bufferOp = null;
        prateleOp = null;
    }

    /**
     * Odeslání sms na příslušnou adresu
     *
     * @param phoneNumber telefonní číslo
     * @param message     zpráva k odeslani
     * @return při úspěchu vrací true
     */
    boolean send(@Nullable String phoneNumber, @Nullable String message) {
        if (TextUtils.isEmpty(phoneNumber) || message == null) return false;
        boolean retVal = false;
        SmsManager sms = SmsManager.getDefault();
        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent("sent"), 0);
        try {
            sms.sendTextMessage(phoneNumber, null, message, sentPI, null);
            retVal = true;
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        return retVal;
    }

    /**
     * Odešle lokaci pomocí SMS na příslušné číslo
     */
    public void odesliLokaci(SmsItem smsItem) {
        SettingOp settingOp = new SettingOp(context);

        LatLngItem langLngItem = settingOp.getLangLngItem();
        String sLat = langLngItem.sGetLatitude();
        String sLong = langLngItem.sGetLongitude();


        String zprava = SMS_HASH + SmsItem.ODPOVED_LOKACE + "#" + smsItem.getSmsID() + "#" + sLat + ":" + sLong + ":" + langLngItem.iGetProvider() +
                ":" + settingOp.getString(def.PREF_BATTERY_PERCENT);
        if (def.emulator) MyLog.d(TAG, "Odpoved: " + zprava);
        send(smsItem.getCislo(), zprava);
    }


    /**
     * Odešle žádost o spojení na příslušné číslo
     */
    public void odesliZadostRodice(String cislo) {
        String zprava = SMS_HASH + SmsItem.CHCI_SE_SPOJIT + "#" + def.vratSMSID();
        if (def.emulator) MyLog.d(TAG, "Odeslano: " + zprava);
        send(cislo, zprava);
    }

    /**
     * Odešle androidId
     */
    public void odesliOdpovedClena(SmsItem smsItem) {
        String zprava = SMS_HASH + SmsItem.ME_ANDROID_ID + "#" + def.android_id;
        if (def.emulator) MyLog.d(TAG, "Odeslano: " + zprava);
        send(smsItem.getCislo(), zprava);
    }


    /**
     * Přečte všechny sms a příslušně naplní buffer
     * v bufferu budou jen zaznamy náležející přátelům
     * pozor tato kombinace platí pouze pro řídícího
     */
    private void naplnArchivRidici() {
        arSmsItem.clear();
        Cursor cursor = getOutbox();
        int typSMS;

        try {
            if (cursor != null && cursor.moveToFirst()) { // must check the result to prevent exception
                do {
                    SmsItem smsItem = new SmsItem(context);
                    typSMS = smsItem.parse(cursor, prateleOp);
                    if (prateleOp.vratDleTelefonu(smsItem.getCislo()) != null) { // je otazka, je-li v přátelích
                        if (typSMS == SmsItem.SMS_VRAT_LOKACI) {
                            addZadostSMS(smsItem);
                        }
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        cursor = getInbox();
        try {
            if (cursor != null && cursor.moveToFirst()) { // must check the result to prevent exception
                do {
                    SmsItem smsItem = new SmsItem(context);
                    typSMS = smsItem.parse(cursor, prateleOp);
                    if (prateleOp.vratDleTelefonu(smsItem.getCislo()) != null) { // je otazka, je-li v přátelích
                        if (typSMS == SmsItem.SMS_ODPOVED_LOKACE) {
                            zapisOdpovedSMS(smsItem);
                        }
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
    }

    private void naplnArchivClen() {
        arSmsItem.clear();
        Cursor cursor = getInbox();

        int typSMS;

        try {
            if (cursor != null && cursor.moveToFirst()) { // must check the result to prevent exception
                do {
                    SmsItem smsItem = new SmsItem(context);
                    typSMS = smsItem.parse(cursor, prateleOp);
                    if (prateleOp.vratDleTelefonu(smsItem.getCislo()) != null) { // je otazka, je-li v přátelích
                        if (typSMS == SmsItem.SMS_VRAT_LOKACI) {
                            if (nebylaOdpoved(smsItem)) addZadostSMS(smsItem);
                        }
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }

        cursor = getOutbox();

        try {
            if (cursor != null && cursor.moveToFirst()) { // must check the result to prevent exception
                do {
                    SmsItem smsItem = new SmsItem(context);
                    typSMS = smsItem.parse(cursor, prateleOp);
                    if (prateleOp.vratDleTelefonu(smsItem.getCislo()) != null) { // je otazka, je-li v přátelích
                        if (typSMS == SmsItem.SMS_ODPOVED_LOKACE) {
                            zapisOdpovedSMS(smsItem);
                        }
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
    }

    /**
     * Přečte všechny sms a příslušné SMS a naplní buffer
     * v bufferu budou jen zaznamy náležející danému příteli
     * Tento příkaz funguje jen když je řídící pro člena rodiny je doplnArchivClenRodiny
     */
    public void doplnArchivRidici(String pritelId) {
        Cursor cursor = getOutbox();
        String smsPritelId;


        int typSMS;

        try {
            if (cursor != null && cursor.moveToFirst()) { // must check the result to prevent exception
                do {
                    SmsItem smsItem = new SmsItem(context);
                    typSMS = smsItem.parse(cursor, prateleOp);
                    smsPritelId = prateleOp.vratDleTelefonuID(smsItem.getCislo());
                    if (!TextUtils.isEmpty(smsPritelId) && (smsPritelId.equals(pritelId))) {
                        if (typSMS == SmsItem.SMS_VRAT_LOKACI) {
                            addZadostSMS(smsItem);
                        }
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }

        cursor = getInbox();

        try {
            if (cursor != null && cursor.moveToFirst()) { // must check the result to prevent exception
                do {
                    SmsItem smsItem = new SmsItem(context);
                    typSMS = smsItem.parse(cursor, prateleOp);
                    smsPritelId = prateleOp.vratDleTelefonuID(smsItem.getCislo());
                    if (!TextUtils.isEmpty(smsPritelId) && (smsPritelId.equals(pritelId))) {
                        if (typSMS == SmsItem.SMS_ODPOVED_LOKACE) {
                            zapisOdpovedSMS(smsItem);
                        }
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
    }

    public boolean vyridNevyrizeneSMS() {
        if (def.jeRidici()) return vyridNevyrizeneSMSRidici();
        return vyridNevyrizeneSMSClen();
    }

    /**
     * Přečte vsechny relevantní sms a odešle odpoved
     * jinak se o vse postará událost příchodu sms
     * Musi však být zapsané v bufferu
     */
    private boolean vyridNevyrizeneSMSRidici() {
        boolean retVal = false;
        if (readSendRunning) return false;
        readSendRunning = true;
        ArrayList<Osoba> vsichniPratele = prateleOp.getArPratele();
        // Zajímají mě jen nové prichozi, na které je potřeba reagovat

        for (Osoba pritel : vsichniPratele) {
            openDatabase(BufferOp.DB_READONLY);
            String pritelId = pritel.getUserId();
            Cursor res = bufferOp.mDatabase.rawQuery("select * from " + BufferOp.TBL_SOURADNICE + " where pritelId='" + pritelId + "'" +
                    " and cas_prepsani is null order by id desc", null);
            if (res.moveToFirst()) {
                // vymazat stare zaznamy
                int idNevyplnene = def.parseInt(res.getString(res.getColumnIndex("id")));
                bufferOp.closeDatabase(res);
                openDatabase(BufferOp.DB_WRITABLE);
                bufferOp.mDatabase.execSQL("delete from " + BufferOp.TBL_SOURADNICE + " where pritelId='" + pritelId + "'" +
                        " and id!='" + idNevyplnene + "'");
                bufferOp.closeDatabase();
            } else {
                bufferOp.closeDatabase(res);
            }
        }
        readSendRunning = false;
        return retVal;
    }

    /**
     * Nevyřízené SMS - ale pozor, je otázka, kdy začít
     */
    private boolean vyridNevyrizeneSMSClen() {
        boolean retVal = false;
        if (readSendRunning) return false;
        readSendRunning = true;
        ArrayList<Osoba> vsichniPratele = prateleOp.getArPratele();
        // Zajímají nás jen nově příchozí, na které je potřeba reagovat


        for (Osoba pritel : vsichniPratele) {
            openDatabase(BufferOp.DB_READONLY);
            String pritelId = pritel.getUserId();
            Cursor res = bufferOp.mDatabase.rawQuery("select * from " + BufferOp.TBL_SOURADNICE + " where pritelId='" + pritelId + "'" +
                    " and cas_prepsani is null order by id desc", null);

            SmsItem smsItemOdeslat = null;
            int idNeodeslane = -1;
            if (res.moveToFirst()) { // vymazat stare zaznamy
                idNeodeslane = def.parseInt(res.getString(res.getColumnIndex("id")));
                smsItemOdeslat = new SmsItem(context);
                smsItemOdeslat.clear();
                smsItemOdeslat.setFromBuffer(pritel, res);
            }
            bufferOp.closeDatabase(res);
            if (smsItemOdeslat != null) {
                openDatabase(BufferOp.DB_WRITABLE);
                bufferOp.mDatabase.execSQL("update " + BufferOp.TBL_SOURADNICE + " set cas_prepsani=cas where pritelId='" + pritelId +
                        "' and id!=" + idNeodeslane);
                zapisOdpovedSMS(smsItemOdeslat);
                bufferOp.closeDatabase();
                odesliLokaci(smsItemOdeslat);
                retVal = true;
            }
        }
        readSendRunning = false;
        return retVal;
    }

    /**
     * Zažádání o souřadnice
     *
     * @param pritel
     */
    public void askCoor(Osoba pritel) {
        if (pritel == null) return;
        String address = pritel.getPhoneNumber();
        if (TextUtils.isEmpty(address)) return;
        String smsID = def.vratSMSID();
        if (def.emulator) {
            smsID = "148252168511";
        }
        String zprava = SMS_HASH + SmsItem.VRAT_LOKACI + "#" + smsID;
        if (def.emulator) MyLog.d(TAG, zprava);
        bufferOp.zapisSMSOdeslano(pritel, smsID);
        send(address, zprava);
    }

    private void odpovedLokace(SmsItem smsItem) {
        zapisOdpovedSMS(smsItem);
    }

    /**
     * Jedná se jen o zápis žádosti - opět řízeno přáteli
     *
     * @param smsItem
     * @return
     */
    public boolean addZadostSMS(SmsItem smsItem) {
        openDatabase(BufferOp.DB_WRITABLE);
        String smsID = smsItem.getSmsID();
        String pritelId = smsItem.getUserId();
        if (TextUtils.isEmpty(smsID) || TextUtils.isEmpty(pritelId)) return false;
        Cursor res = bufferOp.mDatabase.rawQuery("select * from " + bufferOp.TBL_SOURADNICE + " where smsID='" +
                smsID + "' and pritelId='" + pritelId + "'", null);
        if (res.moveToFirst()) return false; // dana zadost již byla evidována
        ContentValues values = new ContentValues();
        values.put(def.CL_SMS_ID, smsItem.getSmsID());
        values.put("pritelId", smsItem.getUserId());
        values.put("cas", smsItem.getDatumSMS());
        bufferOp.mDatabase.insert(bufferOp.TBL_SOURADNICE, null, values);
        bufferOp.closeDatabase(res);
        return true;
    }

    private void openDatabase(boolean writable) {
        if (bufferOp == null) bufferOp = new BufferOp(context);
        bufferOp.openDatabase(writable);
    }

    /**
     * Spouští se ze služby a zahrnuje přátele
     */
    public void registrujVsechnySMS() {
        if (def.jeRidici()) {
            naplnArchivRidici();
        } else {
            naplnArchivClen();
        }
        for (SmsItem smsItem : arSmsItem) {
            if (smsItem.getZadost() == SmsItem.SMS_VRAT_LOKACI) {
                if (nebylaOdpoved(smsItem)) addZadostSMS(smsItem);
            }
            if (smsItem.getZadost() == SmsItem.SMS_ODPOVED_LOKACE) {
                odpovedLokace(smsItem);
            }
        }
        arSmsItem.clear();
    }

    /**
     * Zapíše do bufferu všechny SMS a bude je požadovat za vyřízené
     * Toto udělá jen při prvním spuštění
     */
    public void ignorujVsechnySMS() {
        SettingOp settingOp = new SettingOp(context);
        if (!settingOp.getBoolean(def.PREF_PRVNI_NAHRANI_SMS, true)) return;
        settingOp.putBoolean(def.PREF_PRVNI_NAHRANI_SMS, false);

        Cursor cursor = getInbox();
        int typSMS;
        try {
            if (cursor != null && cursor.moveToFirst()) { // must check the result to prevent exception
                do {
                    SmsItem smsItem = new SmsItem(context);
                    typSMS = smsItem.parse(cursor, null);
                    if (typSMS == SmsItem.SMS_ODPOVED_LOKACE || typSMS == SmsItem.SMS_VRAT_LOKACI) {
                        zapisPrimo(smsItem); // Tzn. nebude se ohlížet na přítele
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }

        cursor = getOutbox();
        try {
            if (cursor != null && cursor.moveToFirst()) { // must check the result to prevent exception
                do {
                    SmsItem smsItem = new SmsItem(context);
                    typSMS = smsItem.parse(cursor, null);
                    if (typSMS == SmsItem.SMS_ODPOVED_LOKACE || typSMS == SmsItem.SMS_VRAT_LOKACI) {
                        zapisPrimo(smsItem); // Tzn. nebude se ohlížet na přítele
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }

        //settingOp.putBoolean(def.PREF_PRVNI_NAHRANI_SMS, false);
    }

    /**
     * Odešle testovací SMS na dané číslo
     *
     * @param address testované číslo
     * @return zpráva
     */
    public String sendTest(String address) {
        String zprava = SMS_HASH + TEST + "#" + System.currentTimeMillis();
        send(address, zprava);
        return zprava;
    }

    /**
     * Označí SMS jako přečtenou v bufferu
     *
     * @param smsID
     * @param pritelId
     */
    public void vymazSMSUdaj(String smsID, String pritelId) {
        openDatabase(BufferOp.DB_WRITABLE);

        ContentValues values = new ContentValues();
        values.put("nezobrazovat", 1);
        try {
            bufferOp.mDatabase.update(BufferOp.TBL_SOURADNICE, values, "pritelId=" + pritelId + " and smsID=" + smsID, null);
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        bufferOp.closeDatabase();
    }

    public void odesliZadostPratelstvi(String address) {
        String zprava = SMS_HASH + SmsItem.UZIVATEL_ID + "#" + System.currentTimeMillis() + "#" + def.osoba.getUserId();
        send(address, zprava);
    }

    /**
     * Nastaví aid od přítele, zkontroluje, je-li to adekvátní
     *
     * @param bundle z SMS
     */
    public boolean pridaniPritele(Bundle bundle, final SpojeniOp.OdeslanoServeru odeslanoServeru) {
        final ArrayList<SmsItem> smsItems = new ArrayList<>();
        SmsMessage currentMessage;
        Object[] pdus = (Object[]) bundle.get("pdus");
        final Object[] pdusObj = (Object[]) bundle.get("pdus");
        int typSMS;
        for (int i = 0; i < pdusObj.length; i++) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                String format = bundle.getString("format");
                currentMessage = SmsMessage.createFromPdu((byte[]) pdus[i], format);
            } else {
                currentMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
            }
            String message = currentMessage.getDisplayMessageBody();
            SmsItem smsItem = new SmsItem(context);
            typSMS = SmsItem.parseMessage(message, smsItem);
            if (typSMS == SmsItem.SMS_ME_ANDROID_ID) {
                smsItem.setCislo(currentMessage.getDisplayOriginatingAddress());
                smsItems.add(smsItem);
            }
        }
        if (smsItems.size() == 0) return false;
        final BufferOp bufferOp = new BufferOp(context);
        final PrateleOp prateleOp = new PrateleOp(bufferOp);
        final SpojeniOp spojeniOp = new SpojeniOp(context);
        prateleOp.executeVse(new PrateleOp.CallBackListener() {
            @Override
            public void onFinished() {
                for (SmsItem smsItem : smsItems) {
                    Osoba pritel = prateleOp.vratDleTelefonu(smsItem.getCislo());
                    if (pritel == null || !pritel.spojeniNepotvrzeno()) continue;

                    spojeniOp.potvrzenyPritel(pritel, smsItem.getSmsID(), odeslanoServeru);
                }

            }
        });
        return true;
    }


    /**
     * @param bundle
     * @param taskListener
     * @return vrací true pokud jde skutečně o sms žádající zaslání konfigurace
     */
    public boolean pridejRodice(Bundle bundle, final AsyncService.TaskListener taskListener) {
        SmsMessage currentMessage;
        Object[] pdus = (Object[]) bundle.get("pdus");
        final Object[] pdusObj = (Object[]) bundle.get("pdus");
        int typSMS;
        for (int i = 0; i < pdusObj.length; i++) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                String format = bundle.getString("format");
                currentMessage = SmsMessage.createFromPdu((byte[]) pdus[i], format);
            } else {
                currentMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
            }
            String message = currentMessage.getDisplayMessageBody();
            final SmsItem smsItem = new SmsItem(context);
            typSMS = SmsItem.parseMessage(message, smsItem);
            if (typSMS == SmsItem.SMS_CHCI_SE_SPOJIT) {

                final String phoneNumber = def.phoneNumberStandard(currentMessage.getDisplayOriginatingAddress());
                String action = def.EX_ACTION + def.CMD_OVER_ZADOST;
                action += def.EX_PHONE_NUMBER + phoneNumber;
                action += def.EX_ANDROID_ID + def.android_id;
                AsyncService asyncService = new AsyncService(new AsyncService.TaskListener() {
                    @Override
                    public void onFinished(String nameAction, JSONObject result) {
                        if (result == null) return;
                        Bundle bundle = def.transferJSON(result);
                        String id = bundle.getString(def.RET_ID, "");
                        if (TextUtils.isEmpty(id)) {
                            // Není žádáno od správného rodiče
                            return;
                        }
                        smsItem.setCislo(phoneNumber);
                        odesliOdpovedClena(smsItem);
                        taskListener.onFinished(nameAction, result);
                    }
                }, context, def.CMD_OVER_ZADOST, action);
                asyncService.execute();
                return true;
            }
        }
        return false;
    }

    @Nullable
    private Cursor getInbox() {
        Cursor cursor;
        try {
            cursor = context.getContentResolver().query(Uri.parse("content://sms/inbox"), SmsItem.сolumns, null, null, null);
        } catch (Exception e) {
            cursor = null;
        }
        return cursor;
    }

    @Nullable
    private Cursor getOutbox() {
        Cursor cursor;
        try {
            cursor = context.getContentResolver().query(Uri.parse("content://sms/sent"), SmsItem.сolumns, null, null, null);
        } catch (Exception e) {
            cursor = null;
        }
        return cursor;
    }

    /**
     * Vyhledá sms dle smsID a zapíše odpověď
     *
     * @param smsItem class sms načtený z telefonu
     */
    public void zapisOdpovedSMS(SmsItem smsItem) {
        openDatabase(BufferOp.DB_WRITABLE);
        String pritelId = smsItem.getUserId();
        String smsID = smsItem.getSmsID();
        Cursor res;
        try {
            res = bufferOp.mDatabase.rawQuery("select * from " + BufferOp.TBL_SOURADNICE + " where pritelId='" + pritelId +
                    "' and smsID='" + smsID + "' order by id desc", null);
            if (res == null) return;
        } catch (Exception e) {
            MyLog.e(TAG, e);
            return;
        }
        if (!res.moveToFirst()) return;
        String casPrepsani = res.getString(res.getColumnIndex("cas_prepsani"));
        if (!TextUtils.isEmpty(casPrepsani)) return;

        ContentValues value = new ContentValues();
        value.put("cas_prepsani", smsItem.getDatumSMS());
        value.put(def.PREF_LONGITUDE, smsItem.getLongitude());
        value.put(def.PREF_LATITUDE, smsItem.getLatitude());
        value.put(def.PREF_TYP_ZJISTENI_LOKACE, smsItem.getTypZjisteniLokace());
        try {
            bufferOp.mDatabase.update(BufferOp.TBL_SOURADNICE, value, "pritelId=" + pritelId + " and smsID='" + smsID + "'", null);
            bufferOp.mDatabase.execSQL("delete from " + BufferOp.TBL_SOURADNICE + " where pritelId='" + pritelId + "' and cas_prepsani is null and smsID!=''");
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        bufferOp.closeDatabase(res);
    }

    private boolean nebylaOdpoved(SmsItem smsItem) {
        Cursor res;
        openDatabase(BufferOp.DB_READONLY);
        try {
            res = bufferOp.mDatabase.rawQuery("select * from " + BufferOp.TBL_SOURADNICE + " where smsID='" +
                    smsItem.getSmsID() + "' and " + def.CL_CAS_PREPSANI + " is not null", null);
            if (res == null) {
                bufferOp.closeDatabase();
                return false;
            }

            if (res.moveToFirst()) {
                String casPrepsani;
                try {
                    casPrepsani = res.getString(res.getColumnIndex(def.CL_CAS_PREPSANI));
                } catch (Exception e) {
                    casPrepsani="";
                }
                bufferOp.closeDatabase(res);
                return def.nulovyCas(casPrepsani);
            }
            return false;
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
        return false;
    }

    private void zapisPrimo(SmsItem smsItem) {
        openDatabase(BufferOp.DB_WRITABLE);
        String smsID = smsItem.getSmsID();
        Cursor res;
        try {
            res = bufferOp.mDatabase.rawQuery("select * from " + BufferOp.TBL_SOURADNICE + " where smsID='" +
                    smsID + "'", null);
            if (res == null || !res.moveToFirst()) {
                ContentValues value = new ContentValues();
                value.put(def.CL_CAS_PREPSANI, smsItem.getDatumSMS());
                value.put(def.CL_SMS_ID, smsID);
                bufferOp.mDatabase.insert(BufferOp.TBL_SOURADNICE, null, value);
                return;
            }
        } catch (Exception e) {
            MyLog.e(TAG, e);
            return;
        }

        if (res != null) {
            String casPrepsani = res.getString(res.getColumnIndex(def.CL_CAS_PREPSANI));
            if (TextUtils.isEmpty(casPrepsani)) {
                ContentValues value = new ContentValues();
                value.put(def.CL_CAS_PREPSANI, smsItem.getDatumSMS());
                try {
                    bufferOp.mDatabase.update(BufferOp.TBL_SOURADNICE, value, "smsID='" + smsID + "'", null);
                } catch (Exception e) {
                    MyLog.e(TAG, e);
                }
            }
        }
        bufferOp.closeDatabase(res);
    }
}
