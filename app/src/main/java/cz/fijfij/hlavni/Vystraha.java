package cz.fijfij.hlavni;

import android.content.Context;

import org.json.JSONObject;

/**
 * Created by jiri.kursky on 14.03.2017
 */

public class Vystraha {
    private SettingOp settingOp;
    private Context context;

    public Vystraha(SettingOp settingOp) {
        this.settingOp = settingOp;
        context = settingOp.getContext();
    }

    public void zapisVystrahu(int typVystrahy, String pritelId, String msgText) {
        String action = settingOp.getActionString(def.CMD_VYSTRAHA);
        action += def.EX_TYP_VYSTRAHY + typVystrahy;
        action += def.EX_PRITELID + pritelId;
        action += def.EX_MSG_TEXT + msgText;
        AsyncService asService = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {

            }
        }, context, def.CMD_VYSTRAHA, action);
        asService.execute();
    }
}
