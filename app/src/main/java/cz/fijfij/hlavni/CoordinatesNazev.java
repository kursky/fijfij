/*
 * Copyright (c) 2017 Jiri Kursky
 */

package cz.fijfij.hlavni;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by jiri.kursky on 16.01.2017.
 */
class CoordinatesNazev {
    private LatLng latlng;
    private String nazev;
    private boolean sendSMS;


    CoordinatesNazev(LatLng latlng, String nazev) {
        this.latlng=latlng;
        this.nazev=nazev;
    }
}
