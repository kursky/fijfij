/*
 * Copyright (c) 2017 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import cz.fijfij.R;
import cz.fijfij.hlavni.service.FijService;

/**
 * Created by jiri.kursky on 05.01.2017.
 * Testovací stránka
 */

public class TestovaciStranka extends BaseActivity {
    private ServiceOp serviceOp;
    private TextView txtRychlost;
    private ArrayList<String> zpravy = new ArrayList<>();


    public TestovaciStranka() {
        super(R.layout.testovaci_stranka);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SettingOp settingOp = new SettingOp(this);
        txtRychlost = (TextView) findViewById(R.id.txtRychlost);

        final AdapterZpravy adapterZpravy = new AdapterZpravy(this, zpravy);
        ListView listPoznamek=(ListView) findViewById(R.id.listPoznamek);
        listPoznamek.setAdapter(adapterZpravy);

        serviceOp = new ServiceOp(this, new ServiceOp.ServiceInt() {
            @Override
            public void onServiceConnected() {
                serviceOp.registerClient();
            }

            @Override
            public void onServiceDisconnected() {
                serviceOp.unRegisterClient();
            }

            @Override
            public boolean receivedMessage(Message msg) {
                switch (msg.what) {
                    case FijService.MSG_RYCHLOST:
                        int rychlost = msg.arg1;
                        txtRychlost.setText(rychlost + " km/h");
                        break;
                    case FijService.MSG_DEBUG:
                        String s=msg.getData().getString(def.PREF_ZPRAVA, "");
                        if (!TextUtils.isEmpty(s)) {
                            zpravy.add(s);
                            adapterZpravy.notifyDataSetChanged();
                        }
                }
                return false;
            }

        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (def.debug) serviceOp.doBindService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (def.debug) serviceOp.doUnbindService();
    }

}
