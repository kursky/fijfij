package cz.fijfij.hlavni;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import org.json.JSONObject;

/**
 * Created by jiri.kursky on 5.8.2016.
 * Identifikuje daného uživatele
 */
public class Identifikace {
    private Context context;
    private SettingOp settingOp;
    private CallBackListener callBackListener;
    public static final int ST_OK = 10;
    public static final int ST_CHYBA_BEHEM_KOMUNIKACE = 20;
    public static final int ST_NEAUTORIZOVAN = 40; // sit vrátila, že není ten správný
    public static final int ST_NENI_SIT = 50; // neni v bufferu a sit vypnuta
    private boolean zeSluzby;


    public Identifikace(SettingOp settingOp, boolean zeSluzby, CallBackListener callBackListener) {
        this.context = settingOp.getContext();
        this.callBackListener = callBackListener;
        this.settingOp = settingOp;
        this.zeSluzby = zeSluzby;
    }

    public Identifikace(SettingOp settingOp, boolean zeSluzby) {
        this.context = settingOp.getContext();
        this.callBackListener = null;
        this.settingOp = settingOp;
        this.zeSluzby = zeSluzby;
    }

    public boolean nastavVysledekUzivatelSit(Bundle bundle) {
        if (TextUtils.isEmpty(bundle.getString(def.RET_USER_ID))) return false;
        if (def.osoba == null) def.osoba = new Osoba();
        def.osoba.nastavParametryBundleAll(bundle, def.vratTypParametru());
        return true;
    }


    public interface CallBackListener {
        void onFinished(Osoba osoba, int status);
    }

    /**
     * Zde se analyzuje navrat ze serveru, pokud je smysluplný návrat přejímají se udaje a je-li časové razítko ok
     * Pokud je razítko staré, berou se údaje z preferencí - jsou-li tam
     */
    private AsyncService.TaskListener userUdaje = new AsyncService.TaskListener() {
        @Override
        public void onFinished(String nameAction, JSONObject result) {
            int status = ST_OK;
            if (def.osoba == null) def.osoba = new Osoba();
            if (result == null) { // mohla se stat chyba během komunikace
                def.osoba = settingOp.vratParametryUzivatel(def.vratTypParametru());
                callBackListener.onFinished(def.osoba, ST_CHYBA_BEHEM_KOMUNIKACE);
            } else {
                Bundle bundle = def.transferJSON(result);
                String userId = bundle.getString(def.RET_USER_ID, "");
                if (TextUtils.isEmpty(userId)) {
                    status = ST_NEAUTORIZOVAN;
                    callBackListener.onFinished(def.osoba, status);
                    return;
                }
                def.setPIN(bundle);
                settingOp.putBoolean(def.PREF_REGISTRACE_ZRUSENA, false);
                if (parametryZeSite(bundle)) {
                    if (nastavVysledekUzivatelSit(bundle)) {
                        settingOp.nastavUzivateleDoPreference(def.osoba, ParametrOp.PARS_ADMIN);
                    }
                } else {
                    settingOp.uzivatelZNastaveni(def.osoba, ParametrOp.PARS_ADMIN);
                    if (zeSluzby)
                        settingOp.sendUserParametres(def.osoba, false, null); // zasílají se parametry jen pri spuštěné službě
                }
                callBackListener.onFinished(def.osoba, status);
            }
        }

    };

    public void sitUdaje(AsyncService.TaskListener taskListener) {
        String action = settingOp.getActionString(def.CMD_UDAJE_USER);
        AsyncService asService = new AsyncService(taskListener, context, def.CMD_UDAJE_USER, action);
        asService.execute();
    }

    /**
     * Nastavuje Osoba se všemi parametry
     */
    public void execute() {
        if (def.isNetworkAllowed(settingOp)) {
            sitUdaje(userUdaje);
        } else {
            Osoba osoba = settingOp.vratParametryUzivatel(ParametrOp.PARS_ADMIN);
            def.setPIN(settingOp.getString(def.PREF_PIN, ""));
            int status = ST_OK;
            if (TextUtils.isEmpty(osoba.getUserId())) status = ST_NEAUTORIZOVAN;
            def.nastavUzivatele(osoba);
            callBackListener.onFinished(osoba, status);
        }
    }


    /**
     * Vrátí true, pokud se budou nastavovat údaje ze sítě nebo z preferenci na základě časového razítka
     */
    public boolean parametryZeSite(Bundle result) {
        String prefSetupTimestamp = settingOp.getString(def.PREF_SETUP_TIMESTAMP);
        String sentSetupTimestamp = result.getString(def.RET_TIMESTAMP);

        boolean nastavitZeSite = def.nulovyCas(prefSetupTimestamp) && !settingOp.getBoolean(def.PREF_PRVNI_SPUSTENI, true);
        if (!nastavitZeSite) {
            if (!def.nulovyCas(sentSetupTimestamp)) {
                nastavitZeSite = def.datumPrvniNovejsi(sentSetupTimestamp, prefSetupTimestamp);
            }
        }
        return nastavitZeSite;
    }
}
