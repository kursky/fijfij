package cz.fijfij.hlavni;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.ArrayList;

import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.service.FijService;

/**
 * Created by jiri.kursky on 07.11.2016.
 * Tvoří pole z načtených údajů lokací
 */

public class CoordinatesOp {
    static final String TAG = FijService.class.getSimpleName();
    private String pritelId;
    private BufferOp bufferOp;
    private boolean wasRead = false;
    private ArrayList<CoordinatesItem> arCoordinates = new ArrayList<>(); // jen zobrazene
    private ArrayList<String> arVzdalenost = new ArrayList<>();

    private ArrayList<CoordinatesItem> arCoordinatesNepotvrzene = new ArrayList<>();
    private ArrayList<String> arNepotvrzene = new ArrayList<>();


    public CoordinatesOp(String pritelId, BufferOp bufferOp) {
        this.pritelId = pritelId;
        this.bufferOp = bufferOp;
        wasRead = false;
    }


    /**
     * Přidá souřadnice
     * @param b - definice návrat z centrální databáze
     * @param prvni - první záznam
     */
    public void add(Bundle b, Boolean prvni) {
        wasRead = false;
        bufferOp.addCoordinates(b, prvni);
    }

    /**
     * @param force pokud true budou aktualizovány v každém případě
     */
    public void aktualizuj(boolean force) {
        if (!wasRead || force) {
            nactiUdaje();
            CoordinatesItem lastCoor = null;
            String vzdalenost;
            for (CoordinatesItem c : arCoordinates) {
                vzdalenost = "";
                if (lastCoor == null && c.souradniceOK()) {
                    lastCoor = c;
                } else {
                    if (c.souradniceOK()) {
                        Double v = def.vzdalenostSouradnic(c.getLatitude(), c.getLongitude(), lastCoor.getLatitude(), lastCoor.getLongitude());
                        if (v > 1000) {
                            v = v / 1000;
                            vzdalenost = String.format("%.2f km", v);
                        } else {
                            vzdalenost = String.format("%.0f m", v);
                        }
                        lastCoor = c;
                    } else vzdalenost = "?";

                }
                arVzdalenost.add(vzdalenost);
            }
            if (arVzdalenost.size() > 1) {
                arVzdalenost.remove(0);
                arVzdalenost.add("");
            }
        }
        wasRead = true;
    }

    public int getSizeZpracovane() {
        return arCoordinates.size();
    }


    @Nullable
    public CoordinatesItem getCoordinatesItem(int position) {
        if (position < 0) return null;
        if (position >= arCoordinates.size()) return null;
        return arCoordinates.get(position);
    }

    @Nullable
    CoordinatesItem getCoordinatesItemAktualni(int position) {
        return getCoordinatesItem(position);
    }

    String getVzdalenost(int i) {
        if (i < 0) return "";
        if (i < arVzdalenost.size())
            return arVzdalenost.get(i);
        else
            return "";
    }


    public void clear() {
        arCoordinates.clear();
        arVzdalenost.clear();
        arNepotvrzene.clear();
        wasRead = false;
    }

    /**
     * Základní procedura pro zobrazeni údajů z bufferu do coordinates
     */
    private void nactiUdaje() {
        bufferOp.openDatabase(BufferOp.DB_READONLY);
        Cursor res;
        try {
            res = bufferOp.mDatabase.rawQuery("select * from " + BufferOp.TBL_SOURADNICE + " where pritelId='" + pritelId +
                    "' order by cas_prepsani desc", null);
        } catch (Exception e) {
            MyLog.e(TAG, e);
            return;
        }
        arCoordinates.clear();
        arNepotvrzene.clear();
        arVzdalenost.clear();
        arCoordinatesNepotvrzene.clear();
        if (!res.moveToFirst()) return;
        do {
            try {
                CoordinatesItem ci = new CoordinatesItem(res);
                if (ci.coordinatesOK()) {
                    if (TextUtils.isEmpty(ci.getCasPrepsani())) {
                        arNepotvrzene.add(ci.getCas());
                        arCoordinatesNepotvrzene.add(ci);
                    } else
                        arCoordinates.add(new CoordinatesItem(res));
                }
            } catch (Exception e) {
                MyLog.e(TAG, e);
                return;
            }
        } while (res.moveToNext());
        bufferOp.closeDatabase(res);
    }

    public void nactiUdaje(Osoba osoba) {
        pritelId=osoba.getUserId();
        nactiUdaje();
    }

    ArrayList<String> getArVzdalenost() {
        aktualizuj(false);
        return arVzdalenost;
    }

    CharSequence getIdZdroj(int i) {
        CoordinatesItem ci = getCoordinatesItem(i);
        if (ci == null) return "";
        return ci.getIdZdroj();
    }

    public ArrayList<String> getArNepotvrzene() {
        aktualizuj(false);
        return arNepotvrzene;
    }

    @NonNull
    String getNepotvrzeneCas(int position) {
        if (arCoordinatesNepotvrzene.size() == 0) return "";
        if (position <= arCoordinatesNepotvrzene.size()) {
            CoordinatesItem ci = arCoordinatesNepotvrzene.get(position);
            return ci.getCas();
        }
        return "";
    }

    boolean jsouNaMape(int position) {
        CoordinatesItem ci = getCoordinatesItem(position);
        return ci != null && ci.souradniceOK();
    }
}