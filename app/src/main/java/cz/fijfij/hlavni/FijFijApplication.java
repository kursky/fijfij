/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

public class FijFijApplication extends Application {

    /**
     * Multidex umoznuje vice objektu
     * @param context context
     */
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }
}
