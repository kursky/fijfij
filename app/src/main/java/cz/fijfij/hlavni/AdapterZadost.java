package cz.fijfij.hlavni;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import cz.fijfij.R;
/**
 * Created by jiri.kursky on 07.11.2016.
 * Napojeny adapter na ActivityOsoba
 */

class AdapterZadost extends ArrayAdapter<String> {
    private Activity activity;
    private CoordinatesOp coordinatesOp;


    AdapterZadost(Activity activity, CoordinatesOp coordinatesOp) {
        super(activity, R.layout.list_single_osoba, coordinatesOp.getArNepotvrzene());
        this.coordinatesOp = coordinatesOp;
        this.activity = activity;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_single_zadost, null, true);
        TextView txtCas = (TextView) rowView.findViewById(R.id.txtZadostCas);
        String cas = def.dateMySQLczech(coordinatesOp.getNepotvrzeneCas(position));
        txtCas.setText(cas);
        return rowView;
    }
}

