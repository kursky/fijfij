/*
 * Copyright (c) 2016 Jiri Kursky
 * První spuštěná aplikace. Mimo toho, kdy jse spuštěná přímo z funkce.
 * Dále pak následuje "Otazky" - to se ptá na povolení k přístupům
 */

package cz.fijfij.hlavni;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;

import cz.fijfij.R;
import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.lokace.LocationService;
import cz.fijfij.hlavni.service.FijService;


public class Splash extends Activity {
    private ServiceOp serviceOp;
    private Context context;
    private SettingOp settingOp;
    private ProverPristupy proverPristupy;
    private TextView txtVerze;
    static final String TAG = Splash.class.getSimpleName();

    ///////////////////////////////////////////////////////////
    // Komunikace se službou
    ServiceOp.ServiceInt serviceInt = new ServiceOp.ServiceInt() {

        @Override
        public void onServiceConnected() {
            serviceOp.registerClient();
            serviceOp.sendMessageToService(FijService.MSG_TEST_SERVICE);
        }

        @Override
        public void onServiceDisconnected() {
            serviceOp.unRegisterClient();
        }

        /**
         * Návrat ze služby
         */
        @Override
        public boolean receivedMessage(Message msg) {
            switch (msg.what) {
                case FijService.MSG_NOT_RUNNING:
                    // Služba dosud není zaháčkovaná
                    // tzn. je zadefinovaná, ale neproběhl startService
                    serviceOp.startService();
                    break;
                case FijService.MSG_PRATELE_NACTENI:
                case FijService.MSG_OK:
                    SledovaniActivit.startActivity(context, 1000, def.getMainActivity(context));
                    break;
                case FijService.MSG_NEAUTORIZOVAN:
                    llUvodniObrazovka.setVisibility(View.GONE);
                    rlChybaKomunikace.setVisibility(View.VISIBLE);
                    break;

            }
            return false;
        }
    };
    // Komunikace se službou
    /////////////////////////////////////////////////

    private RelativeLayout rlChybaKomunikace;
    private RelativeLayout llUvodniObrazovka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        settingOp = new SettingOp(this);
        def.novaVerze = settingOp.getInt(def.PREF_VERSION_CODE) < def.getVersionCode(this);

        // Kontrola jestli je přístup ke službám telefonu
        // v konstruktoru se definují jednotlivá oprávnění
        // V tuto chvíli je otázka, jestli tam dát lokaci
        proverPristupy = new ProverPristupy(this, settingOp);

        // PREF_NASTAV_PRISTUPY - zatím jen na vypnutí? nemá jiné opodstatnění
        if (settingOp.getBoolean(def.PREF_NASTAV_PRISTUPY, true)) {
            if (proverPristupy.chybiPovoleni()) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Zde se prověřují přístupy
                        Intent intent = new Intent(context, Otazky.class);
                        startActivity(intent);
                    }
                }, 1000);
                return;
            }
        }
        new ParametrOp();
        def.factory(this);
        String pin = settingOp.getString(def.PREF_PIN, "");
        def.setPIN(pin);
        setContentView(R.layout.activity_splash);
        try {
            rlChybaKomunikace = (RelativeLayout) findViewById(R.id.rl_ChybaKomunikace);
        } catch (Exception e) {
            MyLog.e("SP", e);
        }

        TextView txtApp = (TextView) findViewById(R.id.txtApp);
        txtVerze = (TextView) findViewById(R.id.txtVerze);
        if (def.isXLargeTablet(this)) {
            txtApp.setTextSize(112);
            txtVerze.setTextSize(24);
        } else {
            txtApp.setTextSize(50);
            txtVerze.setTextSize(10);
        }
        llUvodniObrazovka = (RelativeLayout) findViewById(R.id.ll_UvodniObrazovka);
        rlChybaKomunikace.setVisibility(View.GONE);
        llUvodniObrazovka.setVisibility(View.VISIBLE);
        String verze = getString(R.string.version_name) + " " + def.getVersionName(this);
        txtVerze.setText(verze);
        new LocationService(settingOp);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dokonciSplash();
            }
        }, 3000);
    }

    /**
     * Voláno se zpožděním z create - z důvodu aby se objevil layout (?)
     * Pokud je síť k dispozici jako první se načítá verze, jinak to jde standardní cestou na kontrolu
     * telefonu
     */
    private void dokonciSplash() {
        if (def.isNetworkAvailable(this)) {
            String action = settingOp.getActionString(def.CMD_VERSION_CODE);
            action += def.EX_COUNTRY + def.getCountry();

            AsyncService as = new AsyncService(new AsyncService.TaskListener() {
                @Override
                public void onFinished(String nameAction, JSONObject result) {
                    if (result != null) {
                        int versionCode = def.resultGetInt(result, def.RET_VERSION_CODE, -1);
                        settingOp.putInt(def.PREF_VERSION_CODE, versionCode);
                        kontrolaTelefonu();
                    }
                }
            }, this, def.CMD_VERSION_CODE, action);
            as.setupStandardListener(new AsyncService.StandardListener() {
                @Override
                public void onReturn(int stReturn, String msg) {
                    if (stReturn == def.ST_BAN) {
                        txtVerze.setText(msg);
                        return;
                    }
                }
            });
            as.execute();
        } else {
            kontrolaTelefonu();
        }
    }


    /**
     * Vyvolá dialog o nutnosti mít zapnutou síť
     */
    private void dlgChybaKomunikace(int typChyby) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.info);
        builder.setCancelable(true);
        int msg;
        switch (typChyby) {
            case def.ERR_REGISTRACE_ZRUSENA:
                msg = R.string.byla_zrusena_registrace;
                break;
            case def.ERR_NENI_ZAPNUTA_SIT:
                msg = R.string.pouze_sit_opet;
                break;
            case def.ERR_CHYBA_KOMUNIKACE:
                msg = R.string.server_neodpovedel;
                break;
            default:
                msg = R.string.nespecifikovana_chyba;
        }

        builder.setMessage(msg);
        builder.setPositiveButton(R.string.dlgOK, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        builder.show();
    }

    /**
     * Pokud se jedná o první spuštění, kontroluje se, je-li telefon zaregistrován
     * a podniknou se další aktivity
     */
    private void kontrolaTelefonu() {
        // Pouze při prvním spuštění
        if (settingOp.getBoolean(def.PREF_PRVNI_SPUSTENI, true)) {
            settingOp.putBoolean(def.PREF_PRVNI_NASTAVENI, true);
            if (!def.isNetworkAvailable(this)) {
                dlgChybaKomunikace(def.ERR_NENI_ZAPNUTA_SIT);
                return;
            }
            // Pozor, toto platí pouze pro prvotní spuštění aplikace
            final Identifikace identifikace = new Identifikace(settingOp, false);
            identifikace.sitUdaje(new AsyncService.TaskListener() {
                @Override
                public void onFinished(String nameAction, JSONObject result) {
                    Bundle bundle = def.transferJSON(result);
                    if (bundle == null) {
                        dlgChybaKomunikace(def.ERR_CHYBA_KOMUNIKACE);
                        return;
                    }
                    String userId = bundle.getString(def.RET_USERID, "");
                    if (userId != null && userId.equals("0")) userId = "";

                    String androidId = bundle.getString(def.RET_ANDROID_ID, "");
                    if (TextUtils.isEmpty((androidId)) && TextUtils.isEmpty(userId)) {
                        SledovaniActivit.startActivity(context, 1000, UvodniLogin.class);
                    } else {
                        if (identifikace.nastavVysledekUzivatelSit(bundle)) {
                            settingOp.nastavUzivateleDoPreference(def.osoba, def.vratTypParametru());
                            settingOp.putBoolean(def.PREF_PRVNI_SPUSTENI, false);
                            serviceOp = new ServiceOp(getBaseContext(), serviceInt);
                            serviceOp.doBindService();
                        } else {
                            SledovaniActivit.startActivity(context, 1000, UvodniLogin.class);
                        }
                    }

                }
            });

        } else {
            // Pokud je řídící mělo by se zjistit, jestli není zrušen
            // zde se nesmí použít def.jeRidici - ještě nejsou zadefinované parametry osoby v classu def
            final boolean jeRidici = settingOp.getBoolean(def.PREF_JE_RIDICI, false);
            if ((def.isNetworkAvailable(context) && jeRidici) || def.isNetworkAllowed(settingOp)) {
                zjistiIdentifikaci();
            } else {
                serviceOp = new ServiceOp(getBaseContext(), serviceInt);
                serviceOp.doBindService();
            }
        }
    }

    /**
     * Identifikace dle android_id, je-li již zaregistrován
     */
    private void zjistiIdentifikaci() {
        final Identifikace identifikace = new Identifikace(settingOp, false);
        identifikace.sitUdaje(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
                Bundle bundle = def.transferJSON(result);
                if (bundle == null) { // Selhala odpoved serveru
                    if (settingOp.getBoolean(def.PREF_REGISTRACE_ZRUSENA, false)) {
                        dlgChybaKomunikace(def.ERR_REGISTRACE_ZRUSENA);
                        return;
                    }
                    serviceOp = new ServiceOp(getBaseContext(), serviceInt);
                    serviceOp.doBindService();
                    return;
                }
                String pin = def.setPIN(bundle);
                settingOp.putString(def.PREF_PIN, pin);
                String email = bundle.getString(def.RET_EMAIL, "");
                settingOp.putString(def.PREF_EMAIL, email);
                String nick = bundle.getString(def.RET_NICK, "");
                settingOp.putString(def.PREF_NICK, nick);
                String userId = bundle.getString("USERID", "");
                if (TextUtils.isEmpty(userId)) {
                    startActivity(new Intent(getBaseContext(), UvodniLogin.class));
                } else {
                    if (identifikace.parametryZeSite(bundle)) {
                        if (identifikace.nastavVysledekUzivatelSit(bundle)) {
                            settingOp.nastavUzivateleDoPreference(def.osoba, def.vratTypParametru());
                        }
                    }
                    serviceOp = new ServiceOp(getBaseContext(), serviceInt);
                    serviceOp.doBindService();
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (serviceOp != null) {
            serviceOp.doUnbindService();
            serviceOp = null;
        }
    }

    public void zkusitZnovu(View view) {
        serviceOp.sendMessageToService(FijService.MSG_TEST_SERVICE, 0, null);
    }
}
