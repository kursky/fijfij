package cz.fijfij.hlavni;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.nastaveni.ParametrItem;

/**
 * Created by jiri.kursky on 5.8.2016.
 * Operace s přáteli
 */
public class PrateleOp {
    private static final ArrayList<Osoba> arPratele = new ArrayList<>();
    private final Context context;
    private CallBackListener callBackListener;
    private final BufferOp bufferOp;
    private ArrayList<ParametrItem> parametrItems;
    private ParametrOp parametrOp;

    public Osoba upravPritele(Bundle bundle) {
        String userId = bundle.getString(def.PREF_USER_ID);
        if (TextUtils.isEmpty(userId)) return null;
        Osoba osoba = vratDleUserId(userId);
        if (osoba == null) return null;
        for (ParametrItem pi : parametrItems) {
            String klic = pi.getKlic();
            switch (pi.getTyp()) {
                case ParametrItem.PAR_BOOLEAN:
                    osoba.setKeyValue(klic, bundle.getInt(klic));
                    break;
                case ParametrItem.PAR_STRING:
                    osoba.setKeyValue(klic, bundle.getString(klic));
                    break;
            }
        }
        zapisDoBufferuPratele(osoba);
        return osoba;
    }

    public BufferOp getBufferOp() {
        return bufferOp;
    }


    public interface CallBackListener {
        void onFinished();
    }

    /**
     * Constructor
     *
     * @param bufferOp         operace s databází - inicializovaný
     * @param callBackListener návratová funkce
     */
    public PrateleOp(BufferOp bufferOp, @Nullable CallBackListener callBackListener) {
        this.context = bufferOp.getContext();
        this.callBackListener = callBackListener;
        this.bufferOp = bufferOp;
        parametrOp = new ParametrOp();
        parametrItems = parametrOp.getParametrItems(ParametrOp.PARS_ADMIN);
        bufferOp.kontrolujTabulku(BufferOp.TBL_PRATELE);
    }

    public PrateleOp(BufferOp bufferOp) {
        this.context = bufferOp.getContext();
        this.callBackListener = null;
        this.bufferOp = bufferOp;
        parametrOp = new ParametrOp();
        parametrItems = parametrOp.getParametrItems(ParametrOp.PARS_CLEN);
        bufferOp.kontrolujTabulku(BufferOp.TBL_PRATELE);
    }

    // Načtení přátel - výsledek ze sítě
    private final AsyncService.TaskListener asyncNacteniPratel = new AsyncService.TaskListener() {
        @Override
        public void onFinished(String nameAction, JSONObject result) {
            if (result != null) {
                Bundle b = def.transferJSON(result);
                definujPratele(b);
            } else {
                nactiOffline();
            }
            if (callBackListener != null) callBackListener.onFinished();
        }
    };


    /**
     * Zapíše pratele do databaze ze zdroje - serveru
     *
     * @param zdroj navrácené hodnoty ze serveru - transformované na bundle
     */
    private void definujPratele(Bundle zdroj) {
        arPratele.clear();

        if (zdroj != null) {
            bufferOp.openDatabase(BufferOp.DB_WRITABLE);
            bufferOp.mDatabase.execSQL("delete from " + BufferOp.TBL_PRATELE);
            bufferOp.closeDatabase();
            ArrayList<ParametrItem> dbItems = parametrOp.getParametrItems(ParametrOp.PARS_DB);

            try {
                for (int i = 1; i <= 5; ++i) {
                    boolean maUdaje = false;
                    for (ParametrItem pi : dbItems) {
                        String dbKlic = pi.getRetKlic() + "_" + i;
                        if (pi.getRetKlic().equals(def.RET_USER_ID)) {
                            maUdaje = !TextUtils.isEmpty(zdroj.getString(dbKlic, ""));
                            break;
                        }
                    }

                    if (maUdaje) {
                        Osoba osoba = new Osoba();
                        for (ParametrItem pi : dbItems) {
                            String dbKlic = pi.getRetKlic() + "_" + i;
                            switch (pi.getTyp()) {
                                case ParametrItem.PAR_BOOLEAN:
                                    osoba.setKeyValue(pi.getKlic(), def.parseInt(zdroj.getString(dbKlic, pi.getDefaultString())));
                                    break;
                                case ParametrItem.PAR_STRING:
                                    osoba.setKeyValue(pi.getKlic(), zdroj.getString(dbKlic, pi.getDefaultString()));
                                    break;
                            }
                        }
                        arPratele.add(osoba);
                        zapisDoBufferuPratele(osoba);
                    }

                }
            } catch (Exception e) {
                MyLog.e("PRATELE", e);
            }
        }
    }

    private static boolean tblPrateleBylaOpravena = false;

    /**
     * Zapíše do bufferu osobu - případně přepisuje
     */
    private void zapisDoBufferuPratele(Osoba osoba) {
        bufferOp.openDatabase(BufferOp.DB_WRITABLE);
        boolean doUpdate = !TextUtils.isEmpty(osoba.getUserId());

        if (doUpdate) {
            Cursor res = bufferOp.mDatabase.rawQuery("select * from " + BufferOp.TBL_PRATELE + " where " + def.PREF_USER_ID + "='" + osoba.getUserId() + "'", null);
            doUpdate = res.moveToFirst();
            res.close();
        }
        ContentValues values = new ContentValues();
        for (ParametrItem pi : parametrItems) {
            if (pi.getPouzeRidici() || (pi.getRetKlic() == null)) continue;
            String klic = pi.getKlic();
            if (pi.getTyp() == ParametrItem.PAR_BOOLEAN)
                values.put(klic, osoba.getKeyValueInt(klic));
            else
                values.put(klic, osoba.getKeyValue(klic));
        }
        try {
            if (doUpdate) {
                bufferOp.mDatabase.update(BufferOp.TBL_PRATELE, values, def.PREF_USER_ID + "='" + osoba.getUserId() + "'", null);
            } else
                bufferOp.mDatabase.insertOrThrow(BufferOp.TBL_PRATELE, null, values);
        } catch (Exception e) {
            if (tblPrateleBylaOpravena) {
                MyLog.e("Zapis pratel do databaze", e.getMessage());
            } else {
                bufferOp.resetTabulky(BufferOp.TBL_PRATELE);
                zapisDoBufferuPratele(osoba);
                tblPrateleBylaOpravena = false;
            }
        }
        bufferOp.closeDatabase();
    }

    /**
     * Vrátí osobu dle čísla telefonu
     *
     * @param cislo hledané číslo telefonu
     * @return Osoba
     */
    public Osoba vratDleTelefonu(String cislo) {
        if (TextUtils.isEmpty(cislo)) return null;
        for (int i = 0; i < arPratele.size(); ++i) {
            if (def.porovnejTelCislo(context, cislo, arPratele.get(i).getPhoneNumber())) {
                try {
                    return arPratele.get(i);
                } catch (Exception e) {
                    return null;
                }
            }
        }
        return null;
    }

    @Nullable
    public String vratDleTelefonuID(String cislo) {
        Osoba osoba = vratDleTelefonu(cislo);
        if (osoba == null) return null;
        return osoba.getUserId();
    }

    @Nullable
    public Osoba vratDleUserId(String userId) {
        for (Osoba osoba : arPratele) {
            if (userId.equals(osoba.getUserId())) return osoba;
        }
        return null;
    }

    private void clear() {
        arPratele.clear();
    }


    public void execute() {
        clear();
        if (def.isNetworkAllowed(new SettingOp(context)) || (def.jeRidici() && def.isNetworkAvailable(context))) {
            AsyncService asService = new AsyncService(asyncNacteniPratel, context, def.CMD_NACTI_PRATELE);
            asService.execute();
        } else {
            nactiOffline();
            if (callBackListener != null) callBackListener.onFinished();
        }
    }

    public void executeVse() {
        clear();
        SettingOp settingOp = new SettingOp(context);
        if (def.isNetworkAllowed(settingOp) || (def.jeRidici() && def.isNetworkAvailable(context))) {
            String action = settingOp.getActionString(def.CMD_NACTI_PRATELE);
            action += def.EX_NEPRIRAZENE;
            AsyncService asService = new AsyncService(asyncNacteniPratel, context, def.CMD_NACTI_PRATELE, action);
            asService.execute();
        } else {
            nactiOffline();
            if (callBackListener != null) callBackListener.onFinished();
        }
    }

    public void executeVse(CallBackListener callBackListener) {
        this.callBackListener = callBackListener;
        executeVse();
    }

    /**
     * Nebude se kontrolovat povolení sítě
     */
    void executeForced() {
        clear();
        AsyncService asService = new AsyncService(asyncNacteniPratel, context, def.CMD_NACTI_PRATELE);
        asService.execute();
    }


    /**
     * Přečte přátele z bufferu
     */
    public void nactiOffline() {
        clear();
        bufferOp.openDatabase(BufferOp.DB_READONLY);
        Cursor res = bufferOp.mDatabase.rawQuery("select * from " + BufferOp.TBL_PRATELE, null);
        try {
            if (res.moveToFirst()) {
                do {
                    arPratele.add(new Osoba(res));
                } while (res.moveToNext());
            }
        } catch (Exception e) {
            MyLog.e("POP", e);
        }
        bufferOp.closeDatabase(res);
    }

    Osoba vratPriteleDlePhoneNumber(String phoneNumber) {
        for (Osoba osoba : arPratele) {
            if (def.porovnejTelCislo(context, osoba.getPhoneNumber(), phoneNumber)) return osoba;
        }
        return null;
    }


    Osoba vratPriteleNick(String nick) {
        nick = nick.toLowerCase();
        for (Osoba osoba : arPratele) {
            if (nick.equals(osoba.getNick().toLowerCase())) return osoba;
        }
        return null;
    }

    public int getSize() {
        return arPratele.size();
    }

    /**
     * Vrací veškeré aktivní nicky
     *
     * @return nicky
     */
    public ArrayList<String> arNick() {
        ArrayList<String> retVal = new ArrayList<>();
        for (Osoba p : arPratele) {
            retVal.add(p.getNick());
        }
        return retVal;
    }


    /**
     * Vrátí přítele dle pořadí v poli
     *
     * @param i pozice
     * @return Osoba
     */
    @Nullable
    public Osoba vratPritele(int i) {
        if (i < 0) return null;
        if (i < arPratele.size()) {
            return arPratele.get(i);
        }
        return null;
    }

    /**
     * Vrátí přítele dle id
     *
     * @param pritelId id přítele
     * @return
     */
    @Nullable
    public Osoba vratPriteleId(@Nullable String pritelId) {
        if (TextUtils.isEmpty(pritelId)) return null;
        for (Osoba o : arPratele) {
            if (o.getUserId().equals(pritelId)) return o;
        }
        return null;
    }


    void setParametres(Osoba osoba) {
        String userId = osoba.getUserId();
        if (TextUtils.isEmpty(userId)) return;
        zapisDoBufferuPratele(osoba);
        nactiOffline();
    }

    public ArrayList<Osoba> getArPratele() {
        return arPratele;
    }

    @Nullable
    public String getUsersId() {
        String retVal = null;
        int i = 0;
        for (Osoba osoba : arPratele) {
            if (i > 0) retVal += def.AR_GLUE;
            retVal += osoba.getUserId();
            ++i;
        }
        return retVal;
    }
}