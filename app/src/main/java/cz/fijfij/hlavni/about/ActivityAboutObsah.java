package cz.fijfij.hlavni.about;

import android.app.Activity;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.fijfij.R;
import cz.fijfij.hlavni.SettingOp;
import cz.fijfij.hlavni.def;

/**
 * Created by jiri.kursky on 02.04.2017
 */

public class ActivityAboutObsah {
    /**
     * An array of sample (dummy) items.
     */
    public static final List<DummyItem> ITEMS = new ArrayList<DummyItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

    private static final int COUNT = 25;


    public ActivityAboutObsah(Activity activity) {
        SettingOp settingOp = new SettingOp(activity);
        addItem(new DummyItem(activity.getString(R.string.version_name), def.getVersionName(activity), null));
        addItem(new DummyItem(activity.getString(R.string.your_id), def.userId(), null));
        addItem(new DummyItem(activity.getString(R.string.your_nick), settingOp.getString(def.PREF_NICK), null));
        addItem(new DummyItem(activity.getString(R.string.android_id), def.android_id, null));
        addItem(new DummyItem(activity.getString(R.string.our_email), activity.getString(R.string.email_app), null));
    }


    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static DummyItem createDummyItem(int position) {
        return new DummyItem(String.valueOf(position), "Item " + position, makeDetails(position));
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public final String id;
        public final String content;
        public final String details;

        public DummyItem(String id, String content, @Nullable String details) {
            this.id = id;
            this.content = content;
            this.details = details;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
