package cz.fijfij.hlavni.about;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import cz.fijfij.R;

/**
 * Created by jiri.kursky on 02.04.2017
 */

public class ActivityAbout extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_item_list);
        RecyclerView recyclerView=(RecyclerView) findViewById(R.id.listAbout);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        new ActivityAboutObsah(this);
        recyclerView.setAdapter(new AboutRecyclerViewAdapter(ActivityAboutObsah.ITEMS));
    }
}
