package cz.fijfij.hlavni;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import cz.fijfij.R;

/**
 * Created by jiri.kursky on 07.11.2016
 */

public class CoordinatesItem {
    private double latitude;
    private double longitude;
    private String cas = "";
    private String casPrepsani;
    private String smsID;
    private String idZdroj;
    private boolean nezobrazovat;
    private int typZjisteniLokace = 0;


    CoordinatesItem(Cursor res) {
        if (res == null) {
            setCas("");
            return;
        }
        setCas(res.getString(res.getColumnIndex(def.CL_CAS)));
        setCasPrepsani(res.getString(res.getColumnIndex(def.CL_CAS_PREPSANI)));
        setLongitude(res.getString(res.getColumnIndex(def.PREF_LONGITUDE)));
        setLatitude(res.getString(res.getColumnIndex(def.PREF_LATITUDE)));
        setSmsID(res.getString(res.getColumnIndex(def.CL_SMS_ID)));
        setIdZdroj(res.getString(res.getColumnIndex(def.CL_ID_ZDROJ)));
        setTypZjisteniLokace(res.getInt(res.getColumnIndex(def.PREF_TYP_ZJISTENI_LOKACE)));
        setNezobrazovat(res.getInt(res.getColumnIndex(def.CL_NEZOBRAZOVAT)) != 0);
    }

    boolean coordinatesOK() {
        if (nezobrazovat) return false;
        if (TextUtils.isEmpty(cas)) return false;
        return true;
    }

    private void setLongitude(String longitude) {
        if (TextUtils.isEmpty(longitude)) longitude = "0.0";
        setLongitude(Double.parseDouble(longitude));
    }

    private void setLatitude(String latitude) {
        if (TextUtils.isEmpty(latitude)) latitude = "0.0";
        setLatitude(Double.parseDouble(latitude));
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @NonNull
    public String getCas() {
        if (TextUtils.isEmpty(cas)) cas = "";
        return cas;
    }

    void setCas(String cas) {
        this.cas = cas;
    }

    public String getCasPrepsani() {
        return casPrepsani;
    }

    void setCasPrepsani(String casPrepsani) {
        if (TextUtils.isEmpty(casPrepsani)) {
            this.casPrepsani = "";
        } else {
            this.casPrepsani = casPrepsani;
        }
    }

    void setSmsID(String smsID) {
        this.smsID = smsID;
    }

    String getSmsID() {
        return smsID;
    }

    void setIdZdroj(String idZdroj) {
        this.idZdroj = idZdroj;
    }

    String getIdZdroj() {
        return idZdroj;
    }

    void setNezobrazovat(boolean nezobrazovat) {
        this.nezobrazovat = nezobrazovat;
    }

    boolean souradniceOK() {
        return Math.round(getLongitude()) != 0;
    }

    void setTypZjisteniLokace(int typZjisteniLokace) {
        this.typZjisteniLokace = typZjisteniLokace;
    }


    public String sGetTypZjisteniLokace(Context context) {
        switch (typZjisteniLokace) {
            case 0:
            case def.LOKACE_PROVIDER_ZADNY:
                return context.getString(R.string.lokace_vypnuto);
            case def.LOKACE_PROVIDER_NETWORK:
                return context.getString(R.string.lokace_network);
            case def.LOKACE_PROVIDER_GPS:
                return context.getString(R.string.lokace_gps);
        }
        return context.getString(R.string.lokace_vypnuto);
    }
}
