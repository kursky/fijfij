/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.WindowManager;

import java.util.ArrayList;

import cz.fijfij.R;
import cz.fijfij.hlavni.nastaveni.ParametrItem;

/**
 * Created by jiri.kursky on 03.12.2016.
 * Kombinace konstant zadefinovaných v def.java
 */

public class ParametrOp {
    public static final int PARS_ADMIN = 10;
    public static final int PARS_RIDICI = 20;
    public static final int PARS_CLEN = 30;
    static final int PARS_CLEN_VIDITELNE = 40;
    static final int PARS_RIDICI_VIDITELNE = 50;
    static final int PARS_DB = 60;

    private ArrayList<ParametrItem> parametrItems;
    private ArrayList<ParametrItem> parametrItemsClen;
    private ArrayList<ParametrItem> parametrItemsRidici;
    private ArrayList<ParametrItem> parametrItemsClenViditelne;
    private ArrayList<ParametrItem> parametrItemsRidiciViditelne;


    private ArrayList<ParametrItem> dbItems;            // Používané v databázích

    private ArrayList<String> allKeys;
    private ArrayList<String> allKeysClen;
    private ArrayList<String> allKeysRidici;
    private ArrayList<String> allKeysClenViditelne;
    private ArrayList<String> allKeysRidiciViditelne;

    /**
     * Zde je hlavni definice jednotlivých parametru
     */
    public ParametrOp() {
        allKeys = new ArrayList<>();
        allKeysClen = new ArrayList<>();
        allKeysRidici = new ArrayList<>();
        allKeysClenViditelne = new ArrayList<>(); // Všechny
        allKeysRidiciViditelne = new ArrayList<>(); // Všechny
        dbItems = new ArrayList<>(); // Databázové

        parametrItems = new ArrayList<>();
        parametrItemsClen = new ArrayList<>();
        parametrItemsRidici = new ArrayList<>();
        parametrItemsClenViditelne = new ArrayList<>();
        parametrItemsRidiciViditelne = new ArrayList<>();

        setParam(def.PREF_USER_ID, def.RET_USER_ID, R.string.par_user_id, ParametrItem.PAR_STRING, "", -1).setZakazEditace().setNezobrazovat();

        setParam(def.PREF_NICK, def.RET_NICK, R.string.nickname, ParametrItem.PAR_STRING, "", 20);
        setParam(def.PREF_PHONE_NUMBER, def.RET_PHONE_NUMBER, R.string.par_phone_number, ParametrItem.PAR_STRING, "", 13).setInputType(InputType.TYPE_CLASS_PHONE);
        setParam(def.PREF_EMAIL, def.RET_EMAIL, R.string.vas_email, ParametrItem.PAR_STRING, "", 40).setRidiciPouze();
        setParam(def.PREF_PIN, def.RET_PIN, R.string.par_pin, ParametrItem.PAR_STRING, "", 4).setInputType(InputType.TYPE_CLASS_NUMBER).setPouzeClen();

        setParam(def.PREF_POSLEDNI_CAS, def.RET_POSLEDNI_CAS, R.string.par_posledni_cas, ParametrItem.PAR_STRING, "", 20).setNezobrazovat();
        setParam(def.PREF_POSLEDNI_LONGITUDE, def.RET_POSLEDNI_LONGITUDE, R.string.par_posledni_long, ParametrItem.PAR_STRING, "", 20).setNezobrazovat();
        setParam(def.PREF_POSLEDNI_LATITUDE, def.RET_POSLEDNI_LATITUDE, R.string.par_posledni_lat, ParametrItem.PAR_STRING, "", 20).setNezobrazovat();
        //setParam(def.PREF_VERSION_CODE, def.RET_VERSION_CODE, R.string.par_version_code, ParametrItem.PAR_STRING, "", 5).setNezobrazovat();

        setParam(def.PREF_SMS_POVOLENO, def.RET_SMS_POVOLENO, R.string.par_sms_povoleno, ParametrItem.PAR_BOOLEAN, true);
        setParam(def.PREF_POUZE_SMS, def.RET_POUZE_SMS, R.string.par_pouze_sms, ParametrItem.PAR_BOOLEAN, false).setNezobrazovat();
        setParam(def.PREF_JE_RIDICI, def.RET_JE_RIDICI, R.string.par_ridici, ParametrItem.PAR_BOOLEAN, false).setNezobrazovat();

        setParam(def.PREF_SPOJOVACI_KLIC, def.RET_KLIC, R.string.par_klic, ParametrItem.PAR_STRING, "", 10).setNezobrazovat();
        setParam(def.PREF_SPOJENI, def.RET_SPOJENI, R.string.par_spojeni, ParametrItem.PAR_STRING, "", 30).setNezobrazovat();

        setParam(def.PREF_BATTERY_PERCENT, def.RET_BATTERY_PERCENT, R.string.baterie, ParametrItem.PAR_STRING, "", 10).setNezobrazovat().setPouzeClen();

        setParam(def.PREF_POVOLENO_WIFI, def.RET_POVOLENO_WIFI, R.string.par_povoleno_wifi, ParametrItem.PAR_BOOLEAN, true);
        setParam(def.PREF_DATA_POVOLENA, def.RET_DATA_POVOLENA, R.string.par_data_povolena, ParametrItem.PAR_BOOLEAN, true);
        setParam(def.PREF_NO_RESTART_SERVICE, def.RET_NO_RESTART_SERVICE, R.string.nespoustet_pri_startu, ParametrItem.PAR_BOOLEAN, false);
        setParam(def.PREF_NEZOBRAZOVAT_BEH, null, R.string.nezobrazovat_beh, ParametrItem.PAR_BOOLEAN, false).setNezobrazovat();

        setDbItem(def.PREF_SPOJENI, def.RET_SPOJENI);

        for (ParametrItem pi : parametrItems) {
            allKeys.add(pi.getKlic());
            if (!pi.getPouzeClen()) {
                parametrItemsRidici.add(pi);
                if (!pi.getNezobrazovat()) parametrItemsRidiciViditelne.add(pi);
            }
            if (!pi.getPouzeRidici()) {
                parametrItemsClen.add(pi);
                if (!pi.getNezobrazovat()) parametrItemsClenViditelne.add(pi);
            }
        }


        for (ParametrItem pi : parametrItemsClen) {
            allKeysClen.add(pi.getKlic());
        }

        for (ParametrItem pi : parametrItemsClenViditelne) {
            allKeysClenViditelne.add(pi.getKlic());
        }

        for (ParametrItem pi : parametrItemsRidici) {
            allKeysRidici.add(pi.getKlic());
        }
        for (ParametrItem pi : parametrItemsRidiciViditelne) {
            allKeysRidiciViditelne.add(pi.getKlic());
        }
    }

    /**
     * Nastaveni parametru
     *
     * @param key              klic do preference
     * @param dbKey            klic databázi
     * @param nazev            nazev zobrazovány v preferencích
     * @param typ              BOOLEAN|STRING
     * @param pocatecniHodnota defaultní hodnota - neplatí pro řídící
     * @return ParametrItem
     */
    private ParametrItem setParam(String key, String dbKey, int nazev, int typ, String pocatecniHodnota, int maxDelka) {
        ParametrItem pi;
        parametrItems.add(pi = new ParametrItem(key, dbKey, nazev, typ, pocatecniHodnota, maxDelka));
        dbItems.add(pi);
        return pi;
    }

    private ParametrItem setDbItem(String key, String retKlic) {
        ParametrItem pi;
        dbItems.add(pi = new ParametrItem(key, retKlic));
        return pi;
    }


    private ParametrItem setParam(String key, @Nullable String dbKey, int nazev, int typ, boolean pocatecniHodnota) {
        ParametrItem pi;
        parametrItems.add(pi = new ParametrItem(key, dbKey, nazev, typ, pocatecniHodnota));
        dbItems.add(pi);
        return pi;
    }


    @Nullable
    ParametrItem getParametrItem(String key, int typParametru) {
        ArrayList<ParametrItem> zdroj = getParametrItems(typParametru);
        for (ParametrItem pi : zdroj) {
            if (pi.getKlic().equals(key)) return pi;
        }
        return null;
    }

    @Nullable
    ParametrItem getParametrItem(int pozice, int typParametru) {
        ArrayList<ParametrItem> zdroj = getParametrItems(typParametru);
        if (pozice >= zdroj.size()) return null;
        return zdroj.get(pozice);
    }

    /**
     * Vraci typ parametru
     *
     * @param key klic parametru
     * @return int
     */
    public int getParameterType(String key) {
        ParametrItem pi = getParametrItem(key, PARS_ADMIN);
        if (pi == null) return ParametrItem.PAR_STRING;
        return pi.getTyp();
    }


    /**
     * Vrátí AlertDialog zadefinuje dlgEditText
     */
    static AlertDialog getAlertDialog(Context context, ParametrItem parametrItem) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog dialog;
        builder.setTitle(R.string.nastaveni_hodnoty);
        builder.setCancelable(true);
        builder.setMessage(parametrItem.getNazev());
        builder.setView(parametrItem.getDlgEditText());
        builder.setNegativeButton(R.string.zpet, null);
        builder.setPositiveButton(R.string.dlgOK, null);
        dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();
        return dialog;
    }


    /**
     * Nastavi dle vraceneho dotazu JSON parametry osoby
     *
     * @param osoba cilova osoba
     */
    void nastavOsobu(@NonNull Osoba osoba, @NonNull Bundle result) {
        for (ParametrItem pi : parametrItems) {
            switch (pi.getTyp()) {
                case ParametrItem.PAR_STRING:
                    osoba.setKeyValue(pi.getKlic(), result.getString(pi.getRetKlic(), pi.getDefaultString()));
                    break;

                case ParametrItem.PAR_BOOLEAN:
                    osoba.setKeyValue(pi.getKlic(), result.getInt(pi.getRetKlic(), pi.getDefaultBoolInt()));
                    break;
            }
        }
    }

    ArrayList<ParametrItem> getParametrItems(int typParametru) {
        switch (typParametru) {
            case PARS_ADMIN:
                return parametrItems;
            case PARS_DB:
                return dbItems;
            case PARS_CLEN:
                return parametrItemsClen;
            case PARS_CLEN_VIDITELNE:
                return parametrItemsClenViditelne;
            case PARS_RIDICI:
                return parametrItemsRidici;
            case PARS_RIDICI_VIDITELNE:
                return parametrItemsRidiciViditelne;
        }
        return null;
    }

    ArrayList<String> getAllKeys(int typParametru) {
        switch (typParametru) {
            case PARS_ADMIN:
                return allKeys;
            case PARS_CLEN:
                return allKeysClen;
            case PARS_CLEN_VIDITELNE:
                return allKeysClenViditelne;
            case PARS_RIDICI:
                return allKeysRidici;
            case PARS_RIDICI_VIDITELNE:
                return allKeysRidiciViditelne;
        }
        return null;
    }
}
