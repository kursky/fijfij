/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import cz.fijfij.hlavni.service.FijService;

/**
 * Created by jiri.kursky on 20.12.2016
 * zamezuje zastaveni service
 */

public class RestartServiceReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context.getApplicationContext(), FijService.class));

    }
}
