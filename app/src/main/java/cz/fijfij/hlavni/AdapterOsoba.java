/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import cz.fijfij.R;

/**
 * Created by jiri.kursky on 07.11.2016.
 * Napojeny adapter na ActivityOsoba
 */

public class AdapterOsoba extends ArrayAdapter<String> {
    private final CoordinatesOp coordinatesOp;
    private Activity activity;


    AdapterOsoba(Activity activity, CoordinatesOp coordinatesOp) {
        super(activity, R.layout.list_single_osoba, coordinatesOp.getArVzdalenost());
        this.coordinatesOp = coordinatesOp;
        this.activity = activity;
    }

    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_single_osoba, null, true);

        CoordinatesItem ci = coordinatesOp.getCoordinatesItemAktualni(position);
        if (ci == null) return rowView;

        TextView txtPoradi = (TextView) rowView.findViewById(R.id.txtPoradi);
        Integer pozice = position + 1;
        txtPoradi.setText(pozice.toString());

        TextView txtCas = (TextView) rowView.findViewById(R.id.txtCas);
        String cas = def.verbalDateTime(getContext(), ci.getCas());
        txtCas.setText(cas);

        TextView txtCasPrepsani = (TextView) rowView.findViewById(R.id.txtCasPrepsani);
        String casPrepsani = def.verbalDateTime(getContext(), ci.getCasPrepsani());
        if (cas.equals(casPrepsani)) casPrepsani = "";
        casPrepsani = def.zobrazNulovyCas(casPrepsani);
        txtCasPrepsani.setText(casPrepsani);

        TextView txtVzdalenost = (TextView) rowView.findViewById(R.id.txtVzdalenost);
        String vzdalenost = coordinatesOp.getVzdalenost(position);
        txtVzdalenost.setText(vzdalenost);

        ImageView imgSMS = (ImageView) rowView.findViewById(R.id.imgSMS);
        imgSMS.setVisibility(TextUtils.isEmpty(coordinatesOp.getIdZdroj(position)) ? View.VISIBLE : View.INVISIBLE);

        return rowView;
    }
}

