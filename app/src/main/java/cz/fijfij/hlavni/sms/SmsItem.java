package cz.fijfij.hlavni.sms;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import cz.fijfij.hlavni.Osoba;
import cz.fijfij.hlavni.PrateleOp;
import cz.fijfij.hlavni.zapouzdreno.SmsControl;
import cz.fijfij.hlavni.def;

/**
 * Created by jiri.kursky on 5.8.2016
 *
 */
public class SmsItem {
    static final int SMS_GENERAL = 10;
    public static final int SMS_VRAT_LOKACI = 20;
    public static final int SMS_ODPOVED_LOKACE = 30;
    static final int SMS_TEST = 40;
    public static final int SMS_POTVRZENI_SMS = 50;
    public static final int SMS_CHCI_SE_SPOJIT = 60;
    public static final int SMS_ME_ANDROID_ID = 70;

    public static final String SMS_HASH = "#infoo#";
    public static final String VRAT_LOKACI = "coor";
    public static final String ODPOVED_LOKACE = "codp";
    public static final String CHCI_SE_SPOJIT= "getm"; // rodic posila zprávu, že se chce spojit
    public static final String ME_ANDROID_ID= "maid"; // odesílá člen rodiny cislo androidu
    public static final String UZIVATEL_ID = "usid";
    public static final String NASTAVENI = "nast";


    public static String[] сolumns = new String[]{"_id", "thread_id", "address", "person", "date", "body"};
    private final Context context;


    private long id;
    private String smsID; // Identifikátor sms
    private String body;
    private String datumSMS;
    private String datumZpracovani;
    private int zadost;
    private String longitude;
    private String latitude;
    private Osoba osoba;
    DateFormat formatter;
    private String cislo;
    private int typZjisteniLokace;

    public SmsItem(Context context) {
        formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.context=context;
        clear();
    }

    public void clear() {
        id = 0;
        zadost = SMS_GENERAL;
        longitude = "";
        latitude = "";
        smsID = "";
        osoba = null;
        datumSMS = "";
        datumZpracovani = "";
    }

    public void setFromBuffer(Osoba osoba, Cursor res) {
        this.osoba = osoba;
        smsID = res.getString(res.getColumnIndex("smsID"));
    }


    public int parse(Cursor cursor, @Nullable PrateleOp prateleOp) {
        id = cursor.getLong(cursor.getColumnIndex(сolumns[0]));
        String cislo = cursor.getString(cursor.getColumnIndex(сolumns[2]));
        body = cursor.getString(cursor.getColumnIndex(сolumns[5]));

        String dateMiliseconds = cursor.getString(cursor.getColumnIndex(сolumns[4]));
        Long timestamp = Long.parseLong(dateMiliseconds);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        setDatumSMS(formatter.format(calendar.getTime()));
        setDatumZpracovani(def.todaySQL());
        setCislo(cislo);

        // Zde se definuje prijemce a zaroven ten, na koho bude odeslana odpoved
        if (prateleOp!=null) {
            osoba = prateleOp.vratDleTelefonu(cislo);
            if (osoba == null) {
                if (def.vlastniCislo(cislo)) {
                    return zadost;
                }
            }
        }

        return parseMessage(body, this);
    }

    public static int parseMessage(String message, SmsItem smsItem) {
        smsItem.setZadost(SMS_GENERAL);
        int delka = SMS_HASH.length();
        if (message.length() < delka) return SMS_GENERAL;
        if (!message.substring(0, delka).equals(SMS_HASH)) return SMS_GENERAL;
        String zbytek = message.substring(delka);
        int pozice = zbytek.indexOf("#");
        if (pozice < 0) return SmsItem.SMS_GENERAL;
        String akce = zbytek.substring(0, pozice);
        String smsID;
        if (akce.equals(VRAT_LOKACI)) {
            smsID = zbytek.substring(pozice + 1);
            if (TextUtils.isEmpty(smsID)) return SMS_GENERAL;
            smsItem.setSmsID(smsID);
            return smsItem.setZadost(SMS_VRAT_LOKACI);
        }
        if (akce.equals(CHCI_SE_SPOJIT)) {
            smsID = zbytek.substring(pozice + 1);
            if (TextUtils.isEmpty(smsID)) return SMS_GENERAL;
            smsItem.setSmsID(smsID);
            return smsItem.setZadost(SMS_CHCI_SE_SPOJIT);
        }
        if (akce.equals(ME_ANDROID_ID)) {
            smsID = zbytek.substring(pozice + 1);
            if (TextUtils.isEmpty(smsID)) return SMS_GENERAL;
            smsItem.setSmsID(smsID);
            return smsItem.setZadost(SMS_ME_ANDROID_ID);
        }
        if (akce.equals(ODPOVED_LOKACE)) {
            zbytek = zbytek.substring(pozice + 1);
            pozice = zbytek.indexOf("#");
            if (pozice < 0) return SMS_GENERAL;
            smsID = zbytek.substring(0, pozice);
            smsItem.setSmsID(smsID);

            String kum = zbytek.substring(pozice + 1);
            String[] parts = kum.split("\\:"); // escape
            smsItem.setLatitude(parts[0]);
            smsItem.setLongitude(parts[1]);
            if (parts.length>2) {
                smsItem.setTypZjisteniLokace(parts[2]); // V předchozích verzích < 38 se zasílala pouze lokace
            }
            else {
                smsItem.setTypZjisteniLokace("0");
            }
            return smsItem.setZadost(SMS_ODPOVED_LOKACE);
        }
        if (akce.equals(UZIVATEL_ID)) {
            zbytek = zbytek.substring(pozice + 1);
            pozice = zbytek.indexOf("#");
            if (pozice < 0) return SMS_GENERAL;
            smsID = zbytek.substring(0, pozice);
            smsItem.setSmsID(smsID);
            String kum = zbytek.substring(pozice + 1);
            smsItem.setOsobaUserId(kum);
            return smsItem.setZadost(SMS_POTVRZENI_SMS);
        }
        if (akce.equals(SmsControl.TEST)) return smsItem.setZadost(SMS_TEST);
        return SMS_GENERAL;
    }

    public void setOsobaUserId(String userId) {
        if (osoba == null) osoba = new Osoba();
        osoba.setKeyValue(def.PREF_USER_ID, userId);
    }

    public String getCislo() {
        if (osoba==null) return "";
        return osoba.getPhoneNumber();
    }

    public int getZadost() {
        return zadost;
    }

    public String getUserId() {
        return osoba.getUserId();
    }


    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }


    public Osoba getOsoba() {
        return osoba;
    }

    public long getId() {
        return id;
    }

    public String getBody() {
        return body;
    }

    public String getSmsID() {
        return smsID;
    }

    public int setZadost(int zadost) {
        this.zadost = zadost;
        return zadost;
    }

    public void setSmsID(String smsID) {
        this.smsID = smsID;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setOsoba(Osoba osoba) {
        this.osoba = osoba;
    }


    public void setDatumSMS(String datumSMS) {
        this.datumSMS = datumSMS;
    }


    void setDatumZpracovani(String datumZpracovani) {
        this.datumZpracovani = datumZpracovani;
    }

    void setDatumZpracovani(Long datumZpracovani) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(datumZpracovani);
        this.datumZpracovani = formatter.format(calendar.getTime());
    }

    public String getDatumSMS() {
        return datumSMS;
    }

    public void setCislo(String cislo) {
        if (osoba == null) osoba = new Osoba();
        osoba.setPhoneNumber(cislo);
    }

    public void setParams(Osoba osoba, long timestampMillis) {
        setOsoba(osoba);
        setDatumZpracovani(timestampMillis);
        setDatumSMS(def.todaySQL());
    }

    void setTypZjisteniLokace(String typLokace) {
        this.typZjisteniLokace = def.parseInt(typLokace);
    }

    public int getTypZjisteniLokace() {
        return typZjisteniLokace;
    }
}