package cz.fijfij.hlavni;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import cz.fijfij.hlavni.nastaveni.ParametrItem;

/**
 * Created by jiri.kursky on 5.8.2016
 * Zadefinované údaje o uživateli a přátelích
 */
public class Osoba {
    private Bundle bundle = new Bundle();

    public Osoba(Osoba zdroj) {
        bundle.putAll(zdroj.getBundle());
    }

    public Osoba() {
    }

    public Osoba(Bundle bundle) {
        if (bundle != null) this.bundle.putAll(bundle);
    }

    public Bundle getBundle() {
        return bundle;
    }


    /**
     * V bundle musi být vše
     */
    void nastavParametryBundleAll(Bundle bundle, int typParametru) {
        if (def.parametrOp == null) def.parametrOp = new ParametrOp();
        for (ParametrItem pi : def.parametrOp.getParametrItems(typParametru)) {
            String dbKlic = pi.getRetKlic();
            switch (pi.getTyp()) {
                case ParametrItem.PAR_BOOLEAN:
                    setKeyValue(pi.getKlic(), def.parseInt(bundle.getString(dbKlic, pi.getDefaultString())));
                    break;
                case ParametrItem.PAR_STRING:
                    setKeyValue(pi.getKlic(), bundle.getString(dbKlic, pi.getDefaultString()));
                    break;
            }
        }
    }


    @NonNull
    public boolean spojeniNepotvrzeno() {
        return def.nulovyCas(getString(def.PREF_SPOJENI));
    }

    public boolean spojeniSmsNepotvrzeno() {
        return !TextUtils.isEmpty(getString(def.PREF_SPOJENI)) && getString(def.PREF_SPOJOVACI_KLIC).equals("SMS");
    }

    public String getKeyValue(String key) {
        String retVal = bundle.getString(key);
        if (TextUtils.isEmpty(retVal)) retVal = "";
        return retVal;
    }

    int getKeyValueInt(String key) {
        return bundle.getInt(key);
    }

    public boolean getKeyValueBool(String key) {
        int i = bundle.getInt(key);
        return i != 0;
    }

    public void setKeyValue(String key, String hodnota) {
        bundle.putString(key, hodnota);
    }

    void setKeyValue(String key, int hodnota) {
        bundle.putInt(key, hodnota);
    }

    void setKeyValue(String key, boolean hodnota) {
        bundle.putInt(key, hodnota ? 1 : 0);
    }


    /**
     * Používá se pro nastavení přátel
     *
     * @param res vstupní res z databaze buffer
     */
    public Osoba(Cursor res) {
        setCursorKey(res, def.PREF_USER_ID);
        setCursorKey(res, def.PREF_NICK);
        setCursorKey(res, def.PREF_PIN);
        setPhoneNumber(def.phoneNumberStandard(res.getString(res.getColumnIndex(def.PREF_PHONE_NUMBER))));
        setCursorKeyInt(res, def.PREF_POUZE_SMS);
        setCursorKeyInt(res, def.PREF_SMS_POVOLENO);
        setCursorKeyInt(res, def.PREF_JE_RIDICI);
        setCursorKeyInt(res, def.PREF_POVOLENO_WIFI);
        setCursorKeyInt(res, def.PREF_DATA_POVOLENA);
        setCursorKey(res, def.PREF_POSLEDNI_CAS);
        setCursorKey(res, def.PREF_POSLEDNI_LATITUDE);
        setCursorKey(res, def.PREF_POSLEDNI_LONGITUDE);
        setCursorKey(res, def.PREF_SPOJENI);
    }

    /**
     * Převádí do bundle osoby z kurzoru
     */
    private void setCursorKey(Cursor res, String key) {
        String hodnota = res.getString(res.getColumnIndex(key));
        bundle.putString(key, hodnota);
    }

    private void setCursorKeyInt(Cursor res, String key) {
        int hodnota = res.getInt(res.getColumnIndex(key));
        bundle.putInt(key, hodnota);
    }


    @NonNull
    public String getPhoneNumber() {
        return def.phoneNumberStandard(bundle.getString(def.PREF_PHONE_NUMBER, ""));
    }

    @NonNull
    public String getUserId() {
        return bundle.getString(def.PREF_USER_ID, "");
    }

    public String getNick() {
        return bundle.getString(def.PREF_NICK);
    }

    String getNickUri() {
        return Uri.encode((getNick()));
    }

    String getPosledniCas() {
        return bundle.getString(def.PREF_POSLEDNI_CAS);
    }

    public void setPhoneNumber(String hodnota) {
        bundle.putString(def.PREF_PHONE_NUMBER, hodnota);
    }

    void setNick(String hodnota) {
        hodnota = def.bezHacku(hodnota.trim());
        bundle.putString(def.PREF_NICK, hodnota);
    }

    void setUserId(String userId) {
        bundle.putString(def.PREF_USER_ID, userId);
    }

    /**
     * Nastaví defaultní hodnoty pro definování přítele
     */
    void setDefaultPritel() {
        setKeyValue(def.PREF_DATA_POVOLENA, true);
        setKeyValue(def.PREF_POVOLENO_WIFI, true);
        setKeyValue(def.PREF_DATA_POVOLENA, true);
        setKeyValue(def.PREF_SMS_POVOLENO, true);
        setKeyValue(def.PREF_PIN, "7777");
    }

    void setPIN(String hodnota) {
        bundle.putString(def.PREF_PIN, hodnota);
    }

    String getPIN() {
        return bundle.getString(def.PREF_PIN);
    }

    /**
     * Přidá(!) hodnoty z bundle
     *
     * @param data zdroj
     */
    public void putBundle(Bundle data) {
        bundle.putAll(data);
    }

    @NonNull
    public String getString(String klic) {
        return bundle.getString(klic, "");
    }

    int getBaterieInt() {
        String s = getString(def.PREF_BATTERY_PERCENT);
        if (TextUtils.isEmpty(s)) return 0;
        return def.parseInt(s);
    }

    String getBaterie() {
        String retVal = getString(def.PREF_BATTERY_PERCENT);
        if (TextUtils.isEmpty(retVal)) return retVal;
        return retVal + "%";
    }
}
