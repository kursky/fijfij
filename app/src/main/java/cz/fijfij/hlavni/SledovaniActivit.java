/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

/**
 * Created by jiri.kursky on 04.12.2016
 */

public class SledovaniActivit {

    public static final boolean START_SERVICE = true;

    /**
     * Nutno pouzivat - dava moznost sledovat zabindovani service
     *
     * @param context klasicky context
     * @param delay   prodleva pred spustenim v ms
     * @param cls     otevirana aktivita
     */
    public static void startActivity(Context context, int delay, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        openActivity(context, intent, delay);

    }

    static void startActivity(final Context context, int delay, Class<?> cls, int flags) {
        final Intent intent = new Intent(context, cls);
        intent.setFlags(intent.getFlags() | flags);
        openActivity(context, intent, delay);
    }

    public static void startActivity(final Context context, int delay, Class<?> cls, int flags, boolean startService) {
        final Intent intent = new Intent(context, cls);
        intent.setFlags(intent.getFlags() | flags);
        openActivity(context, intent, delay);
    }

    private static void openActivity(final Context context, final Intent intent, int delay) {
        if (delay == 0) {
            context.startActivity(intent);
            return;
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                context.startActivity(intent);
            }
        }, delay);
    }
}
