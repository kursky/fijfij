/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.service.FijService;

/**
 * Created by jiri.kursky on 5.8.2016.
 * Rozšíření aktivit
 */
abstract public class BaseActivity extends AppCompatActivity {
    static final String TAG = FijService.class.getSimpleName();
    protected Intent intent;
    protected int contentView = 0;    // definuje layout
    protected Toolbar toolbar;
    protected ActionBar actionBar = null;
    private Activity activity;
    private boolean heteroOrientation = false;
    CekaniDef cekani;

    public BaseActivity(int contentView, boolean heteroOrientation) {
        this.heteroOrientation = heteroOrientation;
        mSetContentView(contentView);
    }

    public BaseActivity(int contentView) {
        this(contentView, true);
    }

    protected void mSetContentView(int contentView) {
        this.contentView = contentView;
    }

    public View findViewByName(String name) {
        return def.findViewByName(this, name);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        if (!heteroOrientation) setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (contentView != 0) setContentView(contentView);
        if (intent == null) intent = getIntent();

        toolbar = (Toolbar) findViewByName("app_toolbar");
        if (toolbar != null) {
            klikNaSipku(toolbar);
        }
        actionBar = getSupportActionBar();
        if (actionBar != null) nastavActionBar();

        nastavInterfaceCekani();
    }

    void nastavInterfaceCekani() {
        cekani = new CekaniDef(this, new CekaniDef.DefCekani() {
            @Override
            public void nastaveno(int zdrojVolani) {
                cekaniNastaveno(zdrojVolani);
            }
        });
        cekani.doNastavCekej(false);
    }


    /**
     * Volano z nastavCekej
     * Predpoklada se, ze tato proceruda je override
     *
     * @param zdrojVolani identifikator, ktery se predal pri nastavCekej
     */
    public void cekaniNastaveno(int zdrojVolani) {
        MyLog.d(TAG, "Nastaveno");
    }

    /**
     * Procedura, ktera se zpozdenim zavola cekaniNastaveno
     *
     * @param zdrojVolani parametr, ktery se pote predava cekaniNastaveno
     */
    public void nastavCekej(final int zdrojVolani) {
        cekani.nastav(zdrojVolani);
    }


    void nastavCekejNeSpinner(final int zdrojVolani) {
        cekani.neSpinner(zdrojVolani);
    }

    public void nastavCekej() {
        nastavCekej(0);
    }

    protected void zrusCekej() {
        cekani.doNastavCekej(false);
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    public void sipkaNavrat(Toolbar toolbar) {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.super.onBackPressed();
            }
        });
    }

    public void klikNaSipku(Toolbar toolbar) {
        sipkaNavrat(toolbar);
    }

    public void nastavActionBar() {
        if (actionBar == null) return;
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        SledovaniActivit.startActivity(this, 10, def.getMainActivity(this));
    }
}
