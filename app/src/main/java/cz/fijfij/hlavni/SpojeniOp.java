/*
 * Copyright (c) 2017 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import org.json.JSONObject;

/**
 * Created by jiri.kursky on 26.01.2017.
 * Class spravující všechna spojení
 */

public class SpojeniOp {

    private final Context context;

    public SpojeniOp(Context context) {
        this.context = context;
    }

    /**
     * Akce na serveru
     * @param pritel
     * @param androidId
     * @param odeslanoServeru
     */
    public void potvrzenyPritel(Osoba pritel, String androidId, final OdeslanoServeru odeslanoServeru) {
        AsyncService asOdeslani = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
                Bundle bundle = def.transferJSON(result);
                if (bundle == null) {
                    // @TODO zde bude nutno osetrit chybu - nebyl-li navracen userId
                    odeslanoServeru.nastalaChyba();
                } else {
                    String userId = bundle.getString(def.RET_USER_ID);
                    if (TextUtils.isEmpty(userId)) {
                        odeslanoServeru.nastalaChyba();
                        // @TODO zde bude nutno osetrit chybu - nebyl-li navracen userId
                    } else {
                        odeslanoServeru.vseOK(bundle);
                    }
                }
            }
        }, context, "Send hash",
                def.EX_ACTION + def.CMD_SMS_HASH + def.EX_ANDROID_ID + androidId + def.EX_PRITELID + pritel.getUserId() +
                        def.EX_PHONE_NUMBER + pritel.getPhoneNumber() + def.EX_ZALOZIL + def.android_id + def.EX_USERID + def.userId());
        asOdeslani.execute();
    }

    public interface OdeslanoServeru {
        void nastalaChyba();

        void vseOK(Bundle bundle);
    }

    /**
     * Odesílá serveru informaci o zadaném kódu
     */
    void kodRodiceZadan(String sKodOdRidiciho, String androidId, final OdeslanoServeru odeslanoServeru) {
        AsyncService asOdeslani = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
                Bundle bundle = def.transferJSON(result);
                if (bundle == null) {
                    // @TODO zde bude nutno ošetřit chybu - nebyl-li navrácen userId
                    odeslanoServeru.nastalaChyba();
                } else {
                    String userId = bundle.getString(def.RET_USER_ID);
                    int iUserId=def.parseInt(userId);
                    if (iUserId==0) {
                        odeslanoServeru.nastalaChyba();
                    } else {
                        odeslanoServeru.vseOK(bundle);
                    }
                }
            }
        }, context, "Send hash", def.EX_ACTION + def.CMD_SEND_HASH + def.EX_ANDROID_ID + androidId + def.EX_SPOJOVACI_KLIC + sKodOdRidiciho);

        asOdeslani.execute();
    }
}
