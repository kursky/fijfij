package cz.fijfij.hlavni;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import cz.fijfij.BuildConfig;
import cz.fijfij.hlavni.base.MyLog;
import cz.fijfij.hlavni.lokace.LatLngItem;
import cz.fijfij.hlavni.nastaveni.ParametrItem;

import org.json.JSONObject;


import static java.lang.Integer.parseInt;

/**
 * Created by jiri.kursky on 5.8.2016
 * Class obsahující operace s parametry
 */
public class SettingOp {
    public static final String S_PROTOCOL_LOG = "protocolLog";
    public static final Double S_HLIDANA_VZDALENOST = 200.00;
    public static final String S_SKONTROLOVAT_VERZI = "kontrolovatVerzi";
    public static final String S_SMS_KONTROLA = "NICK";
    public static final String S_REAGUJE_NA_BROADCAST = "reagujeNaBroadcast";
    public static final String S_REAGUJE_NA_VYMAZ = "reagujeNaVymaz";
    public static final String S_ZRUS_REGISTRACI = "zrusitRegistraci";
    private final SharedPreferences sharedPreferences;
    private Context context;
    private OdeslanyZmeny odeslanyZmeny;
    private String TAG = "SettingOp";

    public SettingOp(Context context) {
        this.context = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }


    public Context getContext() {
        return context;
    }

    /**
     * Standard odesílaný při každém dotazu
     *
     * @param sPar cmd_
     */
    public String getActionString(String sPar) {
        LatLngItem latLngItem = getLangLngItem();
        String action = def.EX_ACTION + sPar;
        action += def.EX_PHONE_NUMBER + getString(def.PREF_PHONE_NUMBER);
        action += def.EX_ANDROID_ID + def.android_id;
        action += def.EX_USERID + getString(def.PREF_USER_ID);
        action += def.EX_NICK + Uri.encode(getString(def.PREF_NICK));
        action += def.EX_SENT_TIME + def.todaySQL();
        action += def.EX_SENT_TIME_SEC + def.getCurrentTimeSec();
        action += def.EX_LATITUDE + latLngItem.sGetLatitude();
        action += def.EX_LONGITUDE + latLngItem.sGetLongitude();
        action += def.EX_CAS_SOURADNIC + latLngItem.getTimeSec();
        action += def.EX_TYP_ZJISTENI_LOKACE + latLngItem.iGetProvider();
        action += def.EX_AOS + android.os.Build.VERSION.SDK_INT;
        action += def.EX_VERSION + BuildConfig.VERSION_CODE;
        action += def.EX_BATTERY_PERCENT + getString(def.PREF_BATTERY_PERCENT);
        return action;
    }


    /**
     * Pozor neovlivňuje android_id
     *
     * @param odeslanyZmeny callback po odeslani změn
     */
    public void sendUserParametres(@Nullable Osoba osoba, boolean vnuceno, @Nullable final OdeslanyZmeny odeslanyZmeny) {
        if (osoba == null) return;
        String action = def.EX_ACTION + def.CMD_UPDATE_USER;
        action += def.EX_USERID + osoba.getUserId();
        action += def.EX_NICK + osoba.getNickUri();
        action += def.EX_PIN + osoba.getPIN();
        action += def.EX_EMAIL + osoba.getKeyValue(def.PREF_EMAIL);
        action += def.EX_SENT_TIME + def.todaySQL();
        action += def.EX_PHONE_NUMBER + osoba.getPhoneNumber();
        action += def.EX_SMS_POVOLENO + osoba.getKeyValueInt(def.PREF_SMS_POVOLENO);
        action += def.EX_POUZE_SMS + osoba.getKeyValueInt(def.PREF_POUZE_SMS);
        action += def.EX_JE_RIDICI + osoba.getKeyValueInt(def.PREF_JE_RIDICI);
        action += def.EX_POVOLENO_WIFI + osoba.getKeyValueInt(def.PREF_POVOLENO_WIFI);
        action += def.EX_DATA_POVOLENA + osoba.getKeyValueInt(def.PREF_DATA_POVOLENA);
        action += def.EX_OSTATNI_NASTAVENI + osoba.getKeyValueInt(def.PREF_OSTATNI_NASTAVENI);
        action += "&vnuceno=" + (vnuceno ? 1 : 0);
        action += def.exTimeStamp();
        if (odeslanyZmeny != null) odeslanyZmeny.onStart();
        AsyncService asService = new AsyncService(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
                if (odeslanyZmeny != null) odeslanyZmeny.onFinished();
            }
        }, context, "update_uzivatel", action);
        asService.execute();
    }

    public void setLatLng(LatLngItem latLngItem) {
        setDouble(def.PREF_LONGITUDE, latLngItem.getLongitude());
        setDouble(def.PREF_LATITUDE, latLngItem.getLatitude());
        putInt(def.PREF_RYCHLOST, latLngItem.mGetSpeed());
        putLong(def.PREF_CAS_SOURADNIC, latLngItem.getTime());
        putInt(def.PREF_TYP_ZJISTENI_LOKACE, latLngItem.iGetProvider());
    }

    public LatLngItem getLangLngItem() {
        LatLngItem latLngItem = new LatLngItem(getDouble(def.PREF_LATITUDE), getDouble(def.PREF_LONGITUDE), getInt(def.PREF_TYP_ZJISTENI_LOKACE));
        latLngItem.setTime(getLong(def.PREF_CAS_SOURADNIC));
        return latLngItem;
    }

    public int getInt(String nazev) {
        return sharedPreferences.getInt(nazev, 0);
    }

    interface OdeslanyZmeny {
        void onStart();

        void onFinished();
    }

    /**
     * Nastaví preference osoby do uloženého nastaveni včetně časového razítka
     *
     * @param osoba
     */
    public void nastavUzivateleDoPreference(@NonNull Osoba osoba, int typParametru) {
        for (String key : def.parametrOp.getAllKeys(typParametru)) {
            int typ = def.parametrOp.getParameterType(key);
            if (typ == ParametrItem.PAR_BOOLEAN) {
                putBoolean(key, osoba.getKeyValueBool(key));
            } else {
                putString(key, osoba.getKeyValue(key));
            }
        }
        nastavRazitko();
    }

    /**
     * Nastavení dnešního časového razítka parametrů do parametrů - míněno, parametry jsou na serveru a na lokálním počítači
     */
    public void nastavRazitko() {
        nastavRazitko(def.todaySQL());
    }

    /**
     * Nastavení časového razítka do parametrů
     * @param cas definovaný čas
     */
    void nastavRazitko(String cas) {
        putString(def.PREF_SETUP_TIMESTAMP, cas);
    }

    /**
     * Z ulozeneho nastaveni nastavi osobu, za defaultni hodnoty jsou brany ty, co jsou v osoba jiz zadefinovany
     *
     * @param osoba
     */
    public void uzivatelZNastaveni(@NonNull Osoba osoba, int typParametru) {
        for (String key : def.parametrOp.getAllKeys(typParametru)) {
            int typ = def.parametrOp.getParameterType(key);
            if (typ == ParametrItem.PAR_BOOLEAN) {
                osoba.setKeyValue(key, getBoolean(key, osoba.getKeyValueBool(key)));
            } else {
                osoba.setKeyValue(key, getString(key, osoba.getKeyValue(key)));
            }
        }
    }

    public void putBoolean(String klic, boolean hodnota) {
        try {
            sharedPreferences.edit().putBoolean(klic, hodnota).apply();
        } catch (Exception e) {
            MyLog.e(TAG, e);
        }
    }

    /**
     * Vrati veskere parametry k uzivateli z ulozene preference
     *
     * @return Osoba
     */
    @NonNull
    public Osoba vratParametryUzivatel(int typParametru) {
        Osoba osoba = new Osoba();
        if (!getBoolean(S_ZRUS_REGISTRACI, false)) {
            for (String key : def.parametrOp.getAllKeys(typParametru)) {
                ParametrItem pi = def.parametrOp.getParametrItem(key, typParametru);
                if (pi != null) {
                    if (pi.getTyp() == ParametrItem.PAR_BOOLEAN) {
                        osoba.setKeyValue(key, getBoolean(key, pi.getDefaultBool()));
                    } else {
                        osoba.setKeyValue(key, getString(key, pi.getDefaultString()));
                    }

                }
            }
        }
        return osoba;
    }

    public String getString(String nazev, String defValue) {
        return sharedPreferences.getString(nazev, defValue);
    }

    public String getString(String nazev) {
        return getString(nazev, "");
    }

    public int getIntFromString(String nazev, Integer defValue) {
        return parseInt(sharedPreferences.getString(nazev, defValue.toString()));
    }

    double getDouble(String nazev) {
        double retVal;
        try {
            String s = sharedPreferences.getString(nazev, "0.0");
            retVal = Double.parseDouble(s);
        } catch (Exception e) {
            retVal = 0.0;
        }
        return retVal;
    }

    public double getDouble(String nazev, Double defValue) {
        double retVal;
        try {
            String s = sharedPreferences.getString(nazev, defValue.toString());
            retVal = Double.parseDouble(s);
        } catch (Exception e) {
            retVal = defValue;
        }
        return retVal;
    }

    public boolean getBoolean(String nazev, boolean defValue) {
        boolean retVal;
        try {
            retVal = sharedPreferences.getBoolean(nazev, defValue);
        } catch (Exception e) {
            MyLog.e(TAG, e);
            retVal = false;
        }
        return retVal;
    }

    long getLong(String nazev, long defValue) {
        long retVal;
        try {
            retVal = sharedPreferences.getLong(nazev, defValue);
        } catch (Exception e) {
            MyLog.e(TAG, e);
            retVal = 0;
        }
        return retVal;
    }

    long getLong(String nazev) {
        return getLong(nazev, 0);
    }

    public void putString(String nazev, String value) {
        sharedPreferences.edit().putString(nazev, value).apply();
    }

    public void putInt(String nazev, int value) {
        sharedPreferences.edit().putInt(nazev, value).apply();
    }

    void putLong(String nazev, long value) {
        sharedPreferences.edit().putLong(nazev, value).apply();
    }

    public void setDouble(String key, Double value) {
        sharedPreferences.edit().putString(key, value.toString()).apply();
    }
}

