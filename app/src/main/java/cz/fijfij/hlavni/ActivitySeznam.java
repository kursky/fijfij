/*
 * Copyright (c) 2017 Jiri Kursky
 */

package cz.fijfij.hlavni;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import cz.fijfij.R;

/**
 * Created by jiri.kursky on 08.01.2017.
 * Telefonní seznam použitelný pro výběr
 */

public class ActivitySeznam extends BaseActivity {
    private EditText edPhoneNumber;

    public ActivitySeznam() {
        super(R.layout.activity_seznam);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String iPhoneNumber = intent.getStringExtra(def.PREF_PHONE_NUMBER);

        ArrayList<String> contactName = new ArrayList<>();
        final ArrayList<String> phoneNumbers = new ArrayList<>();
        ArrayList<Bitmap> photos = new ArrayList<>();


        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

        String phoneNumber;
        while (phones.moveToNext()) {
            int phoneType = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
            if (phoneType == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE) {
                phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                contactName.add(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
                phoneNumbers.add(phoneNumber);
                photos.add(def.retrieveContactPhoto(this, phoneNumber));
            }
        }
        phones.close();

        AdapterSeznam adapterSeznam = new AdapterSeznam(this, contactName, phoneNumbers, photos);
        ListView list = (ListView) findViewById(R.id.listSeznam);
        list.setAdapter(adapterSeznam);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                finishOK(phoneNumbers.get(position));
            }
        });
        edPhoneNumber = (EditText) findViewById(R.id.edPhone);
        edPhoneNumber.setText(iPhoneNumber);
    }

    private void finishOK(String phoneNumber) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(def.PREF_PHONE_NUMBER, phoneNumber);
        setResult(RESULT_OK, returnIntent);
        finish();
    }


    public void clickOK(View view) {
        finishOK(edPhoneNumber.getText().toString());
    }
}
