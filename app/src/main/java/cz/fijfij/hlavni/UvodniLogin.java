package cz.fijfij.hlavni;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import cz.fijfij.R;
import cz.fijfij.hlavni.lokace.LatLngItem;
import cz.fijfij.hlavni.lokace.LocationService;
import cz.fijfij.hlavni.nastaveni.ParametrItem;

import org.json.JSONObject;

/**
 * Logování voláno při konfiguraci služby, pokud dostane status ST_NEAUTORIZOVAN
 */
public class UvodniLogin extends BaseActivity {
    private static final int OKNO_VYSLEDEK_OK = 2;
    private static final int OKNO_FATALNI_CHYBA = 3;
    private static final int CEKANI_SMS_ZRUSIT = 5;
    private static final int KOD_ZADAN = 6; // Následuje po zadání kódu od řídícího
    private static final int UVOD = 7;

    private Context context;
    private RelativeLayout rlVyber;
    private RelativeLayout rlVysledek;
    private RelativeLayout rlZadaniKodu;
    private RelativeLayout rlRidici;
    private TextView txtInfoText;

    private int vysledekDef;
    private Button btnVysledek;
    private ProgressBar cekejSpinner;
    private String sKodOdRidiciho;
    private Activity activity;
    private SettingOp settingOp;
    private Identifikace identifikace;
    private ParametrOp parametrOp;
    private SmsParovani smsParovani = null;
    private IntentFilter smsFilter;
    String casoveRazitko;
    EditText kodOdRidiciho;
    private TextView txtUpozorneniGPS;
    private TextView txtUpozorneniGPS2;
    private TextView txtUpozorneniNET;
    private TextView txtUpozorneniNET2;
    private RadioButton rbRidici;
    private boolean network;
    private Button btnDalsi;
    private boolean bylaChyba = false;
    private boolean kodZadan = false;

    public UvodniLogin() {
        super(R.layout.activity_uvodni_login, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        activity = this;

        settingOp = new SettingOp(this);
        identifikace = new Identifikace(settingOp, false);
        parametrOp = new ParametrOp();

        RelativeLayout rlHeader = (RelativeLayout) findViewById(R.id.rl_header);
        rlHeader.setVisibility(View.VISIBLE);

        rlVyber = (RelativeLayout) findViewById(R.id.rl_vyber);
        rlVyber.setVisibility(View.VISIBLE);

        rlZadaniKodu = (RelativeLayout) findViewById(R.id.rl_zadaniKodu);
        rlZadaniKodu.setVisibility(View.GONE);

        txtInfoText = (TextView) findViewById(R.id.txtInfoText);

        rlVysledek = (RelativeLayout) findViewById(R.id.rlVysledek);
        rlVysledek.setVisibility(View.GONE);

        btnVysledek = (Button) findViewById(R.id.btnVysledek);
        btnVysledek.setVisibility(View.VISIBLE);

        cekejSpinner = (ProgressBar) findViewById(R.id.cekejSpinner);
        cekejSpinner.setVisibility(View.GONE);

        rlRidici = (RelativeLayout) findViewById(R.id.rl_ridici);
        rlRidici.setVisibility(View.GONE);

        rbRidici = (RadioButton) findViewById(R.id.rbRidici);

        txtUpozorneniGPS = (TextView) findViewById(R.id.txtUpozorneniGPS_1);
        txtUpozorneniGPS2 = (TextView) findViewById(R.id.txtUpozorneniGPS_2);

        txtUpozorneniNET = (TextView) findViewById(R.id.txtUpozorneniNET_1);
        txtUpozorneniNET2 = (TextView) findViewById(R.id.txtUpozorneniNET_2);

        btnDalsi = (Button) findViewById(R.id.btnDalsi);

        kodOdRidiciho = (EditText) findViewById(R.id.edKodOdRidiciho);
        kodOdRidiciho.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                                   @Override
                                                   public void onFocusChange(View v, boolean hasFocus) {
                                                       if (hasFocus && !kodZadan) {
                                                           dlgKodOdRidiciho();
                                                       }
                                                   }
                                               }

        );

        final LocationService locationService = new LocationService(this, new LocationService.ZmenaNastaveni() {
            @Override
            public void onPolohaPovolena(boolean ano) {
                upozorneniNeniTrebaGPS(ano);
            }
        });
        RadioGroup rb = (RadioGroup) findViewById(R.id.radioGroupRodicPritel);
        rb.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (rbRidici.isChecked()) {
                    upozorneniNeniTrebaGPS(true);
                } else {
                    upozorneniNeniTrebaGPS(locationService.isGPSEnabled);
                }
            }

        });
        network = isDataConnected();
        registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                network = isDataConnected();
                upozorneniNeniTrebaNET(network);
            }
        }, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        upozorneniNeniTrebaGPS(locationService.isGPSEnabled);
        upozorneniNeniTrebaNET(network);
        def.hideKeyboard(this);
    }

    private boolean isDataConnected() {
        try {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo().isConnectedOrConnecting();
        } catch (Exception e) {
            return false;
        }
    }

    private void upozorneniNeniTrebaGPS(boolean ano) {
        if (rbRidici.isChecked()) ano = true;
        txtUpozorneniGPS.setVisibility(ano ? View.GONE : View.VISIBLE);
        txtUpozorneniGPS2.setVisibility(ano ? View.GONE : View.VISIBLE);
    }

    private void upozorneniNeniTrebaNET(boolean ano) {
        txtUpozorneniNET.setVisibility(ano ? View.GONE : View.VISIBLE);
        txtUpozorneniNET2.setVisibility(ano ? View.GONE : View.VISIBLE);
        btnDalsi.setVisibility(ano ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setInfo(@StringRes int info) {
        if (info == 0)
            txtInfoText.setText("");
        else
            txtInfoText.setText(info);
    }

    @Override
    public void onResume() {
        super.onResume();
        /*
        LocationService.getLocationManager(this, new LocationService.ZmenaNastaveni() {
            @Override
            public void onPolohaPovolena(boolean ano) {
                upozorneniNeniTrebaGPS(ano);
            }
        });
        */
        if (smsParovani != null) registerReceiver(smsParovani, smsFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (smsParovani != null) unregisterReceiver(smsParovani);
    }

    private void otevriSplash() {
        def.hideKeyboard(activity);
        settingOp.putBoolean(def.PREF_NASTAV_PRISTUPY, true);
        Intent intent = new Intent(context, Splash.class);
        context.startActivity(intent);
    }

    private final Handler startHandler = new Handler();

    /**
     * Ceka se az se objeví 'vnuceno'
     */
    private void cekejSmyckouNaParametry() {
        Runnable runCheckServer = new Runnable() {
            @Override
            public void run() {
                _cekejNaParametry();
            }
        };
        startHandler.postDelayed(runCheckServer, 1000 * 15);
    }

    /**
     * Jsou to parametry, které dodá řídící telefon s příznakem VNUCENO=1
     */
    private void _cekejNaParametry() {
        identifikace.sitUdaje(new AsyncService.TaskListener() {
            @Override
            public void onFinished(String nameAction, JSONObject result) {
                Bundle bundle = def.transferJSON(result);
                if (bundle == null) cekejSmyckouNaParametry();
                boolean vnuceno = def.parseInt(bundle.getString("VNUCENO", "0")) != 0;
                if (vnuceno) {
                    parametrOp.nastavOsobu(def.osoba, bundle);
                    // opet odesílá data o uživateli, tentokráte jiz bez vnuceno
                    settingOp.sendUserParametres(def.osoba, false, new SettingOp.OdeslanyZmeny() {
                        @Override
                        public void onStart() {

                        }

                        @Override
                        public void onFinished() {
                            otevriSplash();
                        }
                    });
                } else {
                    cekejSmyckouNaParametry();
                }
            }
        });

    }


    @Override
    public void cekaniNastaveno(int zdrojVolani) {
        switch (zdrojVolani) {
            case KOD_ZADAN:
                // Tímto se vytvoří userid připojeného k android_id
                // tim to nekončí, musi odeslat ridici konfiguraci - na to se čeká
                // mělo by se vse uložit do parametru
                SpojeniOp spojeniOp = new SpojeniOp(this);
                spojeniOp.kodRodiceZadan(sKodOdRidiciho, def.android_id, new SpojeniOp.OdeslanoServeru() {
                    @Override
                    public void nastalaChyba() {
                        zrusCekej();
                        bylaChyba = true;
                        if (kodOdRidiciho != null) kodOdRidiciho.setError(getString(R.string.wrong_code));
                        dlgKodOdRidiciho();
                    }

                    @Override
                    public void vseOK(Bundle bundle) {
                        def.setUserParams(bundle, ParametrOp.PARS_RIDICI);
                        settingOp.nastavUzivateleDoPreference(def.osoba, ParametrOp.PARS_RIDICI);
                        nastavCekej(UVOD);
                    }
                });
                break;

            case UVOD:
                PrateleOp prateleOp = new PrateleOp(new BufferOp(this), new PrateleOp.CallBackListener() {
                    @Override
                    public void onFinished() {
                        settingOp.nastavRazitko(casoveRazitko);
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle(R.string.info);
                        builder.setCancelable(false);
                        builder.setMessage(R.string.uspesne_sparovani);
                        builder.setPositiveButton(R.string.dlgOK, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                def.restartApp(activity); // restart aplikace
                            }
                        });
                        builder.show();

                    }
                });
                prateleOp.executeForced();
                break;
        }

    }

    /**
     * Závěrečné okno po prihlaseni
     */
    public void btnVysledek(View view) {
        switch (vysledekDef) {
            case CEKANI_SMS_ZRUSIT:
                txtInfoText.setText(R.string.neregistrovany);
                rlVyber.setVisibility(View.VISIBLE);
                rlVysledek.setVisibility(View.GONE);
                break;

            case OKNO_VYSLEDEK_OK:
                final Activity activity = this;
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.info);
                builder.setCancelable(false);
                builder.setMessage(R.string.uspesne_sparovani);
                builder.setPositiveButton(R.string.dlgOK, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        def.restartApp(activity);
                    }
                });
                builder.show();
                def.restartApp(activity);
                break;

            case OKNO_FATALNI_CHYBA:
                break;
        }
    }

    /**
     * Klik na další stránku klik na tlačítko btnDalsi
     * Jsou zde záměrně jen dvě možnosti
     */
    public void stranaClick(View view) {
        if (rbRidici.isChecked()) {
            rlVyber.setVisibility(View.GONE);
            rlVysledek.setVisibility(View.GONE);
            rlRidici.setVisibility(View.VISIBLE);
            rlZadaniKodu.setVisibility(View.GONE);
            txtInfoText.setText(R.string.zalogovani);

        } else {
            rlVyber.setVisibility(View.GONE);
            rlVysledek.setVisibility(View.GONE);
            rlRidici.setVisibility(View.GONE);
            rlZadaniKodu.setVisibility(View.VISIBLE);
            dlgKodOdRidiciho();
        }
    }

    private void dlgKodOdRidiciho() {
        ParametrItem parametrItem = new ParametrItem();
        int nazev = bylaChyba ? R.string.kod_od_ridiciho_chyba : R.string.kod_od_ridiciho;
        parametrItem.setNazev(nazev);
        parametrItem.setInputType(InputType.TYPE_CLASS_TEXT);
        final EditText dlgEditText = parametrItem.setDlgEditText(this);
        final AlertDialog dialog = ParametrOp.getAlertDialog(this, parametrItem);
        Button theButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        theButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mValue = dlgEditText.getText().toString();
                if (TextUtils.isEmpty(mValue)) {
                    Toast.makeText(getBaseContext(), R.string.hodnota_musi_byt_zadana, Toast.LENGTH_SHORT).show();
                    return;
                }
                kodOdRidiciho.setText(mValue);
                kodOdRidiciho.setError(null);
                dialog.dismiss();
            }
        });
    }

    public void clickBtnZpet(View view) {
        def.hideKeyboard(this);
        rlVyber.setVisibility(View.VISIBLE);
        rlVysledek.setVisibility(View.GONE);
        rlRidici.setVisibility(View.GONE);
        rlZadaniKodu.setVisibility(View.GONE);
        txtInfoText.setText(R.string.neregistrovany);
    }


    /**
     * Voláno z tlačítka "Další" při registraci telefonu jako řídícího
     */
    public void clickNastavRidici(View view) {
        def.hideKeyboard(this);
        EditText edNick = (EditText) findViewById(R.id.edNickName);
        EditText edPhoneNumber = (EditText) findViewById(R.id.edPhoneNumber);
        final EditText edEmail = (EditText) findViewById(R.id.edEmail);

        final String nick = edNick.getText().toString();
        if (TextUtils.isEmpty(nick)) {
            edNick.setError(getString(R.string.error_field_required));
            return;
        }
        String phoneNumber = edPhoneNumber.getText().toString();
        String stdPhoneNumber = def.phoneNumberStandard(phoneNumber);
        if (stdPhoneNumber != null) phoneNumber = stdPhoneNumber;
        edPhoneNumber.setText(phoneNumber);

        final String pNumber = phoneNumber;

        if (TextUtils.isEmpty(phoneNumber)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.info);
            builder.setCancelable(true);

            builder.setMessage(R.string.nevyplneno_telefonni_cislo);
            builder.setPositiveButton(R.string.dlgAno, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    definujRidiciho(nick, pNumber, edEmail.getText().toString());
                }
            });
            builder.setNegativeButton(R.string.dlgNe, null);
            builder.show();
        } else {
            definujRidiciho(nick, phoneNumber, edEmail.getText().toString());
        }
    }

    /**
     * Odeslani definice řídícího
     *
     * @param nick        přezdívka
     * @param phoneNumber telefonní číslo
     */
    private void definujRidiciho(String nick, String phoneNumber, String eMail) {
        LatLngItem latLngItem = settingOp.getLangLngItem();
        String action = def.EX_ACTION + def.CMD_NOVY_UZIVATEL;
        action += def.EX_ANDROID_ID + def.android_id;
        action += def.EX_EMAIL + eMail;
        action += def.EX_NICK + Uri.encode(nick);
        action += def.EX_SENT_TIME + def.todaySQL();
        action += def.EX_LATITUDE + latLngItem.sGetLatitude();
        action += def.EX_LONGITUDE + latLngItem.sGetLongitude();
        action += def.EX_AOS + android.os.Build.VERSION.SDK_INT;
        if (!TextUtils.isEmpty(phoneNumber)) action += def.EX_PHONE_NUMBER + phoneNumber;
        action += def.EX_JE_RIDICI + "1";

        rlVyber.setVisibility(View.GONE);
        rlVysledek.setVisibility(View.VISIBLE);
        rlRidici.setVisibility(View.GONE);
        btnVysledek.setVisibility(View.GONE);
        cekejSpinner.setVisibility(View.VISIBLE);
        txtInfoText.setText(R.string.pokus_prihlaseni);

        final String axAction = action;
        new Handler().postDelayed(new Runnable() {
                                      @Override
                                      public void run() {
                                          AsyncService as = new AsyncService(new AsyncService.TaskListener() {
                                              @Override
                                              public void onFinished(String nameAction, JSONObject result) {
                                                  if (result != null) {
                                                      Bundle bundle = def.transferJSON(result);
                                                      if (!TextUtils.isEmpty(bundle.getString(def.RET_USER_ID))) {
                                                          if (def.osoba == null)
                                                              def.osoba = new Osoba();
                                                          def.osoba.nastavParametryBundleAll(bundle, def.vratTypParametru());
                                                          settingOp.nastavUzivateleDoPreference(def.osoba, ParametrOp.PARS_ADMIN);

                                                      }
                                                      cekejSpinner.setVisibility(View.GONE);
                                                      btnVysledek.setVisibility(View.VISIBLE);
                                                      vysledekDef = OKNO_VYSLEDEK_OK;
                                                      btnVysledek.setText(R.string.dlgOK);
                                                      setInfo(R.string.loginOK);

                                                  } else {
                                                      cekejSpinner.setVisibility(View.GONE);
                                                      btnVysledek.setVisibility(View.VISIBLE);
                                                      vysledekDef = CEKANI_SMS_ZRUSIT;
                                                      btnVysledek.setText(R.string.zpet);
                                                      setInfo(R.string.err_chyba_login);
                                                  }
                                              }
                                          }, getBaseContext(), "Login", axAction);
                                          as.execute();
                                      }
                                  }
                , 100);
    }

    /**
     * Nastaven kod z řídícího telefonu a stisknuto tlačítko Další
     */
    public void nastavenKod(View view) {
        kodZadan = true;
        sKodOdRidiciho = kodOdRidiciho.getText().toString();
        nastavCekej(KOD_ZADAN);
    }
}