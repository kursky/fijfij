/*
 * Copyright (c) 2016 Jiri Kursky
 */

package cz.fijfij.hlavni;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import cz.fijfij.R;

/**
 * Created by jiri.kursky on 07.11.2016.
 * Napojeny adapter na ActivityOsoba
 */

public class AdapterZpravy extends ArrayAdapter<String> {
    private Activity activity;
    private ArrayList<String> arZpravy;


    AdapterZpravy(Activity activity, ArrayList<String> arZpravy) {
        super(activity, R.layout.list_single_osoba, arZpravy);
        this.arZpravy=arZpravy;
        this.activity = activity;
    }

    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_single_zadost, null, true);

        if (position>arZpravy.size()) return rowView;
        TextView txtZadostCas = (TextView) rowView.findViewById(R.id.txtZadostCas);
        txtZadostCas.setText(arZpravy.get(position));
        return rowView;
    }
}

