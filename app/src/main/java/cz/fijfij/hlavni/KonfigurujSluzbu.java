package cz.fijfij.hlavni;

import android.content.Context;
import android.os.Message;

import cz.fijfij.R;
import cz.fijfij.hlavni.service.FijService;

/**
 * Created by jiri.kursky on 5.8.2016.
 */
// Zjisti se, jestli sluzba bezi, pokud ano - pretahuji se z ni udaje
//overPrava() -> identifikace -> nactiPratele -> konfigurujSluzbu ->
//        * odeslani MSG_TEST_SERVICE -> pokud nereaguje pousti se sluzba -> Main

public class KonfigurujSluzbu {
    private static final int UVODNI_LOGIN = 1;
    public static final int MAIN_ACTIVITY = 2;
    public ServiceOp serviceOp;
    private Context context;
    private BufferOp bufferOp;
    private CallBackListener callBackListener;
    private Identifikace identifikace;

    public interface CallBackListener {
        void zobrazChybu(int err);
        boolean oteviramNovouAktivitu(int nazevAktivity);
    }

    public KonfigurujSluzbu(Context context, CallBackListener callBackListener) {
        this.context= context;
        this.callBackListener=callBackListener;
        bufferOp=new BufferOp(context);
    }

    public void execute() {
        // Zde se ziskaji informace o uzivateli,
        // pokud je nastavena sit, bere se to z ni
        // provadi se zapis do databaze
        // pokud sit neni vytahuji se udaje z databaze
        //identifikace = new Identifikace(context, bufferOp, identBackListener);
        //identifikace.execute();        // vzdy skoci do identBackListener
    }
    /**
     * Konecne volani identifikace puvodne volano ze Splash nyni z UvodniLogin
     */
    private Identifikace.CallBackListener identBackListener = new Identifikace.CallBackListener() {
        @Override
        public void onFinished(Osoba osoba, int status) {
            switch (status) {
                case Identifikace.ST_OK: // nejlepsi stav
                    nactiPratele();
                    break;
                case Identifikace.ST_NEAUTORIZOVAN: //
                    if (callBackListener.oteviramNovouAktivitu(UVODNI_LOGIN))
                        SledovaniActivit.startActivity(context, 100, UvodniLogin.class);
                    break;
                case Identifikace.ST_CHYBA_BEHEM_KOMUNIKACE:
                    callBackListener.zobrazChybu(R.string.chyba_serveru);
                    break;
                case Identifikace.ST_NENI_SIT:
                    callBackListener.zobrazChybu(R.string.zapneteSit);
                    break;
            }
        }
    };


    private PrateleOp.CallBackListener prateleCallBack= new PrateleOp.CallBackListener() {
        @Override
        public void onFinished() {
            nastavSluzbu();
        }
    };

    private void nactiPratele() {
        PrateleOp prateleOp=new PrateleOp(bufferOp, prateleCallBack);
        prateleOp.execute();
    }

    // Pouziva se pri startu a zjistuje se, zda-li sluzba bezi
    private ServiceOp.ServiceInt serviceIntStart=new ServiceOp.ServiceInt() {
        @Override
        public void onServiceConnected() {
            serviceOp.sendMessageToService(FijService.MSG_TEST_SERVICE,0,null);
        }

        @Override
        public void onServiceDisconnected() {

        }

        @Override
        public boolean receivedMessage(Message msg) {
            switch (msg.what) {
                case FijService.MSG_NOT_RUNNING:
                    serviceOp.startService();
            }
            if (callBackListener.oteviramNovouAktivitu(MAIN_ACTIVITY))
                SledovaniActivit.startActivity(context, 1000, def.getMainActivity(context));
            return false;
        }
    };


    private void nastavSluzbu() {
        serviceOp=new ServiceOp(context, serviceIntStart);
        serviceOp.doBindService();
    }
}
